self["webpackHotUpdate_N_E"]("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "__N_SSG": function() { return /* binding */ __N_SSG; },
/* harmony export */   "default": function() { return /* binding */ Home; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next-seo */ "./node_modules/next-seo/lib/next-seo.module.js");
/* harmony import */ var _components_Layout_Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Layout/Header */ "./components/Layout/Header.js");
/* harmony import */ var _components_Layout_Footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Layout/Footer */ "./components/Layout/Footer.js");
/* harmony import */ var _utils_shuffle_image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/shuffle-image */ "./utils/shuffle-image.js");
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/image */ "./node_modules/next/image.js");
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var framer_motion__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! framer-motion */ "./node_modules/framer-motion/dist/es/index.mjs");
/* harmony import */ var next__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next */ "next");
/* harmony import */ var next__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_intersection_observer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-intersection-observer */ "./node_modules/react-intersection-observer/react-intersection-observer.m.js");
/* harmony import */ var _hooks_useCountDown__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../hooks/useCountDown */ "./hooks/useCountDown.js");
/* harmony import */ var _components_Home_Founder__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/Home/Founder */ "./components/Home/Founder.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_10__);
/* module decorator */ module = __webpack_require__.hmd(module);
/* provided dependency */ var process = __webpack_require__(/*! process */ "./node_modules/process/browser.js");



var _jsxFileName = "C:\\Users\\samee\\Desktop\\crew-main\\pages\\index.js",
    _s = $RefreshSig$();












 // type Props = {
//   data: {
//     title: { rendered: string };
//     content: { rendered: string };
//     acf: {
//       profile_image_url: string;
//       first_section: string;
//       second_section: string;
//       third_section: string;
//       fourth_section: string;
//       value_first: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//       value_second: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//       value_third: {
//         image_: string;
//         info: string;
//       };
//       value_fourth: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//       value_fifth: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//     };
//   };
// };

var mspeaker = [{
  name: "Gary Vaynerchuk",
  itemsToShow: "Gary-Vaynerchuk-min.jpg"
}, {
  name: "Arianna Huffington",
  itemsToShow: "Arianna-Huffington-min.jpg"
}, {
  name: "Robert Kiyosaki",
  itemsToShow: "Robert-Kiyosaki.jpg"
}, {
  name: "Patrick Bet-David",
  itemsToShow: "Patrick-bet-david-min.jpg"
}, {
  name: "Tim Ferriss",
  itemsToShow: "Tim-Ferriss-min.png"
}, {
  name: "Tai Lopez",
  itemsToShow: "Tai-Lopez-min.jpg"
}];
var __N_SSG = true;
function Home(_ref) {
  _s();

  var _this = this;

  var data = _ref.data;

  var _useInView = (0,react_intersection_observer__WEBPACK_IMPORTED_MODULE_11__.useInView)({
    threshold: 0.2
  }),
      ref = _useInView.ref,
      inView = _useInView.inView;

  var _useCountDown = (0,_hooks_useCountDown__WEBPACK_IMPORTED_MODULE_8__.default)("2021-12-04 10:00 AM"),
      showReleaseDate = _useCountDown.showReleaseDate,
      coundownText = _useCountDown.coundownText;

  var animation = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var firstImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var secondImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var thirdImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var fourthImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var slideLeft = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var imagePath = [{
    src: "slideshow/boxer 2.png",
    animation: secondImg
  }, {
    src: "slideshow/boxer.png",
    animation: firstImg
  }, {
    src: "slideshow/dennis-3.png",
    animation: fourthImg
  }, {
    src: "slideshow/diver.png",
    animation: secondImg
  }, {
    src: "slideshow/mobster.png",
    animation: thirdImg
  }, {
    src: "slideshow/pilot.png",
    animation: thirdImg
  }, {
    src: "slideshow/pirates cheetah.png",
    animation: thirdImg
  }, {
    src: "slideshow/russell.png",
    animation: thirdImg
  }, {
    src: "slideshow/surfer-2.png",
    animation: thirdImg
  }, {
    src: "slideshow/tiger woods.png",
    animation: thirdImg
  }, {
    src: "slideshow/zombie cheetah.png",
    animation: thirdImg
  }, // { src: "slideshow/989.png", animation: thirdImg },
  // { src: "slideshow/995.png", animation: thirdImg },
  // { src: "slideshow/1014.png", animation: thirdImg },
  // { src: "slideshow/1007.png", animation: thirdImg },
  // { src: "slideshow/1017.png", animation: thirdImg },
  // { src: "slideshow/105.png", animation: thirdImg },
  // { src: "slideshow/107.png", animation: thirdImg },
  // { src: "slideshow/108.png", animation: thirdImg },
  // { src: "slideshow/109.png", animation: thirdImg },
  // { src: "slideshow/110.png", animation: thirdImg },
  // { src: "slideshow/112.png", animation: thirdImg },
  // { src: "slideshow/124.png", animation: thirdImg },
  // { src: "slideshow/139.png", animation: thirdImg },
  // { src: "slideshow/140.png", animation: thirdImg },
  // { src: "slideshow/92.png", animation: thirdImg },
  // { src: "slideshow/93.png", animation: thirdImg },
  {
    src: "slideshow/1.gif",
    animation: firstImg
  }, {
    src: "slideshow/2.gif",
    animation: secondImg
  }, {
    src: "slideshow/3.png",
    animation: thirdImg
  }, {
    src: "slideshow/4.png",
    animation: fourthImg
  }, {
    src: "slideshow/5.png",
    animation: firstImg
  }, {
    src: "slideshow/6.png",
    animation: secondImg
  }, {
    src: "slideshow/7.png",
    animation: thirdImg
  }, {
    src: "slideshow/8.png",
    animation: fourthImg
  }, {
    src: "slideshow/9.png",
    animation: firstImg
  }, {
    src: "slideshow/10.jpeg",
    animation: firstImg
  }, {
    src: "slideshow/11.png",
    animation: secondImg
  } // { src: "19.png", animation: secondImg },
  // { src: "139.png", animation: secondImg },
  // { src: "147.png", animation: secondImg },
  // { src: "148.png", animation: secondImg },
  // { src: "149.png", animation: secondImg },
  // { src: "5521.png", animation: secondImg },
  // { src: "569.png", animation: secondImg },
  // { src: "520.png", animation: secondImg },
  // { src: "335.png", animation: secondImg },
  // { src: "162.png", animation: secondImg },
  ];

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_7__.useState)(true),
      shouldShowActions = _useState[0],
      setShouldShowActions = _useState[1];

  (0,react__WEBPACK_IMPORTED_MODULE_7__.useEffect)(function () {
    if (inView) {
      animation.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 0.6
        }
      });
      firstImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 0.8
        }
      });
      secondImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 0.9
        }
      });
      thirdImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 1.2
        }
      });
      fourthImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 1.4
        }
      });
      slideLeft.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
    }

    if (!inView) {
      animation.start({
        x: 0,
        y: 258,
        opacity: 0,
        zIndex: -1,
        transition: {
          duration: 1
        }
      });
      firstImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: -1,
        transition: {
          duration: 1
        }
      });
      secondImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
      thirdImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
      fourthImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
      slideLeft.start({
        x: 156,
        y: 0,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
    }
  }, [inView]);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(next_seo__WEBPACK_IMPORTED_MODULE_1__.NextSeo, {
      title: "Home - Coalitioncrew",
      description: "Youtube Twitter Instagram Welcome to the Coalition Mint here This is the NFT for Game Changers. The Coalition Crew is an exclusive collection of 7100 unique Cheetah NFTs living on the Ethereum blockchain. It\u2019s estimated that as of 2021, there are only 7100 cheetahs left in the wild. Cheetahs are currently listed as vulnerable and \u2026 Home Read More \xBB",
      openGraph: {
        description: "Youtube Twitter Instagram Welcome to the Coalition Mint here This is the NFT for Game Changers. The Coalition Crew is an exclusive collection of 7100 unique Cheetah NFTs living on the Ethereum blockchain. It’s estimated that as of 2021, there are only 7100 cheetahs left in the wild. Cheetahs are currently listed as vulnerable and &hellip; Home Read More &raquo;",
        title: "Home - Coalitioncrew",
        type: "website",
        locale: "en_US",
        url: process.env.NEXT_PUBLIC_DOMAIN,
        images: [{
          url: "http://coalitioncrew.com/wp-content/uploads/2021/10/TCC_1-Transparent-1024x982.png",
          width: 1200,
          height: 630,
          alt: "coalition crew logo"
        }]
      }
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 214,
      columnNumber: 7
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("main", {
      className: "mx-auto",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        id: "banner",
        className: " relative bg-crew w-full  h-screen bg-cover flex flex-col justify-between",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_Layout_Header__WEBPACK_IMPORTED_MODULE_2__.default, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 240,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "flex flex-col  md:absolute right-64 bottom-11   justify-center items-center",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            id: "cta2",
            className: "  text-3xl bg-yellow-300 uppercase italic font-bold  mb-2 px-16 py-4  text-iconColor",
            children: "Mint Here"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 243,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "text-2xl font-bold text-gray-800 mb-4",
            children: showReleaseDate && coundownText
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 250,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 242,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 236,
        columnNumber: 9
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: " text-gray-800 ",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "post-content container mx-auto text-lg p-4 text-center md:text-left ",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "first-section",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h1", {
              className: "text-gray-900 text-left our-mission font-bold text-3xl border-b-2 pb-3 border-gray-800 md:w-1/4",
              children: "Our Mission"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 259,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("section", {
              className: "grid md:grid-cols-2 items-center",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "about-mission",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: "Hi! Welcome to the Coalition Crew! This is a limited collection of 5000 unique Cheetah NFTs living on the Ethereum blockchain. This project is technically broken up into three collections."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 265,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: "1st Collection - Coalition Crew (OG COLLECTION) Only 1010 avail. *Update - Sold out in less than a day!*"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 266,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: ["2nd Collection - Auction Cheetahs - 20 avail. These qualify for VIP access to all live events. The auction begins Jan 8th. Please visit our ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
                    target: "_blank",
                    href: "http://discord.gg/3nKRBcDS33",
                    rel: "noreferrer",
                    "class": "underline",
                    children: " Discord"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 268,
                    columnNumber: 160
                  }, this), " for more info on this! "]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 268,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: "3rd Collection - Coalition Crew 2.0 - Only 3970 avail. This will wrap up the collection for the Coalition Crew project. Mint price is .09 ETH and you can mint up to 5. This collection will begin minting Jan 26th at 10am PST/1pm EST."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 269,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: ["Out of the 5000 total, only 50 qualify for VIP access. Please visit our ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
                    target: "_blank",
                    href: "http://discord.gg/3nKRBcDS33",
                    rel: "noreferrer",
                    "class": "underline",
                    children: " Discord"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 270,
                    columnNumber: 92
                  }, this), " Discord for more info on which ones qualify for VIP."]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 270,
                  columnNumber: 17
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 263,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "flex items-center justify-center relative",
                children: [" ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(framer_motion__WEBPACK_IMPORTED_MODULE_12__.motion.div, {
                  initial: {
                    opacity: 0,
                    height: "20rem",
                    width: "20rem"
                  },
                  animate: {
                    opacity: 1,
                    height: "22rem",
                    width: "22rem"
                  },
                  className: "h-72  duration-300 w-72 md:w-96 md:h-96 bg-gray-800 rounded-full absolute z-0 md:left-38"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 274,
                  columnNumber: 19
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_image__WEBPACK_IMPORTED_MODULE_5___default()), {
                  alt: "logo",
                  src: "/img/running-cheetah.gif",
                  width: 400,
                  className: "relative",
                  objectFit: "contain",
                  height: 450
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 279,
                  columnNumber: 19
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 272,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 262,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 258,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 257,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "flex justify-center",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "w-36 h-2 flex  bg-gray-700 my-9"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 292,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 291,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          ref: ref,
          className: "gallery relative justify-items-center flex gap-8  md:grid-cols-4 ",
          children: (0,_utils_shuffle_image__WEBPACK_IMPORTED_MODULE_4__.default)(imagePath).map(function (image, index) {
            return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(framer_motion__WEBPACK_IMPORTED_MODULE_12__.motion.div, {
              className: "item min-w-full rounded-lg",
              animate: image.animation,
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_image__WEBPACK_IMPORTED_MODULE_5___default()), {
                alt: "Cheetah",
                src: "/img/" + image.src,
                width: 620,
                height: 650
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 305,
                columnNumber: 17
              }, _this)
            }, index, false, {
              fileName: _jsxFileName,
              lineNumber: 300,
              columnNumber: 15
            }, _this);
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 295,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          "class": "flex flex-col  right-64 bottom-11   justify-center items-center",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            id: "cta2",
            "class": " roadmap  text-3xl bg-yellow-300 uppercase italic font-bold  mb-2 px-16 py-4  text-iconColor",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
              target: "_blank",
              href: "https://drive.google.com/file/d/1JPAXXLlR-vTpeGe3rHmxeq4XC3US3hv5/view?usp=sharing",
              children: "Roadmap"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 315,
              columnNumber: 132
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 315,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            "class": "text-2xl font-bold text-gray-800 mb-4"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 316,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 314,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "flex justify-center",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "max-w-6xl overflow-hidden w-36 h-2 flex mt-8  bg-gray-800 "
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 320,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 319,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner2",
          className: " relative bg-value w-full  h-screen bg-cover flex flex-col justify-between "
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 322,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner3",
          className: " relative bg-mentor w-full  h-screen bg-cover flex flex-col justify-between my-10"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 326,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner4",
          className: " relative bg-network1 w-full  h-screen bg-cover flex flex-col justify-between mb-10"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 330,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner5",
          className: " relative bg-network2 w-full  h-screen bg-cover flex flex-col justify-between mb-10"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 334,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "post-content container mx-auto text-lg p-4 text-center md:text-left ",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "first-section",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("section", {
              className: "fourth-section" // dangerouslySetInnerHTML={{ __html: data.acf.fourth_section }}
              ,
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: ["Once minted or bought on OpenSea, please fill out this", " ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
                    target: "_blank",
                    href: "https://www.gamechangersmovemement.com/crew",
                    rel: "noreferrer",
                    className: "underline",
                    children: "form"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 347,
                    columnNumber: 21
                  }, this), " ", "to get access to the Academy.", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 356,
                    columnNumber: 21
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                    className: "font-bold",
                    children: "Note!"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 357,
                    columnNumber: 21
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 358,
                    columnNumber: 21
                  }, this), " If you sell your only cheetah, your access to the Academy will be suspended. You must own at least 1 to keep access. ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 360,
                    columnNumber: 29
                  }, this), " After 3 months all holders will receive their cheetah adoption \u201Cpaperwork\u201D. This includes a photo, an adoption certificate, a stuffed animal, a species card + more! ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 363,
                    columnNumber: 27
                  }, this), " We are launching a unique merchandise store for Coalition Crew Members to make custom gear. ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 364,
                    columnNumber: 65
                  }, this), " All holders get automatic WL for future projects, as well as exclusive airdrops."]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 345,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 344,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 340,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 339,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_Home_Founder__WEBPACK_IMPORTED_MODULE_9__.default, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 371,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "h-0.5 w-full bg-gray-700 my-8"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 372,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_Layout_Footer__WEBPACK_IMPORTED_MODULE_3__.default, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 373,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 338,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 256,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 235,
      columnNumber: 7
    }, this)]
  }, void 0, true);
}

_s(Home, "Lqqk7nocCv117nQUjWydCl8unGQ=", false, function () {
  return [react_intersection_observer__WEBPACK_IMPORTED_MODULE_11__.useInView, _hooks_useCountDown__WEBPACK_IMPORTED_MODULE_8__.default, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation];
});

_c = Home;

var _c;

$RefreshReg$(_c, "Home");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsibXNwZWFrZXIiLCJuYW1lIiwiaXRlbXNUb1Nob3ciLCJIb21lIiwiZGF0YSIsInVzZUluVmlldyIsInRocmVzaG9sZCIsInJlZiIsImluVmlldyIsInVzZUNvdW50RG93biIsInNob3dSZWxlYXNlRGF0ZSIsImNvdW5kb3duVGV4dCIsImFuaW1hdGlvbiIsInVzZUFuaW1hdGlvbiIsImZpcnN0SW1nIiwic2Vjb25kSW1nIiwidGhpcmRJbWciLCJmb3VydGhJbWciLCJzbGlkZUxlZnQiLCJpbWFnZVBhdGgiLCJzcmMiLCJ1c2VTdGF0ZSIsInNob3VsZFNob3dBY3Rpb25zIiwic2V0U2hvdWxkU2hvd0FjdGlvbnMiLCJ1c2VFZmZlY3QiLCJzdGFydCIsIngiLCJ5Iiwib3BhY2l0eSIsInpJbmRleCIsInRyYW5zaXRpb24iLCJkdXJhdGlvbiIsImRlc2NyaXB0aW9uIiwidGl0bGUiLCJ0eXBlIiwibG9jYWxlIiwidXJsIiwicHJvY2VzcyIsImVudiIsIk5FWFRfUFVCTElDX0RPTUFJTiIsImltYWdlcyIsIndpZHRoIiwiaGVpZ2h0IiwiYWx0Iiwic2h1ZmZsZSIsIm1hcCIsImltYWdlIiwiaW5kZXgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0NBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLElBQU1BLFFBQVEsR0FBRyxDQUNmO0FBQUVDLE1BQUksRUFBRSxpQkFBUjtBQUEyQkMsYUFBVyxFQUFFO0FBQXhDLENBRGUsRUFFZjtBQUFFRCxNQUFJLEVBQUUsb0JBQVI7QUFBOEJDLGFBQVcsRUFBRTtBQUEzQyxDQUZlLEVBR2Y7QUFBRUQsTUFBSSxFQUFFLGlCQUFSO0FBQTJCQyxhQUFXLEVBQUU7QUFBeEMsQ0FIZSxFQUlmO0FBQUVELE1BQUksRUFBRSxtQkFBUjtBQUE2QkMsYUFBVyxFQUFFO0FBQTFDLENBSmUsRUFLZjtBQUFFRCxNQUFJLEVBQUUsYUFBUjtBQUF1QkMsYUFBVyxFQUFFO0FBQXBDLENBTGUsRUFNZjtBQUFFRCxNQUFJLEVBQUUsV0FBUjtBQUFxQkMsYUFBVyxFQUFFO0FBQWxDLENBTmUsQ0FBakI7O0FBUWUsU0FBU0MsSUFBVCxPQUF3QjtBQUFBOztBQUFBOztBQUFBLE1BQVJDLElBQVEsUUFBUkEsSUFBUTs7QUFBQSxtQkFDYkMsdUVBQVMsQ0FBQztBQUFFQyxhQUFTLEVBQUU7QUFBYixHQUFELENBREk7QUFBQSxNQUM3QkMsR0FENkIsY0FDN0JBLEdBRDZCO0FBQUEsTUFDeEJDLE1BRHdCLGNBQ3hCQSxNQUR3Qjs7QUFBQSxzQkFFS0MsNERBQVksQ0FBQyxxQkFBRCxDQUZqQjtBQUFBLE1BRTdCQyxlQUY2QixpQkFFN0JBLGVBRjZCO0FBQUEsTUFFWkMsWUFGWSxpQkFFWkEsWUFGWTs7QUFJckMsTUFBTUMsU0FBUyxHQUFHQyw0REFBWSxFQUE5QjtBQUNBLE1BQU1DLFFBQVEsR0FBR0QsNERBQVksRUFBN0I7QUFDQSxNQUFNRSxTQUFTLEdBQUdGLDREQUFZLEVBQTlCO0FBQ0EsTUFBTUcsUUFBUSxHQUFHSCw0REFBWSxFQUE3QjtBQUNBLE1BQU1JLFNBQVMsR0FBR0osNERBQVksRUFBOUI7QUFDQSxNQUFNSyxTQUFTLEdBQUdMLDREQUFZLEVBQTlCO0FBQ0EsTUFBTU0sU0FBUyxHQUFHLENBQ2hCO0FBQUVDLE9BQUcsRUFBRSx1QkFBUDtBQUFnQ1IsYUFBUyxFQUFFRztBQUEzQyxHQURnQixFQUVoQjtBQUFFSyxPQUFHLEVBQUUscUJBQVA7QUFBOEJSLGFBQVMsRUFBRUU7QUFBekMsR0FGZ0IsRUFHaEI7QUFBRU0sT0FBRyxFQUFFLHdCQUFQO0FBQWlDUixhQUFTLEVBQUVLO0FBQTVDLEdBSGdCLEVBSWhCO0FBQUVHLE9BQUcsRUFBRSxxQkFBUDtBQUE4QlIsYUFBUyxFQUFFRztBQUF6QyxHQUpnQixFQUtoQjtBQUFFSyxPQUFHLEVBQUUsdUJBQVA7QUFBZ0NSLGFBQVMsRUFBRUk7QUFBM0MsR0FMZ0IsRUFNaEI7QUFBRUksT0FBRyxFQUFFLHFCQUFQO0FBQThCUixhQUFTLEVBQUVJO0FBQXpDLEdBTmdCLEVBT2hCO0FBQUVJLE9BQUcsRUFBRSwrQkFBUDtBQUF3Q1IsYUFBUyxFQUFFSTtBQUFuRCxHQVBnQixFQVFoQjtBQUFFSSxPQUFHLEVBQUUsdUJBQVA7QUFBZ0NSLGFBQVMsRUFBRUk7QUFBM0MsR0FSZ0IsRUFTaEI7QUFBRUksT0FBRyxFQUFFLHdCQUFQO0FBQWlDUixhQUFTLEVBQUVJO0FBQTVDLEdBVGdCLEVBVWhCO0FBQUVJLE9BQUcsRUFBRSwyQkFBUDtBQUFvQ1IsYUFBUyxFQUFFSTtBQUEvQyxHQVZnQixFQVdoQjtBQUFFSSxPQUFHLEVBQUUsOEJBQVA7QUFBdUNSLGFBQVMsRUFBRUk7QUFBbEQsR0FYZ0IsRUFZaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQztBQUFFSSxPQUFHLEVBQUUsaUJBQVA7QUFBMEJSLGFBQVMsRUFBRUU7QUFBckMsR0E1QmUsRUE2QmY7QUFBRU0sT0FBRyxFQUFFLGlCQUFQO0FBQTBCUixhQUFTLEVBQUVHO0FBQXJDLEdBN0JlLEVBOEJmO0FBQUVLLE9BQUcsRUFBRSxpQkFBUDtBQUEwQlIsYUFBUyxFQUFFSTtBQUFyQyxHQTlCZSxFQStCZjtBQUFFSSxPQUFHLEVBQUUsaUJBQVA7QUFBMEJSLGFBQVMsRUFBRUs7QUFBckMsR0EvQmUsRUFnQ2Y7QUFBRUcsT0FBRyxFQUFFLGlCQUFQO0FBQTBCUixhQUFTLEVBQUVFO0FBQXJDLEdBaENlLEVBaUNmO0FBQUVNLE9BQUcsRUFBRSxpQkFBUDtBQUEwQlIsYUFBUyxFQUFFRztBQUFyQyxHQWpDZSxFQWtDZjtBQUFFSyxPQUFHLEVBQUUsaUJBQVA7QUFBMEJSLGFBQVMsRUFBRUk7QUFBckMsR0FsQ2UsRUFtQ2Y7QUFBRUksT0FBRyxFQUFFLGlCQUFQO0FBQTBCUixhQUFTLEVBQUVLO0FBQXJDLEdBbkNlLEVBb0NmO0FBQUVHLE9BQUcsRUFBRSxpQkFBUDtBQUEwQlIsYUFBUyxFQUFFRTtBQUFyQyxHQXBDZSxFQXFDZjtBQUFFTSxPQUFHLEVBQUUsbUJBQVA7QUFBNEJSLGFBQVMsRUFBRUU7QUFBdkMsR0FyQ2UsRUFzQ2Y7QUFBRU0sT0FBRyxFQUFFLGtCQUFQO0FBQTJCUixhQUFTLEVBQUVHO0FBQXRDLEdBdENlLENBdUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhEZ0IsR0FBbEI7O0FBVnFDLGtCQTZEYU0sK0NBQVEsQ0FBQyxJQUFELENBN0RyQjtBQUFBLE1BNkQ5QkMsaUJBN0Q4QjtBQUFBLE1BNkRYQyxvQkE3RFc7O0FBOERyQ0Msa0RBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSWhCLE1BQUosRUFBWTtBQUNWSSxlQUFTLENBQUNhLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLENBRFc7QUFFZEMsU0FBQyxFQUFFLENBRlc7QUFHZEMsZUFBTyxFQUFFLENBSEs7QUFJZEMsY0FBTSxFQUFFLENBSk07QUFLZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMRSxPQUFoQjtBQU9BakIsY0FBUSxDQUFDVyxLQUFULENBQWU7QUFDYkMsU0FBQyxFQUFFLENBRFU7QUFFYkMsU0FBQyxFQUFFLENBRlU7QUFHYkMsZUFBTyxFQUFFLENBSEk7QUFJYkMsY0FBTSxFQUFFLENBSks7QUFLYkMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMQyxPQUFmO0FBT0FoQixlQUFTLENBQUNVLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLENBRFc7QUFFZEMsU0FBQyxFQUFFLENBRlc7QUFHZEMsZUFBTyxFQUFFLENBSEs7QUFJZEMsY0FBTSxFQUFFLENBSk07QUFLZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMRSxPQUFoQjtBQU9BZixjQUFRLENBQUNTLEtBQVQsQ0FBZTtBQUNiQyxTQUFDLEVBQUUsQ0FEVTtBQUViQyxTQUFDLEVBQUUsQ0FGVTtBQUdiQyxlQUFPLEVBQUUsQ0FISTtBQUliQyxjQUFNLEVBQUUsQ0FKSztBQUtiQyxrQkFBVSxFQUFFO0FBQUVDLGtCQUFRLEVBQUU7QUFBWjtBQUxDLE9BQWY7QUFPQWQsZUFBUyxDQUFDUSxLQUFWLENBQWdCO0FBQ2RDLFNBQUMsRUFBRSxDQURXO0FBRWRDLFNBQUMsRUFBRSxDQUZXO0FBR2RDLGVBQU8sRUFBRSxDQUhLO0FBSWRDLGNBQU0sRUFBRSxDQUpNO0FBS2RDLGtCQUFVLEVBQUU7QUFBRUMsa0JBQVEsRUFBRTtBQUFaO0FBTEUsT0FBaEI7QUFPQWIsZUFBUyxDQUFDTyxLQUFWLENBQWdCO0FBQ2RDLFNBQUMsRUFBRSxDQURXO0FBRWRDLFNBQUMsRUFBRSxDQUZXO0FBR2RDLGVBQU8sRUFBRSxDQUhLO0FBSWRDLGNBQU0sRUFBRSxDQUpNO0FBS2RDLGtCQUFVLEVBQUU7QUFBRUMsa0JBQVEsRUFBRTtBQUFaO0FBTEUsT0FBaEI7QUFPRDs7QUFDRCxRQUFJLENBQUN2QixNQUFMLEVBQWE7QUFDWEksZUFBUyxDQUFDYSxLQUFWLENBQWdCO0FBQ2RDLFNBQUMsRUFBRSxDQURXO0FBRWRDLFNBQUMsRUFBRSxHQUZXO0FBR2RDLGVBQU8sRUFBRSxDQUhLO0FBSWRDLGNBQU0sRUFBRSxDQUFDLENBSks7QUFNZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFORSxPQUFoQjtBQVFBakIsY0FBUSxDQUFDVyxLQUFULENBQWU7QUFDYkMsU0FBQyxFQUFFLENBRFU7QUFFYkMsU0FBQyxFQUFFLEdBRlU7QUFHYkMsZUFBTyxFQUFFLENBSEk7QUFJYkMsY0FBTSxFQUFFLENBQUMsQ0FKSTtBQUtiQyxrQkFBVSxFQUFFO0FBQUVDLGtCQUFRLEVBQUU7QUFBWjtBQUxDLE9BQWY7QUFPQWhCLGVBQVMsQ0FBQ1UsS0FBVixDQUFnQjtBQUNkQyxTQUFDLEVBQUUsQ0FEVztBQUVkQyxTQUFDLEVBQUUsR0FGVztBQUlkQyxlQUFPLEVBQUUsQ0FKSztBQUtkQyxjQUFNLEVBQUUsQ0FMTTtBQU1kQyxrQkFBVSxFQUFFO0FBQUVDLGtCQUFRLEVBQUU7QUFBWjtBQU5FLE9BQWhCO0FBUUFmLGNBQVEsQ0FBQ1MsS0FBVCxDQUFlO0FBQ2JDLFNBQUMsRUFBRSxDQURVO0FBRWJDLFNBQUMsRUFBRSxHQUZVO0FBSWJDLGVBQU8sRUFBRSxDQUpJO0FBS2JDLGNBQU0sRUFBRSxDQUxLO0FBTWJDLGtCQUFVLEVBQUU7QUFBRUMsa0JBQVEsRUFBRTtBQUFaO0FBTkMsT0FBZjtBQVFBZCxlQUFTLENBQUNRLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLENBRFc7QUFFZEMsU0FBQyxFQUFFLEdBRlc7QUFJZEMsZUFBTyxFQUFFLENBSks7QUFLZEMsY0FBTSxFQUFFLENBTE07QUFNZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFORSxPQUFoQjtBQVNBYixlQUFTLENBQUNPLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLEdBRFc7QUFFZEMsU0FBQyxFQUFFLENBRlc7QUFHZEMsZUFBTyxFQUFFLENBSEs7QUFJZEMsY0FBTSxFQUFFLENBSk07QUFLZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMRSxPQUFoQjtBQU9EO0FBQ0YsR0E5RlEsRUE4Rk4sQ0FBQ3ZCLE1BQUQsQ0E5Rk0sQ0FBVDtBQStGQSxzQkFDRTtBQUFBLDRCQUNFLDhEQUFDLDZDQUFEO0FBQ0UsV0FBSyxFQUFFLHNCQURUO0FBRUUsaUJBQVcsRUFBQyw2V0FGZDtBQUdFLGVBQVMsRUFBRTtBQUNUd0IsbUJBQVcsRUFDVCw2V0FGTztBQUdUQyxhQUFLLEVBQUUsc0JBSEU7QUFJVEMsWUFBSSxFQUFFLFNBSkc7QUFLVEMsY0FBTSxFQUFFLE9BTEM7QUFNVEMsV0FBRyxFQUFFQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsa0JBTlI7QUFPVEMsY0FBTSxFQUFFLENBQ047QUFDRUosYUFBRyxFQUFFLG9GQURQO0FBRUVLLGVBQUssRUFBRSxJQUZUO0FBR0VDLGdCQUFNLEVBQUUsR0FIVjtBQUlFQyxhQUFHLEVBQUU7QUFKUCxTQURNO0FBUEM7QUFIYjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFzQkU7QUFBTSxlQUFTLEVBQUMsU0FBaEI7QUFBQSw4QkFDRTtBQUNFLFVBQUUsRUFBQyxRQURMO0FBRUUsaUJBQVMsRUFBQywyRUFGWjtBQUFBLGdDQUlFLDhEQUFDLDhEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBSkYsZUFNRTtBQUFLLG1CQUFTLEVBQUMsNkVBQWY7QUFBQSxrQ0FDRTtBQUNFLGNBQUUsRUFBQyxNQURMO0FBRUUscUJBQVMsRUFBQyxzRkFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQVFFO0FBQU0scUJBQVMsRUFBQyx1Q0FBaEI7QUFBQSxzQkFDR2pDLGVBQWUsSUFBSUM7QUFEdEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFSRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBTkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFxQkU7QUFBSyxpQkFBUyxFQUFDLGlCQUFmO0FBQUEsZ0NBQ0U7QUFBSyxtQkFBUyxFQUFDLHNFQUFmO0FBQUEsaUNBQ0U7QUFBSyxxQkFBUyxFQUFDLGVBQWY7QUFBQSxvQ0FDRTtBQUFJLHVCQUFTLEVBQUMsaUdBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFJRTtBQUFTLHVCQUFTLEVBQUMsa0NBQW5CO0FBQUEsc0NBQ0U7QUFDRSx5QkFBUyxFQUFDLGVBRFo7QUFBQSx3Q0FFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFGQSxlQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUhBLGVBS0E7QUFBQSwwTEFBK0k7QUFBRywwQkFBTSxFQUFDLFFBQVY7QUFBbUIsd0JBQUksRUFBQyw4QkFBeEI7QUFBdUQsdUJBQUcsRUFBQyxZQUEzRDtBQUF3RSw2QkFBTSxXQUE5RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFBL0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUxBLGVBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBTkEsZUFPQTtBQUFBLHNIQUEyRTtBQUFHLDBCQUFNLEVBQUMsUUFBVjtBQUFtQix3QkFBSSxFQUFDLDhCQUF4QjtBQUF1RCx1QkFBRyxFQUFDLFlBQTNEO0FBQXdFLDZCQUFNLFdBQTlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQUEzRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURGLGVBVUU7QUFBSyx5QkFBUyxFQUFDLDJDQUFmO0FBQUEsMkJBQ0csR0FESCxlQUVFLDhEQUFDLHNEQUFEO0FBQ0UseUJBQU8sRUFBRTtBQUFFaUIsMkJBQU8sRUFBRSxDQUFYO0FBQWNjLDBCQUFNLEVBQUUsT0FBdEI7QUFBK0JELHlCQUFLLEVBQUU7QUFBdEMsbUJBRFg7QUFFRSx5QkFBTyxFQUFFO0FBQUViLDJCQUFPLEVBQUUsQ0FBWDtBQUFjYywwQkFBTSxFQUFFLE9BQXRCO0FBQStCRCx5QkFBSyxFQUFFO0FBQXRDLG1CQUZYO0FBR0UsMkJBQVMsRUFBQztBQUhaO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBRkYsZUFPRSw4REFBQyxtREFBRDtBQUNFLHFCQUFHLEVBQUUsTUFEUDtBQUVFLHFCQUFHLDRCQUZMO0FBR0UsdUJBQUssRUFBRSxHQUhUO0FBSUUsMkJBQVMsRUFBQyxVQUpaO0FBS0UsMkJBQVMsRUFBQyxTQUxaO0FBTUUsd0JBQU0sRUFBRTtBQU5WO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBUEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQVZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFKRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBbUNFO0FBQUssbUJBQVMsRUFBQyxxQkFBZjtBQUFBLGlDQUNFO0FBQUsscUJBQVMsRUFBQztBQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQW5DRixlQXVDRTtBQUNFLGFBQUcsRUFBRWxDLEdBRFA7QUFFRSxtQkFBUyxFQUFDLG1FQUZaO0FBQUEsb0JBSUdxQyw2REFBTyxDQUFDekIsU0FBRCxDQUFQLENBQW1CMEIsR0FBbkIsQ0FBdUIsVUFBQ0MsS0FBRCxFQUFRQyxLQUFSO0FBQUEsZ0NBQ3RCLDhEQUFDLHNEQUFEO0FBQ0UsdUJBQVMsRUFBQyw0QkFEWjtBQUVFLHFCQUFPLEVBQUVELEtBQUssQ0FBQ2xDLFNBRmpCO0FBQUEscUNBS0UsOERBQUMsbURBQUQ7QUFDRSxtQkFBRyxFQUFFLFNBRFA7QUFFRSxtQkFBRyxFQUFFLFVBQVVrQyxLQUFLLENBQUMxQixHQUZ2QjtBQUdFLHFCQUFLLEVBQUUsR0FIVDtBQUlFLHNCQUFNLEVBQUU7QUFKVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEYsZUFHTzJCLEtBSFA7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFEc0I7QUFBQSxXQUF2QjtBQUpIO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBdkNGLGVBMERFO0FBQUssbUJBQU0saUVBQVg7QUFBQSxrQ0FDRTtBQUFRLGNBQUUsRUFBQyxNQUFYO0FBQWtCLHFCQUFNLDhGQUF4QjtBQUFBLG1DQUF1SDtBQUFHLG9CQUFNLEVBQUMsUUFBVjtBQUFtQixrQkFBSSxFQUFDLG9GQUF4QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF2SDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBRUU7QUFBTSxxQkFBTTtBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQTFERixlQStERTtBQUFLLG1CQUFTLEVBQUMscUJBQWY7QUFBQSxpQ0FDRTtBQUFLLHFCQUFTLEVBQUM7QUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkEvREYsZUFrRUU7QUFDRSxZQUFFLEVBQUMsU0FETDtBQUVFLG1CQUFTLEVBQUM7QUFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQWxFRixlQXNFRTtBQUNFLFlBQUUsRUFBQyxTQURMO0FBRUUsbUJBQVMsRUFBQztBQUZaO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBdEVGLGVBMEVFO0FBQ0UsWUFBRSxFQUFDLFNBREw7QUFFRSxtQkFBUyxFQUFDO0FBRlo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkExRUYsZUE4RUU7QUFDRSxZQUFFLEVBQUMsU0FETDtBQUVFLG1CQUFTLEVBQUM7QUFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQTlFRixlQWtGRTtBQUFLLG1CQUFTLEVBQUMsc0VBQWY7QUFBQSxrQ0FDRTtBQUFLLHFCQUFTLEVBQUMsZUFBZjtBQUFBLG1DQUNFO0FBQ0UsdUJBQVMsRUFBQyxnQkFEWixDQUVFO0FBRkY7QUFBQSxxQ0FJRTtBQUFBLHVDQUNFO0FBQUEsdUZBQ3lELEdBRHpELGVBRUU7QUFDRSwwQkFBTSxFQUFDLFFBRFQ7QUFFRSx3QkFBSSxFQUFDLDZDQUZQO0FBR0UsdUJBQUcsRUFBQyxZQUhOO0FBSUUsNkJBQVMsRUFBQyxXQUpaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQUZGLEVBU08sR0FUUCxnREFXRTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQVhGLGVBWUU7QUFBTSw2QkFBUyxFQUFDLFdBQWhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQVpGLGVBYUU7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFiRix5SUFlVTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQWZWLG1NQWtCUTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQWxCUixnSEFtQjhDO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBbkI5QztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBREYsZUFpQ0UsOERBQUMsNkRBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFqQ0YsZUFrQ0U7QUFBSyxxQkFBUyxFQUFDO0FBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFsQ0YsZUFtQ0UsOERBQUMsOERBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFuQ0Y7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQWxGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0FyQkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBdEJGO0FBQUEsa0JBREY7QUF1S0Q7O0dBcFV1QjVDLEk7VUFDRUUsbUUsRUFDa0JJLHdELEVBRXhCSSx3RCxFQUNEQSx3RCxFQUNDQSx3RCxFQUNEQSx3RCxFQUNDQSx3RCxFQUNBQSx3RDs7O0tBVElWLEkiLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvaW5kZXguNGViMTUwNmYzMWI3ZjcyNWZkNTQuaG90LXVwZGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5leHRTZW8gfSBmcm9tIFwibmV4dC1zZW9cIjtcbmltcG9ydCBIZWFkZXIgZnJvbSBcIi4uL2NvbXBvbmVudHMvTGF5b3V0L0hlYWRlclwiO1xuaW1wb3J0IEZvb3RlciBmcm9tIFwiLi4vY29tcG9uZW50cy9MYXlvdXQvRm9vdGVyXCI7XG5pbXBvcnQgc2h1ZmZsZSBmcm9tIFwiLi4vdXRpbHMvc2h1ZmZsZS1pbWFnZVwiO1xuaW1wb3J0IEltYWdlIGZyb20gXCJuZXh0L2ltYWdlXCI7XG5pbXBvcnQgeyBtb3Rpb24sIHVzZUFuaW1hdGlvbiB9IGZyb20gXCJmcmFtZXItbW90aW9uXCI7XG5pbXBvcnQgeyBHZXRTdGF0aWNQcm9wcyB9IGZyb20gXCJuZXh0XCI7XG5pbXBvcnQgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgeyB1c2VJblZpZXcgfSBmcm9tIFwicmVhY3QtaW50ZXJzZWN0aW9uLW9ic2VydmVyXCI7XG5pbXBvcnQgdXNlQ291bnREb3duIGZyb20gXCIuLi9ob29rcy91c2VDb3VudERvd25cIjtcbmltcG9ydCBGb3VuZGVyIGZyb20gXCIuLi9jb21wb25lbnRzL0hvbWUvRm91bmRlclwiO1xuaW1wb3J0IGR5bmFtaWMgZnJvbSBcIm5leHQvZHluYW1pY1wiO1xuXG4vLyB0eXBlIFByb3BzID0ge1xuLy8gICBkYXRhOiB7XG4vLyAgICAgdGl0bGU6IHsgcmVuZGVyZWQ6IHN0cmluZyB9O1xuLy8gICAgIGNvbnRlbnQ6IHsgcmVuZGVyZWQ6IHN0cmluZyB9O1xuLy8gICAgIGFjZjoge1xuLy8gICAgICAgcHJvZmlsZV9pbWFnZV91cmw6IHN0cmluZztcbi8vICAgICAgIGZpcnN0X3NlY3Rpb246IHN0cmluZztcbi8vICAgICAgIHNlY29uZF9zZWN0aW9uOiBzdHJpbmc7XG4vLyAgICAgICB0aGlyZF9zZWN0aW9uOiBzdHJpbmc7XG4vLyAgICAgICBmb3VydGhfc2VjdGlvbjogc3RyaW5nO1xuLy8gICAgICAgdmFsdWVfZmlyc3Q6IHtcbi8vICAgICAgICAgaW1hZ2VfOiB7IHNpemVzOiB7IGxhcmdlOiBzdHJpbmcgfTsgdXJsOiBzdHJpbmcgfTtcbi8vICAgICAgICAgaW5mbzogc3RyaW5nO1xuLy8gICAgICAgfTtcbi8vICAgICAgIHZhbHVlX3NlY29uZDoge1xuLy8gICAgICAgICBpbWFnZV86IHsgc2l6ZXM6IHsgbGFyZ2U6IHN0cmluZyB9OyB1cmw6IHN0cmluZyB9O1xuLy8gICAgICAgICBpbmZvOiBzdHJpbmc7XG4vLyAgICAgICB9O1xuLy8gICAgICAgdmFsdWVfdGhpcmQ6IHtcbi8vICAgICAgICAgaW1hZ2VfOiBzdHJpbmc7XG4vLyAgICAgICAgIGluZm86IHN0cmluZztcbi8vICAgICAgIH07XG4vLyAgICAgICB2YWx1ZV9mb3VydGg6IHtcbi8vICAgICAgICAgaW1hZ2VfOiB7IHNpemVzOiB7IGxhcmdlOiBzdHJpbmcgfTsgdXJsOiBzdHJpbmcgfTtcbi8vICAgICAgICAgaW5mbzogc3RyaW5nO1xuLy8gICAgICAgfTtcbi8vICAgICAgIHZhbHVlX2ZpZnRoOiB7XG4vLyAgICAgICAgIGltYWdlXzogeyBzaXplczogeyBsYXJnZTogc3RyaW5nIH07IHVybDogc3RyaW5nIH07XG4vLyAgICAgICAgIGluZm86IHN0cmluZztcbi8vICAgICAgIH07XG4vLyAgICAgfTtcbi8vICAgfTtcbi8vIH07XG5jb25zdCBtc3BlYWtlciA9IFtcbiAgeyBuYW1lOiBcIkdhcnkgVmF5bmVyY2h1a1wiLCBpdGVtc1RvU2hvdzogXCJHYXJ5LVZheW5lcmNodWstbWluLmpwZ1wiIH0sXG4gIHsgbmFtZTogXCJBcmlhbm5hIEh1ZmZpbmd0b25cIiwgaXRlbXNUb1Nob3c6IFwiQXJpYW5uYS1IdWZmaW5ndG9uLW1pbi5qcGdcIiB9LFxuICB7IG5hbWU6IFwiUm9iZXJ0IEtpeW9zYWtpXCIsIGl0ZW1zVG9TaG93OiBcIlJvYmVydC1LaXlvc2FraS5qcGdcIiB9LFxuICB7IG5hbWU6IFwiUGF0cmljayBCZXQtRGF2aWRcIiwgaXRlbXNUb1Nob3c6IFwiUGF0cmljay1iZXQtZGF2aWQtbWluLmpwZ1wiIH0sXG4gIHsgbmFtZTogXCJUaW0gRmVycmlzc1wiLCBpdGVtc1RvU2hvdzogXCJUaW0tRmVycmlzcy1taW4ucG5nXCIgfSxcbiAgeyBuYW1lOiBcIlRhaSBMb3BlelwiLCBpdGVtc1RvU2hvdzogXCJUYWktTG9wZXotbWluLmpwZ1wiIH0sXG5dO1xuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSG9tZSh7IGRhdGEgfSkge1xuICBjb25zdCB7IHJlZiwgaW5WaWV3IH0gPSB1c2VJblZpZXcoeyB0aHJlc2hvbGQ6IDAuMiB9KTtcbiAgY29uc3QgeyBzaG93UmVsZWFzZURhdGUsIGNvdW5kb3duVGV4dCB9ID0gdXNlQ291bnREb3duKFwiMjAyMS0xMi0wNCAxMDowMCBBTVwiKTtcblxuICBjb25zdCBhbmltYXRpb24gPSB1c2VBbmltYXRpb24oKTtcbiAgY29uc3QgZmlyc3RJbWcgPSB1c2VBbmltYXRpb24oKTtcbiAgY29uc3Qgc2Vjb25kSW1nID0gdXNlQW5pbWF0aW9uKCk7XG4gIGNvbnN0IHRoaXJkSW1nID0gdXNlQW5pbWF0aW9uKCk7XG4gIGNvbnN0IGZvdXJ0aEltZyA9IHVzZUFuaW1hdGlvbigpO1xuICBjb25zdCBzbGlkZUxlZnQgPSB1c2VBbmltYXRpb24oKTtcbiAgY29uc3QgaW1hZ2VQYXRoID0gW1xuICAgIHsgc3JjOiBcInNsaWRlc2hvdy9ib3hlciAyLnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIHsgc3JjOiBcInNsaWRlc2hvdy9ib3hlci5wbmdcIiwgYW5pbWF0aW9uOiBmaXJzdEltZyB9LFxuICAgIHsgc3JjOiBcInNsaWRlc2hvdy9kZW5uaXMtMy5wbmdcIiwgYW5pbWF0aW9uOiBmb3VydGhJbWcgfSxcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvZGl2ZXIucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L21vYnN0ZXIucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvcGlsb3QucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvcGlyYXRlcyBjaGVldGFoLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L3J1c3NlbGwucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvc3VyZmVyLTIucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvdGlnZXIgd29vZHMucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvem9tYmllIGNoZWV0YWgucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvOTg5LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93Lzk5NS5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMDE0LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzEwMDcucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTAxNy5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMDUucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTA3LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzEwOC5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMDkucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTEwLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzExMi5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMjQucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTM5LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzE0MC5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy85Mi5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy85My5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvMS5naWZcIiwgYW5pbWF0aW9uOiBmaXJzdEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvMi5naWZcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzMucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzQucG5nXCIsIGFuaW1hdGlvbjogZm91cnRoSW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy81LnBuZ1wiLCBhbmltYXRpb246IGZpcnN0SW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy82LnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvNy5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvOC5wbmdcIiwgYW5pbWF0aW9uOiBmb3VydGhJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzkucG5nXCIsIGFuaW1hdGlvbjogZmlyc3RJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzEwLmpwZWdcIiwgYW5pbWF0aW9uOiBmaXJzdEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvMTEucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgLy8geyBzcmM6IFwiMTkucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgLy8geyBzcmM6IFwiMTM5LnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcIjE0Ny5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCIxNDgucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgLy8geyBzcmM6IFwiMTQ5LnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcIjU1MjEucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgLy8geyBzcmM6IFwiNTY5LnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcIjUyMC5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCIzMzUucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgLy8geyBzcmM6IFwiMTYyLnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICBdO1xuXG4gIGNvbnN0IFtzaG91bGRTaG93QWN0aW9ucywgc2V0U2hvdWxkU2hvd0FjdGlvbnNdID0gdXNlU3RhdGUodHJ1ZSk7XG4gIHVzZUVmZmVjdCgoKSA9PiB7XG4gICAgaWYgKGluVmlldykge1xuICAgICAgYW5pbWF0aW9uLnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMCxcbiAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgekluZGV4OiAxLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAwLjYgfSxcbiAgICAgIH0pO1xuICAgICAgZmlyc3RJbWcuc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAwLFxuICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDAuOCB9LFxuICAgICAgfSk7XG4gICAgICBzZWNvbmRJbWcuc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAwLFxuICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDAuOSB9LFxuICAgICAgfSk7XG4gICAgICB0aGlyZEltZy5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDAsXG4gICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgIHpJbmRleDogMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMS4yIH0sXG4gICAgICB9KTtcbiAgICAgIGZvdXJ0aEltZy5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDAsXG4gICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgIHpJbmRleDogMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMS40IH0sXG4gICAgICB9KTtcbiAgICAgIHNsaWRlTGVmdC5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDAsXG4gICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgIHpJbmRleDogMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMSB9LFxuICAgICAgfSk7XG4gICAgfVxuICAgIGlmICghaW5WaWV3KSB7XG4gICAgICBhbmltYXRpb24uc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAyNTgsXG4gICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgIHpJbmRleDogLTEsXG5cbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMSB9LFxuICAgICAgfSk7XG4gICAgICBmaXJzdEltZy5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDI1MixcbiAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgekluZGV4OiAtMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMSB9LFxuICAgICAgfSk7XG4gICAgICBzZWNvbmRJbWcuc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAyNTIsXG5cbiAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgekluZGV4OiAxLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAxIH0sXG4gICAgICB9KTtcbiAgICAgIHRoaXJkSW1nLnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMjUyLFxuXG4gICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgIHpJbmRleDogMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMSB9LFxuICAgICAgfSk7XG4gICAgICBmb3VydGhJbWcuc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAyNTIsXG5cbiAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgekluZGV4OiAxLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAxIH0sXG4gICAgICB9KTtcblxuICAgICAgc2xpZGVMZWZ0LnN0YXJ0KHtcbiAgICAgICAgeDogMTU2LFxuICAgICAgICB5OiAwLFxuICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDEgfSxcbiAgICAgIH0pO1xuICAgIH1cbiAgfSwgW2luVmlld10pO1xuICByZXR1cm4gKFxuICAgIDw+XG4gICAgICA8TmV4dFNlb1xuICAgICAgICB0aXRsZT17XCJIb21lIC0gQ29hbGl0aW9uY3Jld1wifVxuICAgICAgICBkZXNjcmlwdGlvbj1cIllvdXR1YmUgVHdpdHRlciBJbnN0YWdyYW0gV2VsY29tZSB0byB0aGUgQ29hbGl0aW9uIE1pbnQgaGVyZSBUaGlzIGlzIHRoZSBORlQgZm9yIEdhbWUgQ2hhbmdlcnMuIFRoZSBDb2FsaXRpb24gQ3JldyBpcyBhbiBleGNsdXNpdmUgY29sbGVjdGlvbiBvZiA3MTAwIHVuaXF1ZSBDaGVldGFoIE5GVHMgbGl2aW5nIG9uIHRoZSBFdGhlcmV1bSBibG9ja2NoYWluLiBJdOKAmXMgZXN0aW1hdGVkIHRoYXQgYXMgb2YgMjAyMSwgdGhlcmUgYXJlIG9ubHkgNzEwMCBjaGVldGFocyBsZWZ0IGluIHRoZSB3aWxkLiBDaGVldGFocyBhcmUgY3VycmVudGx5IGxpc3RlZCBhcyB2dWxuZXJhYmxlIGFuZCAmaGVsbGlwOyBIb21lIFJlYWQgTW9yZSAmcmFxdW87XCJcbiAgICAgICAgb3BlbkdyYXBoPXt7XG4gICAgICAgICAgZGVzY3JpcHRpb246XG4gICAgICAgICAgICBcIllvdXR1YmUgVHdpdHRlciBJbnN0YWdyYW0gV2VsY29tZSB0byB0aGUgQ29hbGl0aW9uIE1pbnQgaGVyZSBUaGlzIGlzIHRoZSBORlQgZm9yIEdhbWUgQ2hhbmdlcnMuIFRoZSBDb2FsaXRpb24gQ3JldyBpcyBhbiBleGNsdXNpdmUgY29sbGVjdGlvbiBvZiA3MTAwIHVuaXF1ZSBDaGVldGFoIE5GVHMgbGl2aW5nIG9uIHRoZSBFdGhlcmV1bSBibG9ja2NoYWluLiBJdOKAmXMgZXN0aW1hdGVkIHRoYXQgYXMgb2YgMjAyMSwgdGhlcmUgYXJlIG9ubHkgNzEwMCBjaGVldGFocyBsZWZ0IGluIHRoZSB3aWxkLiBDaGVldGFocyBhcmUgY3VycmVudGx5IGxpc3RlZCBhcyB2dWxuZXJhYmxlIGFuZCAmaGVsbGlwOyBIb21lIFJlYWQgTW9yZSAmcmFxdW87XCIsXG4gICAgICAgICAgdGl0bGU6IFwiSG9tZSAtIENvYWxpdGlvbmNyZXdcIixcbiAgICAgICAgICB0eXBlOiBcIndlYnNpdGVcIixcbiAgICAgICAgICBsb2NhbGU6IFwiZW5fVVNcIixcbiAgICAgICAgICB1cmw6IHByb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0RPTUFJTixcbiAgICAgICAgICBpbWFnZXM6IFtcbiAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgdXJsOiBcImh0dHA6Ly9jb2FsaXRpb25jcmV3LmNvbS93cC1jb250ZW50L3VwbG9hZHMvMjAyMS8xMC9UQ0NfMS1UcmFuc3BhcmVudC0xMDI0eDk4Mi5wbmdcIixcbiAgICAgICAgICAgICAgd2lkdGg6IDEyMDAsXG4gICAgICAgICAgICAgIGhlaWdodDogNjMwLFxuICAgICAgICAgICAgICBhbHQ6IFwiY29hbGl0aW9uIGNyZXcgbG9nb1wiLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICBdLFxuICAgICAgICB9fVxuICAgICAgLz5cblxuICAgICAgPG1haW4gY2xhc3NOYW1lPVwibXgtYXV0b1wiPlxuICAgICAgICA8ZGl2XG4gICAgICAgICAgaWQ9XCJiYW5uZXJcIlxuICAgICAgICAgIGNsYXNzTmFtZT1cIiByZWxhdGl2ZSBiZy1jcmV3IHctZnVsbCAgaC1zY3JlZW4gYmctY292ZXIgZmxleCBmbGV4LWNvbCBqdXN0aWZ5LWJldHdlZW5cIlxuICAgICAgICA+XG4gICAgICAgICAgPEhlYWRlciAvPlxuXG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4IGZsZXgtY29sICBtZDphYnNvbHV0ZSByaWdodC02NCBib3R0b20tMTEgICBqdXN0aWZ5LWNlbnRlciBpdGVtcy1jZW50ZXJcIj5cbiAgICAgICAgICAgIDxidXR0b25cbiAgICAgICAgICAgICAgaWQ9XCJjdGEyXCJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiICB0ZXh0LTN4bCBiZy15ZWxsb3ctMzAwIHVwcGVyY2FzZSBpdGFsaWMgZm9udC1ib2xkICBtYi0yIHB4LTE2IHB5LTQgIHRleHQtaWNvbkNvbG9yXCJcbiAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgTWludCBIZXJlXG4gICAgICAgICAgICA8L2J1dHRvbj5cblxuICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwidGV4dC0yeGwgZm9udC1ib2xkIHRleHQtZ3JheS04MDAgbWItNFwiPlxuICAgICAgICAgICAgICB7c2hvd1JlbGVhc2VEYXRlICYmIGNvdW5kb3duVGV4dH1cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCIgdGV4dC1ncmF5LTgwMCBcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInBvc3QtY29udGVudCBjb250YWluZXIgbXgtYXV0byB0ZXh0LWxnIHAtNCB0ZXh0LWNlbnRlciBtZDp0ZXh0LWxlZnQgXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZpcnN0LXNlY3Rpb25cIj5cbiAgICAgICAgICAgICAgPGgxIGNsYXNzTmFtZT1cInRleHQtZ3JheS05MDAgdGV4dC1sZWZ0IG91ci1taXNzaW9uIGZvbnQtYm9sZCB0ZXh0LTN4bCBib3JkZXItYi0yIHBiLTMgYm9yZGVyLWdyYXktODAwIG1kOnctMS80XCI+XG4gICAgICAgICAgICAgICAgT3VyIE1pc3Npb25cbiAgICAgICAgICAgICAgPC9oMT5cbiAgICAgICAgICAgICAgPHNlY3Rpb24gY2xhc3NOYW1lPVwiZ3JpZCBtZDpncmlkLWNvbHMtMiBpdGVtcy1jZW50ZXJcIj5cbiAgICAgICAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJhYm91dC1taXNzaW9uXCI+ICAgICAgICAgIFxuICAgICAgICAgICAgICAgIDxwPkhpISBXZWxjb21lIHRvIHRoZSBDb2FsaXRpb24gQ3JldyEgVGhpcyBpcyBhIGxpbWl0ZWQgY29sbGVjdGlvbiBvZiA1MDAwIHVuaXF1ZSBDaGVldGFoIE5GVHMgbGl2aW5nIG9uIHRoZSBFdGhlcmV1bSBibG9ja2NoYWluLiBUaGlzIHByb2plY3QgaXMgdGVjaG5pY2FsbHkgYnJva2VuIHVwIGludG8gdGhyZWUgY29sbGVjdGlvbnMuPC9wPlxuICAgICAgICAgICAgICAgIDxwPjFzdCBDb2xsZWN0aW9uIC0gQ29hbGl0aW9uIENyZXcgKE9HIENPTExFQ1RJT04pIE9ubHkgMTAxMCBhdmFpbC5cbipVcGRhdGUgLSBTb2xkIG91dCBpbiBsZXNzIHRoYW4gYSBkYXkhKjwvcD5cbiAgICAgICAgICAgICAgICA8cD4ybmQgQ29sbGVjdGlvbiAtIEF1Y3Rpb24gQ2hlZXRhaHMgLSAyMCBhdmFpbC4gVGhlc2UgcXVhbGlmeSBmb3IgVklQIGFjY2VzcyB0byBhbGwgbGl2ZSBldmVudHMuIFRoZSBhdWN0aW9uIGJlZ2lucyBKYW4gOHRoLiBQbGVhc2UgdmlzaXQgb3VyIDxhIHRhcmdldD1cIl9ibGFua1wiIGhyZWY9XCJodHRwOi8vZGlzY29yZC5nZy8zbktSQmNEUzMzXCIgcmVsPVwibm9yZWZlcnJlclwiIGNsYXNzPVwidW5kZXJsaW5lXCI+IERpc2NvcmQ8L2E+IGZvciBtb3JlIGluZm8gb24gdGhpcyEgPC9wPlxuICAgICAgICAgICAgICAgIDxwPjNyZCBDb2xsZWN0aW9uIC0gQ29hbGl0aW9uIENyZXcgMi4wIC0gT25seSAzOTcwIGF2YWlsLiBUaGlzIHdpbGwgd3JhcCB1cCB0aGUgY29sbGVjdGlvbiBmb3IgdGhlIENvYWxpdGlvbiBDcmV3IHByb2plY3QuIE1pbnQgcHJpY2UgaXMgLjA5IEVUSCBhbmQgeW91IGNhbiBtaW50IHVwIHRvIDUuIFRoaXMgY29sbGVjdGlvbiB3aWxsIGJlZ2luIG1pbnRpbmcgSmFuIDI2dGggYXQgMTBhbSBQU1QvMXBtIEVTVC48L3A+XG4gICAgICAgICAgICAgICAgPHA+T3V0IG9mIHRoZSA1MDAwIHRvdGFsLCBvbmx5IDUwIHF1YWxpZnkgZm9yIFZJUCBhY2Nlc3MuIFBsZWFzZSB2aXNpdCBvdXIgPGEgdGFyZ2V0PVwiX2JsYW5rXCIgaHJlZj1cImh0dHA6Ly9kaXNjb3JkLmdnLzNuS1JCY0RTMzNcIiByZWw9XCJub3JlZmVycmVyXCIgY2xhc3M9XCJ1bmRlcmxpbmVcIj4gRGlzY29yZDwvYT4gRGlzY29yZCBmb3IgbW9yZSBpbmZvIG9uIHdoaWNoIG9uZXMgcXVhbGlmeSBmb3IgVklQLjwvcD5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXggaXRlbXMtY2VudGVyIGp1c3RpZnktY2VudGVyIHJlbGF0aXZlXCI+XG4gICAgICAgICAgICAgICAgICB7XCIgXCJ9XG4gICAgICAgICAgICAgICAgICA8bW90aW9uLmRpdlxuICAgICAgICAgICAgICAgICAgICBpbml0aWFsPXt7IG9wYWNpdHk6IDAsIGhlaWdodDogXCIyMHJlbVwiLCB3aWR0aDogXCIyMHJlbVwiIH19XG4gICAgICAgICAgICAgICAgICAgIGFuaW1hdGU9e3sgb3BhY2l0eTogMSwgaGVpZ2h0OiBcIjIycmVtXCIsIHdpZHRoOiBcIjIycmVtXCIgfX1cbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiaC03MiAgZHVyYXRpb24tMzAwIHctNzIgbWQ6dy05NiBtZDpoLTk2IGJnLWdyYXktODAwIHJvdW5kZWQtZnVsbCBhYnNvbHV0ZSB6LTAgbWQ6bGVmdC0zOFwiXG4gICAgICAgICAgICAgICAgICA+PC9tb3Rpb24uZGl2PlxuICAgICAgICAgICAgICAgICAgPEltYWdlXG4gICAgICAgICAgICAgICAgICAgIGFsdD17XCJsb2dvXCJ9XG4gICAgICAgICAgICAgICAgICAgIHNyYz17YC9pbWcvcnVubmluZy1jaGVldGFoLmdpZmB9XG4gICAgICAgICAgICAgICAgICAgIHdpZHRoPXs0MDB9XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInJlbGF0aXZlXCJcbiAgICAgICAgICAgICAgICAgICAgb2JqZWN0Rml0PVwiY29udGFpblwiXG4gICAgICAgICAgICAgICAgICAgIGhlaWdodD17NDUwfVxuICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4IGp1c3RpZnktY2VudGVyXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cInctMzYgaC0yIGZsZXggIGJnLWdyYXktNzAwIG15LTlcIj48L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICBcbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICByZWY9e3JlZn1cbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImdhbGxlcnkgcmVsYXRpdmUganVzdGlmeS1pdGVtcy1jZW50ZXIgZmxleCBnYXAtOCAgbWQ6Z3JpZC1jb2xzLTQgXCJcbiAgICAgICAgICA+XG4gICAgICAgICAgICB7c2h1ZmZsZShpbWFnZVBhdGgpLm1hcCgoaW1hZ2UsIGluZGV4KSA9PiAoXG4gICAgICAgICAgICAgIDxtb3Rpb24uZGl2XG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiaXRlbSBtaW4tdy1mdWxsIHJvdW5kZWQtbGdcIlxuICAgICAgICAgICAgICAgIGFuaW1hdGU9e2ltYWdlLmFuaW1hdGlvbn1cbiAgICAgICAgICAgICAgICBrZXk9e2luZGV4fVxuICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgPEltYWdlXG4gICAgICAgICAgICAgICAgICBhbHQ9e1wiQ2hlZXRhaFwifVxuICAgICAgICAgICAgICAgICAgc3JjPXtcIi9pbWcvXCIgKyBpbWFnZS5zcmN9XG4gICAgICAgICAgICAgICAgICB3aWR0aD17NjIwfVxuICAgICAgICAgICAgICAgICAgaGVpZ2h0PXs2NTB9XG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9tb3Rpb24uZGl2PlxuICAgICAgICAgICAgKSl9XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzcz1cImZsZXggZmxleC1jb2wgIHJpZ2h0LTY0IGJvdHRvbS0xMSAgIGp1c3RpZnktY2VudGVyIGl0ZW1zLWNlbnRlclwiPlxuICAgICAgICAgICAgPGJ1dHRvbiBpZD1cImN0YTJcIiBjbGFzcz1cIiByb2FkbWFwICB0ZXh0LTN4bCBiZy15ZWxsb3ctMzAwIHVwcGVyY2FzZSBpdGFsaWMgZm9udC1ib2xkICBtYi0yIHB4LTE2IHB5LTQgIHRleHQtaWNvbkNvbG9yXCI+PGEgdGFyZ2V0PVwiX2JsYW5rXCIgaHJlZj1cImh0dHBzOi8vZHJpdmUuZ29vZ2xlLmNvbS9maWxlL2QvMUpQQVhYTGxSLXZUcGVHZTNySG14ZXE0WEMzVVMzaHY1L3ZpZXc/dXNwPXNoYXJpbmdcIj5Sb2FkbWFwPC9hPjwvYnV0dG9uPlxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJ0ZXh0LTJ4bCBmb250LWJvbGQgdGV4dC1ncmF5LTgwMCBtYi00XCI+PC9zcGFuPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICBcbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgganVzdGlmeS1jZW50ZXJcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWF4LXctNnhsIG92ZXJmbG93LWhpZGRlbiB3LTM2IGgtMiBmbGV4IG10LTggIGJnLWdyYXktODAwIFwiPjwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGlkPVwiYmFubmVyMlwiXG4gICAgICAgICAgICBjbGFzc05hbWU9XCIgcmVsYXRpdmUgYmctdmFsdWUgdy1mdWxsICBoLXNjcmVlbiBiZy1jb3ZlciBmbGV4IGZsZXgtY29sIGp1c3RpZnktYmV0d2VlbiBcIlxuICAgICAgICAgID48L2Rpdj5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBpZD1cImJhbm5lcjNcIlxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiIHJlbGF0aXZlIGJnLW1lbnRvciB3LWZ1bGwgIGgtc2NyZWVuIGJnLWNvdmVyIGZsZXggZmxleC1jb2wganVzdGlmeS1iZXR3ZWVuIG15LTEwXCJcbiAgICAgICAgICA+PC9kaXY+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgaWQ9XCJiYW5uZXI0XCJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cIiByZWxhdGl2ZSBiZy1uZXR3b3JrMSB3LWZ1bGwgIGgtc2NyZWVuIGJnLWNvdmVyIGZsZXggZmxleC1jb2wganVzdGlmeS1iZXR3ZWVuIG1iLTEwXCJcbiAgICAgICAgICA+PC9kaXY+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgaWQ9XCJiYW5uZXI1XCJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cIiByZWxhdGl2ZSBiZy1uZXR3b3JrMiB3LWZ1bGwgIGgtc2NyZWVuIGJnLWNvdmVyIGZsZXggZmxleC1jb2wganVzdGlmeS1iZXR3ZWVuIG1iLTEwXCJcbiAgICAgICAgICA+PC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwb3N0LWNvbnRlbnQgY29udGFpbmVyIG14LWF1dG8gdGV4dC1sZyBwLTQgdGV4dC1jZW50ZXIgbWQ6dGV4dC1sZWZ0IFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmaXJzdC1zZWN0aW9uXCI+XG4gICAgICAgICAgICAgIDxzZWN0aW9uXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm91cnRoLXNlY3Rpb25cIlxuICAgICAgICAgICAgICAgIC8vIGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogZGF0YS5hY2YuZm91cnRoX3NlY3Rpb24gfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgICAgT25jZSBtaW50ZWQgb3IgYm91Z2h0IG9uIE9wZW5TZWEsIHBsZWFzZSBmaWxsIG91dCB0aGlze1wiIFwifVxuICAgICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vd3d3LmdhbWVjaGFuZ2Vyc21vdmVtZW1lbnQuY29tL2NyZXdcIlxuICAgICAgICAgICAgICAgICAgICAgIHJlbD1cIm5vcmVmZXJyZXJcIlxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInVuZGVybGluZVwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICBmb3JtXG4gICAgICAgICAgICAgICAgICAgIDwvYT57XCIgXCJ9XG4gICAgICAgICAgICAgICAgICAgIHRvIGdldCBhY2Nlc3MgdG8gdGhlIEFjYWRlbXkuXG4gICAgICAgICAgICAgICAgICAgIDxiciAvPlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJmb250LWJvbGRcIj5Ob3RlITwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPGJyIC8+IElmIHlvdSBzZWxsIHlvdXIgb25seSBjaGVldGFoLCB5b3VyIGFjY2VzcyB0byB0aGVcbiAgICAgICAgICAgICAgICAgICAgQWNhZGVteSB3aWxsIGJlIHN1c3BlbmRlZC4gWW91IG11c3Qgb3duIGF0IGxlYXN0IDEgdG8ga2VlcFxuICAgICAgICAgICAgICAgICAgICBhY2Nlc3MuIDxiciAvPiBBZnRlciAzIG1vbnRocyBhbGwgaG9sZGVycyB3aWxsIHJlY2VpdmUgdGhlaXJcbiAgICAgICAgICAgICAgICAgICAgY2hlZXRhaCBhZG9wdGlvbiDigJxwYXBlcndvcmvigJ0uIFRoaXMgaW5jbHVkZXMgYSBwaG90bywgYW5cbiAgICAgICAgICAgICAgICAgICAgYWRvcHRpb24gY2VydGlmaWNhdGUsIGEgc3R1ZmZlZCBhbmltYWwsIGEgc3BlY2llcyBjYXJkICtcbiAgICAgICAgICAgICAgICAgICAgbW9yZSEgPGJyIC8+IFdlIGFyZSBsYXVuY2hpbmcgYSB1bmlxdWUgbWVyY2hhbmRpc2Ugc3RvcmUgZm9yXG4gICAgICAgICAgICAgICAgICAgIENvYWxpdGlvbiBDcmV3IE1lbWJlcnMgdG8gbWFrZSBjdXN0b20gZ2Vhci4gPGJyIC8+IEFsbFxuICAgICAgICAgICAgICAgICAgICBob2xkZXJzIGdldCBhdXRvbWF0aWMgV0wgZm9yIGZ1dHVyZSBwcm9qZWN0cywgYXMgd2VsbCBhc1xuICAgICAgICAgICAgICAgICAgICBleGNsdXNpdmUgYWlyZHJvcHMuXG4gICAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvc2VjdGlvbj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPEZvdW5kZXIgLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaC0wLjUgdy1mdWxsIGJnLWdyYXktNzAwIG15LThcIj48L2Rpdj5cbiAgICAgICAgICAgIDxGb290ZXIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L21haW4+XG4gICAgPC8+XG4gICk7XG59XG5cbmV4cG9ydCBjb25zdCBnZXRTdGF0aWNQcm9wcyA9IGFzeW5jICgpID0+IHtcbiAgY29uc3QgZGF0YSA9IGF3YWl0IGZldGNoKFxuICAgIGAke3Byb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0JMT0dfVVJMfS93cC92Mi9wYWdlcy80ODc3YFxuICApLnRoZW4oKTtcbiAgY29uc3QgaG9tZSA9IGF3YWl0IGRhdGEuanNvbigpO1xuXG4gIHJldHVybiB7XG4gICAgcHJvcHM6IHtcbiAgICAgIGRhdGE6IGhvbWUsXG4gICAgfSxcbiAgfTtcbn07XG4iXSwic291cmNlUm9vdCI6IiJ9