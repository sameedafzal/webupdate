self["webpackHotUpdate_N_E"]("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "__N_SSG": function() { return /* binding */ __N_SSG; },
/* harmony export */   "default": function() { return /* binding */ Home; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next-seo */ "./node_modules/next-seo/lib/next-seo.module.js");
/* harmony import */ var _components_Layout_Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Layout/Header */ "./components/Layout/Header.js");
/* harmony import */ var _components_Layout_Footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Layout/Footer */ "./components/Layout/Footer.js");
/* harmony import */ var _utils_shuffle_image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/shuffle-image */ "./utils/shuffle-image.js");
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/image */ "./node_modules/next/image.js");
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var framer_motion__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! framer-motion */ "./node_modules/framer-motion/dist/es/index.mjs");
/* harmony import */ var next__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next */ "next");
/* harmony import */ var next__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_intersection_observer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-intersection-observer */ "./node_modules/react-intersection-observer/react-intersection-observer.m.js");
/* harmony import */ var _hooks_useCountDown__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../hooks/useCountDown */ "./hooks/useCountDown.js");
/* harmony import */ var _components_Home_Founder__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/Home/Founder */ "./components/Home/Founder.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_10__);
/* module decorator */ module = __webpack_require__.hmd(module);
/* provided dependency */ var process = __webpack_require__(/*! process */ "./node_modules/process/browser.js");



var _jsxFileName = "C:\\Users\\samee\\Desktop\\crew-main\\pages\\index.js",
    _s = $RefreshSig$();












 // type Props = {
//   data: {
//     title: { rendered: string };
//     content: { rendered: string };
//     acf: {
//       profile_image_url: string;
//       first_section: string;
//       second_section: string;
//       third_section: string;
//       fourth_section: string;
//       value_first: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//       value_second: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//       value_third: {
//         image_: string;
//         info: string;
//       };
//       value_fourth: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//       value_fifth: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//     };
//   };
// };

var mspeaker = [{
  name: "Gary Vaynerchuk",
  itemsToShow: "Gary-Vaynerchuk-min.jpg"
}, {
  name: "Arianna Huffington",
  itemsToShow: "Arianna-Huffington-min.jpg"
}, {
  name: "Robert Kiyosaki",
  itemsToShow: "Robert-Kiyosaki.jpg"
}, {
  name: "Patrick Bet-David",
  itemsToShow: "Patrick-bet-david-min.jpg"
}, {
  name: "Tim Ferriss",
  itemsToShow: "Tim-Ferriss-min.png"
}, {
  name: "Tai Lopez",
  itemsToShow: "Tai-Lopez-min.jpg"
}];
var __N_SSG = true;
function Home(_ref) {
  _s();

  var _this = this;

  var data = _ref.data;

  var _useInView = (0,react_intersection_observer__WEBPACK_IMPORTED_MODULE_11__.useInView)({
    threshold: 0.2
  }),
      ref = _useInView.ref,
      inView = _useInView.inView;

  var _useCountDown = (0,_hooks_useCountDown__WEBPACK_IMPORTED_MODULE_8__.default)("2021-12-04 10:00 AM"),
      showReleaseDate = _useCountDown.showReleaseDate,
      coundownText = _useCountDown.coundownText;

  var animation = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var firstImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var secondImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var thirdImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var fourthImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var slideLeft = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var imagePath = [{
    src: "slideshow/boxer 2.png",
    animation: secondImg
  }, {
    src: "slideshow/boxer.png",
    animation: firstImg
  }, {
    src: "slideshow/dennis-3.png",
    animation: fourthImg
  }, {
    src: "slideshow/diver.png",
    animation: secondImg
  }, {
    src: "slideshow/mobster.png",
    animation: thirdImg
  }, {
    src: "slideshow/pilot.png",
    animation: thirdImg
  }, {
    src: "slideshow/pirates cheetah.png",
    animation: thirdImg
  }, {
    src: "slideshow/russell.png",
    animation: thirdImg
  }, {
    src: "slideshow/surfer-2.png",
    animation: thirdImg
  }, {
    src: "slideshow/tiger woods.png",
    animation: thirdImg
  }, {
    src: "slideshow/zombie cheetah.png",
    animation: thirdImg
  }, // { src: "slideshow/989.png", animation: thirdImg },
  // { src: "slideshow/995.png", animation: thirdImg },
  // { src: "slideshow/1014.png", animation: thirdImg },
  // { src: "slideshow/1007.png", animation: thirdImg },
  // { src: "slideshow/1017.png", animation: thirdImg },
  // { src: "slideshow/105.png", animation: thirdImg },
  // { src: "slideshow/107.png", animation: thirdImg },
  // { src: "slideshow/108.png", animation: thirdImg },
  // { src: "slideshow/109.png", animation: thirdImg },
  // { src: "slideshow/110.png", animation: thirdImg },
  // { src: "slideshow/112.png", animation: thirdImg },
  // { src: "slideshow/124.png", animation: thirdImg },
  // { src: "slideshow/139.png", animation: thirdImg },
  // { src: "slideshow/140.png", animation: thirdImg },
  // { src: "slideshow/92.png", animation: thirdImg },
  // { src: "slideshow/93.png", animation: thirdImg },
  {
    src: "slideshow/1.gif",
    animation: firstImg
  }, {
    src: "slideshow/2.gif",
    animation: secondImg
  }, {
    src: "slideshow/3.png",
    animation: thirdImg
  }, {
    src: "slideshow/4.png",
    animation: fourthImg
  }, {
    src: "slideshow/5.png",
    animation: firstImg
  }, {
    src: "slideshow/6.png",
    animation: secondImg
  }, {
    src: "slideshow/7.png",
    animation: thirdImg
  }, {
    src: "slideshow/8.png",
    animation: fourthImg
  }, {
    src: "slideshow/9.png",
    animation: firstImg
  }, {
    src: "slideshow/10.jpeg",
    animation: firstImg
  }, {
    src: "slideshow/11.png",
    animation: secondImg
  } // { src: "19.png", animation: secondImg },
  // { src: "139.png", animation: secondImg },
  // { src: "147.png", animation: secondImg },
  // { src: "148.png", animation: secondImg },
  // { src: "149.png", animation: secondImg },
  // { src: "5521.png", animation: secondImg },
  // { src: "569.png", animation: secondImg },
  // { src: "520.png", animation: secondImg },
  // { src: "335.png", animation: secondImg },
  // { src: "162.png", animation: secondImg },
  ];

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_7__.useState)(true),
      shouldShowActions = _useState[0],
      setShouldShowActions = _useState[1];

  (0,react__WEBPACK_IMPORTED_MODULE_7__.useEffect)(function () {
    if (inView) {
      animation.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 0.6
        }
      });
      firstImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 0.8
        }
      });
      secondImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 0.9
        }
      });
      thirdImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 1.2
        }
      });
      fourthImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 1.4
        }
      });
      slideLeft.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
    }

    if (!inView) {
      animation.start({
        x: 0,
        y: 258,
        opacity: 0,
        zIndex: -1,
        transition: {
          duration: 1
        }
      });
      firstImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: -1,
        transition: {
          duration: 1
        }
      });
      secondImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
      thirdImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
      fourthImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
      slideLeft.start({
        x: 156,
        y: 0,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
    }
  }, [inView]);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(next_seo__WEBPACK_IMPORTED_MODULE_1__.NextSeo, {
      title: "Home - Coalitioncrew",
      description: "Youtube Twitter Instagram Welcome to the Coalition Mint here This is the NFT for Game Changers. The Coalition Crew is an exclusive collection of 7100 unique Cheetah NFTs living on the Ethereum blockchain. It\u2019s estimated that as of 2021, there are only 7100 cheetahs left in the wild. Cheetahs are currently listed as vulnerable and \u2026 Home Read More \xBB",
      openGraph: {
        description: "Youtube Twitter Instagram Welcome to the Coalition Mint here This is the NFT for Game Changers. The Coalition Crew is an exclusive collection of 7100 unique Cheetah NFTs living on the Ethereum blockchain. It’s estimated that as of 2021, there are only 7100 cheetahs left in the wild. Cheetahs are currently listed as vulnerable and &hellip; Home Read More &raquo;",
        title: "Home - Coalitioncrew",
        type: "website",
        locale: "en_US",
        url: process.env.NEXT_PUBLIC_DOMAIN,
        images: [{
          url: "http://coalitioncrew.com/wp-content/uploads/2021/10/TCC_1-Transparent-1024x982.png",
          width: 1200,
          height: 630,
          alt: "coalition crew logo"
        }]
      }
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 214,
      columnNumber: 7
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("main", {
      className: "mx-auto",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        id: "banner",
        className: " relative bg-crew w-full  h-screen bg-cover flex flex-col justify-between",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_Layout_Header__WEBPACK_IMPORTED_MODULE_2__.default, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 240,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "flex flex-col  md:absolute right-64 bottom-11   justify-center items-center",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            id: "cta2",
            className: "  text-3xl bg-yellow-300 uppercase italic font-bold  mb-2 px-16 py-4  text-iconColor",
            children: "Mint Here"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 243,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "text-2xl font-bold text-gray-800 mb-4",
            children: showReleaseDate && coundownText
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 250,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 242,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 236,
        columnNumber: 9
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: " text-gray-800 ",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "post-content container mx-auto text-lg p-4 text-center md:text-left ",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "first-section",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h1", {
              className: "text-gray-900 text-left our-mission font-bold text-3xl border-b-2 pb-3 border-gray-800 md:w-1/4",
              children: "Our Mission"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 259,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("section", {
              className: "grid md:grid-cols-2 items-center",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "about-mission",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: "Hi! Welcome to the Coalition Crew! This is a limited collection of 5000 unique Cheetah NFTs living on the Ethereum blockchain. This project is technically broken up into three collections."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 265,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: "1st Collection - Coalition Crew (OG COLLECTION) Only 1010 avail. *Update - Sold out in less than a day!*"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 266,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: ["2nd Collection - Auction Cheetahs - 20 avail. These qualify for VIP access to all live events. The auction begins Jan 8th. Please visit our ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
                    target: "_blank",
                    href: "http://discord.gg/3nKRBcDS33",
                    rel: "noreferrer",
                    "class": "underline",
                    children: " Discord"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 268,
                    columnNumber: 160
                  }, this), " for more info on this! "]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 268,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: "3rd Collection - Coalition Crew 2.0 - Only 3970 avail. This will wrap up the collection for the Coalition Crew project. Mint price is .09 ETH and you can mint up to 5. This collection will begin minting Jan 26th at 10am PST/1pm EST."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 269,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: ["Out of the 5000 total, only 50 qualify for VIP access. Please visit our ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
                    target: "_blank",
                    href: "http://discord.gg/3nKRBcDS33",
                    rel: "noreferrer",
                    "class": "underline",
                    children: " Discord"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 270,
                    columnNumber: 92
                  }, this), " Discord for more info on which ones qualify for VIP."]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 270,
                  columnNumber: 17
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 263,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "flex items-center justify-center relative",
                children: [" ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(framer_motion__WEBPACK_IMPORTED_MODULE_12__.motion.div, {
                  initial: {
                    opacity: 0,
                    height: "20rem",
                    width: "20rem"
                  },
                  animate: {
                    opacity: 1,
                    height: "22rem",
                    width: "22rem"
                  },
                  className: "h-72  duration-300 w-72 md:w-96 md:h-96 bg-gray-800 rounded-full absolute z-0 md:left-38"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 274,
                  columnNumber: 19
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_image__WEBPACK_IMPORTED_MODULE_5___default()), {
                  alt: "logo",
                  src: "/img/running-cheetah.gif",
                  width: 400,
                  className: "relative",
                  objectFit: "contain",
                  height: 450
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 279,
                  columnNumber: 19
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 272,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 262,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 258,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 257,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "flex justify-center",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "w-36 h-2 flex  bg-gray-700 my-9"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 292,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 291,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          ref: ref,
          className: "gallery relative justify-items-center flex gap-8  md:grid-cols-4 ",
          children: (0,_utils_shuffle_image__WEBPACK_IMPORTED_MODULE_4__.default)(imagePath).map(function (image, index) {
            return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(framer_motion__WEBPACK_IMPORTED_MODULE_12__.motion.div, {
              className: "item min-w-full rounded-lg",
              animate: image.animation,
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_image__WEBPACK_IMPORTED_MODULE_5___default()), {
                alt: "Cheetah",
                src: "/img/" + image.src,
                width: 620,
                height: 650
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 305,
                columnNumber: 17
              }, _this)
            }, index, false, {
              fileName: _jsxFileName,
              lineNumber: 300,
              columnNumber: 15
            }, _this);
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 295,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          "class": "flex flex-col  right-64 bottom-11   justify-center items-center",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            id: "cta2",
            "class": "  text-3xl bg-yellow-300 uppercase italic font-bold  mb-2 px-16 py-4  text-iconColor",
            children: "Mint Here"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 314,
            columnNumber: 88
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            "class": "text-2xl font-bold text-gray-800 mb-4"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 314,
            columnNumber: 217
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 314,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner2",
          "class": " relative bg-value w-full  h-screen bg-cover flex flex-col justify-between "
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 315,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "flex justify-center",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "max-w-6xl overflow-hidden w-36 h-2 flex mt-8  bg-gray-800 "
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 319,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 318,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner2",
          className: " relative bg-value w-full  h-screen bg-cover flex flex-col justify-between "
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 321,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner3",
          className: " relative bg-mentor w-full  h-screen bg-cover flex flex-col justify-between my-10"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 325,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner4",
          className: " relative bg-network1 w-full  h-screen bg-cover flex flex-col justify-between mb-10"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 329,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner5",
          className: " relative bg-network2 w-full  h-screen bg-cover flex flex-col justify-between mb-10"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 333,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "post-content container mx-auto text-lg p-4 text-center md:text-left ",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "first-section",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("section", {
              className: "fourth-section" // dangerouslySetInnerHTML={{ __html: data.acf.fourth_section }}
              ,
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: ["Once minted or bought on OpenSea, please fill out this", " ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
                    target: "_blank",
                    href: "https://www.gamechangersmovemement.com/crew",
                    rel: "noreferrer",
                    className: "underline",
                    children: "form"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 346,
                    columnNumber: 21
                  }, this), " ", "to get access to the Academy.", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 355,
                    columnNumber: 21
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                    className: "font-bold",
                    children: "Note!"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 356,
                    columnNumber: 21
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 357,
                    columnNumber: 21
                  }, this), " If you sell your only cheetah, your access to the Academy will be suspended. You must own at least 1 to keep access. ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 359,
                    columnNumber: 29
                  }, this), " After 3 months all holders will receive their cheetah adoption \u201Cpaperwork\u201D. This includes a photo, an adoption certificate, a stuffed animal, a species card + more! ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 362,
                    columnNumber: 27
                  }, this), " We are launching a unique merchandise store for Coalition Crew Members to make custom gear. ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 363,
                    columnNumber: 65
                  }, this), " All holders get automatic WL for future projects, as well as exclusive airdrops."]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 344,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 343,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 339,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 338,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_Home_Founder__WEBPACK_IMPORTED_MODULE_9__.default, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 370,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "h-0.5 w-full bg-gray-700 my-8"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 371,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_Layout_Footer__WEBPACK_IMPORTED_MODULE_3__.default, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 372,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 337,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 256,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 235,
      columnNumber: 7
    }, this)]
  }, void 0, true);
}

_s(Home, "Lqqk7nocCv117nQUjWydCl8unGQ=", false, function () {
  return [react_intersection_observer__WEBPACK_IMPORTED_MODULE_11__.useInView, _hooks_useCountDown__WEBPACK_IMPORTED_MODULE_8__.default, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation];
});

_c = Home;

var _c;

$RefreshReg$(_c, "Home");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsibXNwZWFrZXIiLCJuYW1lIiwiaXRlbXNUb1Nob3ciLCJIb21lIiwiZGF0YSIsInVzZUluVmlldyIsInRocmVzaG9sZCIsInJlZiIsImluVmlldyIsInVzZUNvdW50RG93biIsInNob3dSZWxlYXNlRGF0ZSIsImNvdW5kb3duVGV4dCIsImFuaW1hdGlvbiIsInVzZUFuaW1hdGlvbiIsImZpcnN0SW1nIiwic2Vjb25kSW1nIiwidGhpcmRJbWciLCJmb3VydGhJbWciLCJzbGlkZUxlZnQiLCJpbWFnZVBhdGgiLCJzcmMiLCJ1c2VTdGF0ZSIsInNob3VsZFNob3dBY3Rpb25zIiwic2V0U2hvdWxkU2hvd0FjdGlvbnMiLCJ1c2VFZmZlY3QiLCJzdGFydCIsIngiLCJ5Iiwib3BhY2l0eSIsInpJbmRleCIsInRyYW5zaXRpb24iLCJkdXJhdGlvbiIsImRlc2NyaXB0aW9uIiwidGl0bGUiLCJ0eXBlIiwibG9jYWxlIiwidXJsIiwicHJvY2VzcyIsImVudiIsIk5FWFRfUFVCTElDX0RPTUFJTiIsImltYWdlcyIsIndpZHRoIiwiaGVpZ2h0IiwiYWx0Iiwic2h1ZmZsZSIsIm1hcCIsImltYWdlIiwiaW5kZXgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0NBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLElBQU1BLFFBQVEsR0FBRyxDQUNmO0FBQUVDLE1BQUksRUFBRSxpQkFBUjtBQUEyQkMsYUFBVyxFQUFFO0FBQXhDLENBRGUsRUFFZjtBQUFFRCxNQUFJLEVBQUUsb0JBQVI7QUFBOEJDLGFBQVcsRUFBRTtBQUEzQyxDQUZlLEVBR2Y7QUFBRUQsTUFBSSxFQUFFLGlCQUFSO0FBQTJCQyxhQUFXLEVBQUU7QUFBeEMsQ0FIZSxFQUlmO0FBQUVELE1BQUksRUFBRSxtQkFBUjtBQUE2QkMsYUFBVyxFQUFFO0FBQTFDLENBSmUsRUFLZjtBQUFFRCxNQUFJLEVBQUUsYUFBUjtBQUF1QkMsYUFBVyxFQUFFO0FBQXBDLENBTGUsRUFNZjtBQUFFRCxNQUFJLEVBQUUsV0FBUjtBQUFxQkMsYUFBVyxFQUFFO0FBQWxDLENBTmUsQ0FBakI7O0FBUWUsU0FBU0MsSUFBVCxPQUF3QjtBQUFBOztBQUFBOztBQUFBLE1BQVJDLElBQVEsUUFBUkEsSUFBUTs7QUFBQSxtQkFDYkMsdUVBQVMsQ0FBQztBQUFFQyxhQUFTLEVBQUU7QUFBYixHQUFELENBREk7QUFBQSxNQUM3QkMsR0FENkIsY0FDN0JBLEdBRDZCO0FBQUEsTUFDeEJDLE1BRHdCLGNBQ3hCQSxNQUR3Qjs7QUFBQSxzQkFFS0MsNERBQVksQ0FBQyxxQkFBRCxDQUZqQjtBQUFBLE1BRTdCQyxlQUY2QixpQkFFN0JBLGVBRjZCO0FBQUEsTUFFWkMsWUFGWSxpQkFFWkEsWUFGWTs7QUFJckMsTUFBTUMsU0FBUyxHQUFHQyw0REFBWSxFQUE5QjtBQUNBLE1BQU1DLFFBQVEsR0FBR0QsNERBQVksRUFBN0I7QUFDQSxNQUFNRSxTQUFTLEdBQUdGLDREQUFZLEVBQTlCO0FBQ0EsTUFBTUcsUUFBUSxHQUFHSCw0REFBWSxFQUE3QjtBQUNBLE1BQU1JLFNBQVMsR0FBR0osNERBQVksRUFBOUI7QUFDQSxNQUFNSyxTQUFTLEdBQUdMLDREQUFZLEVBQTlCO0FBQ0EsTUFBTU0sU0FBUyxHQUFHLENBQ2hCO0FBQUVDLE9BQUcsRUFBRSx1QkFBUDtBQUFnQ1IsYUFBUyxFQUFFRztBQUEzQyxHQURnQixFQUVoQjtBQUFFSyxPQUFHLEVBQUUscUJBQVA7QUFBOEJSLGFBQVMsRUFBRUU7QUFBekMsR0FGZ0IsRUFHaEI7QUFBRU0sT0FBRyxFQUFFLHdCQUFQO0FBQWlDUixhQUFTLEVBQUVLO0FBQTVDLEdBSGdCLEVBSWhCO0FBQUVHLE9BQUcsRUFBRSxxQkFBUDtBQUE4QlIsYUFBUyxFQUFFRztBQUF6QyxHQUpnQixFQUtoQjtBQUFFSyxPQUFHLEVBQUUsdUJBQVA7QUFBZ0NSLGFBQVMsRUFBRUk7QUFBM0MsR0FMZ0IsRUFNaEI7QUFBRUksT0FBRyxFQUFFLHFCQUFQO0FBQThCUixhQUFTLEVBQUVJO0FBQXpDLEdBTmdCLEVBT2hCO0FBQUVJLE9BQUcsRUFBRSwrQkFBUDtBQUF3Q1IsYUFBUyxFQUFFSTtBQUFuRCxHQVBnQixFQVFoQjtBQUFFSSxPQUFHLEVBQUUsdUJBQVA7QUFBZ0NSLGFBQVMsRUFBRUk7QUFBM0MsR0FSZ0IsRUFTaEI7QUFBRUksT0FBRyxFQUFFLHdCQUFQO0FBQWlDUixhQUFTLEVBQUVJO0FBQTVDLEdBVGdCLEVBVWhCO0FBQUVJLE9BQUcsRUFBRSwyQkFBUDtBQUFvQ1IsYUFBUyxFQUFFSTtBQUEvQyxHQVZnQixFQVdoQjtBQUFFSSxPQUFHLEVBQUUsOEJBQVA7QUFBdUNSLGFBQVMsRUFBRUk7QUFBbEQsR0FYZ0IsRUFZaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQztBQUFFSSxPQUFHLEVBQUUsaUJBQVA7QUFBMEJSLGFBQVMsRUFBRUU7QUFBckMsR0E1QmUsRUE2QmY7QUFBRU0sT0FBRyxFQUFFLGlCQUFQO0FBQTBCUixhQUFTLEVBQUVHO0FBQXJDLEdBN0JlLEVBOEJmO0FBQUVLLE9BQUcsRUFBRSxpQkFBUDtBQUEwQlIsYUFBUyxFQUFFSTtBQUFyQyxHQTlCZSxFQStCZjtBQUFFSSxPQUFHLEVBQUUsaUJBQVA7QUFBMEJSLGFBQVMsRUFBRUs7QUFBckMsR0EvQmUsRUFnQ2Y7QUFBRUcsT0FBRyxFQUFFLGlCQUFQO0FBQTBCUixhQUFTLEVBQUVFO0FBQXJDLEdBaENlLEVBaUNmO0FBQUVNLE9BQUcsRUFBRSxpQkFBUDtBQUEwQlIsYUFBUyxFQUFFRztBQUFyQyxHQWpDZSxFQWtDZjtBQUFFSyxPQUFHLEVBQUUsaUJBQVA7QUFBMEJSLGFBQVMsRUFBRUk7QUFBckMsR0FsQ2UsRUFtQ2Y7QUFBRUksT0FBRyxFQUFFLGlCQUFQO0FBQTBCUixhQUFTLEVBQUVLO0FBQXJDLEdBbkNlLEVBb0NmO0FBQUVHLE9BQUcsRUFBRSxpQkFBUDtBQUEwQlIsYUFBUyxFQUFFRTtBQUFyQyxHQXBDZSxFQXFDZjtBQUFFTSxPQUFHLEVBQUUsbUJBQVA7QUFBNEJSLGFBQVMsRUFBRUU7QUFBdkMsR0FyQ2UsRUFzQ2Y7QUFBRU0sT0FBRyxFQUFFLGtCQUFQO0FBQTJCUixhQUFTLEVBQUVHO0FBQXRDLEdBdENlLENBdUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhEZ0IsR0FBbEI7O0FBVnFDLGtCQTZEYU0sK0NBQVEsQ0FBQyxJQUFELENBN0RyQjtBQUFBLE1BNkQ5QkMsaUJBN0Q4QjtBQUFBLE1BNkRYQyxvQkE3RFc7O0FBOERyQ0Msa0RBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSWhCLE1BQUosRUFBWTtBQUNWSSxlQUFTLENBQUNhLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLENBRFc7QUFFZEMsU0FBQyxFQUFFLENBRlc7QUFHZEMsZUFBTyxFQUFFLENBSEs7QUFJZEMsY0FBTSxFQUFFLENBSk07QUFLZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMRSxPQUFoQjtBQU9BakIsY0FBUSxDQUFDVyxLQUFULENBQWU7QUFDYkMsU0FBQyxFQUFFLENBRFU7QUFFYkMsU0FBQyxFQUFFLENBRlU7QUFHYkMsZUFBTyxFQUFFLENBSEk7QUFJYkMsY0FBTSxFQUFFLENBSks7QUFLYkMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMQyxPQUFmO0FBT0FoQixlQUFTLENBQUNVLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLENBRFc7QUFFZEMsU0FBQyxFQUFFLENBRlc7QUFHZEMsZUFBTyxFQUFFLENBSEs7QUFJZEMsY0FBTSxFQUFFLENBSk07QUFLZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMRSxPQUFoQjtBQU9BZixjQUFRLENBQUNTLEtBQVQsQ0FBZTtBQUNiQyxTQUFDLEVBQUUsQ0FEVTtBQUViQyxTQUFDLEVBQUUsQ0FGVTtBQUdiQyxlQUFPLEVBQUUsQ0FISTtBQUliQyxjQUFNLEVBQUUsQ0FKSztBQUtiQyxrQkFBVSxFQUFFO0FBQUVDLGtCQUFRLEVBQUU7QUFBWjtBQUxDLE9BQWY7QUFPQWQsZUFBUyxDQUFDUSxLQUFWLENBQWdCO0FBQ2RDLFNBQUMsRUFBRSxDQURXO0FBRWRDLFNBQUMsRUFBRSxDQUZXO0FBR2RDLGVBQU8sRUFBRSxDQUhLO0FBSWRDLGNBQU0sRUFBRSxDQUpNO0FBS2RDLGtCQUFVLEVBQUU7QUFBRUMsa0JBQVEsRUFBRTtBQUFaO0FBTEUsT0FBaEI7QUFPQWIsZUFBUyxDQUFDTyxLQUFWLENBQWdCO0FBQ2RDLFNBQUMsRUFBRSxDQURXO0FBRWRDLFNBQUMsRUFBRSxDQUZXO0FBR2RDLGVBQU8sRUFBRSxDQUhLO0FBSWRDLGNBQU0sRUFBRSxDQUpNO0FBS2RDLGtCQUFVLEVBQUU7QUFBRUMsa0JBQVEsRUFBRTtBQUFaO0FBTEUsT0FBaEI7QUFPRDs7QUFDRCxRQUFJLENBQUN2QixNQUFMLEVBQWE7QUFDWEksZUFBUyxDQUFDYSxLQUFWLENBQWdCO0FBQ2RDLFNBQUMsRUFBRSxDQURXO0FBRWRDLFNBQUMsRUFBRSxHQUZXO0FBR2RDLGVBQU8sRUFBRSxDQUhLO0FBSWRDLGNBQU0sRUFBRSxDQUFDLENBSks7QUFNZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFORSxPQUFoQjtBQVFBakIsY0FBUSxDQUFDVyxLQUFULENBQWU7QUFDYkMsU0FBQyxFQUFFLENBRFU7QUFFYkMsU0FBQyxFQUFFLEdBRlU7QUFHYkMsZUFBTyxFQUFFLENBSEk7QUFJYkMsY0FBTSxFQUFFLENBQUMsQ0FKSTtBQUtiQyxrQkFBVSxFQUFFO0FBQUVDLGtCQUFRLEVBQUU7QUFBWjtBQUxDLE9BQWY7QUFPQWhCLGVBQVMsQ0FBQ1UsS0FBVixDQUFnQjtBQUNkQyxTQUFDLEVBQUUsQ0FEVztBQUVkQyxTQUFDLEVBQUUsR0FGVztBQUlkQyxlQUFPLEVBQUUsQ0FKSztBQUtkQyxjQUFNLEVBQUUsQ0FMTTtBQU1kQyxrQkFBVSxFQUFFO0FBQUVDLGtCQUFRLEVBQUU7QUFBWjtBQU5FLE9BQWhCO0FBUUFmLGNBQVEsQ0FBQ1MsS0FBVCxDQUFlO0FBQ2JDLFNBQUMsRUFBRSxDQURVO0FBRWJDLFNBQUMsRUFBRSxHQUZVO0FBSWJDLGVBQU8sRUFBRSxDQUpJO0FBS2JDLGNBQU0sRUFBRSxDQUxLO0FBTWJDLGtCQUFVLEVBQUU7QUFBRUMsa0JBQVEsRUFBRTtBQUFaO0FBTkMsT0FBZjtBQVFBZCxlQUFTLENBQUNRLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLENBRFc7QUFFZEMsU0FBQyxFQUFFLEdBRlc7QUFJZEMsZUFBTyxFQUFFLENBSks7QUFLZEMsY0FBTSxFQUFFLENBTE07QUFNZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFORSxPQUFoQjtBQVNBYixlQUFTLENBQUNPLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLEdBRFc7QUFFZEMsU0FBQyxFQUFFLENBRlc7QUFHZEMsZUFBTyxFQUFFLENBSEs7QUFJZEMsY0FBTSxFQUFFLENBSk07QUFLZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMRSxPQUFoQjtBQU9EO0FBQ0YsR0E5RlEsRUE4Rk4sQ0FBQ3ZCLE1BQUQsQ0E5Rk0sQ0FBVDtBQStGQSxzQkFDRTtBQUFBLDRCQUNFLDhEQUFDLDZDQUFEO0FBQ0UsV0FBSyxFQUFFLHNCQURUO0FBRUUsaUJBQVcsRUFBQyw2V0FGZDtBQUdFLGVBQVMsRUFBRTtBQUNUd0IsbUJBQVcsRUFDVCw2V0FGTztBQUdUQyxhQUFLLEVBQUUsc0JBSEU7QUFJVEMsWUFBSSxFQUFFLFNBSkc7QUFLVEMsY0FBTSxFQUFFLE9BTEM7QUFNVEMsV0FBRyxFQUFFQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsa0JBTlI7QUFPVEMsY0FBTSxFQUFFLENBQ047QUFDRUosYUFBRyxFQUFFLG9GQURQO0FBRUVLLGVBQUssRUFBRSxJQUZUO0FBR0VDLGdCQUFNLEVBQUUsR0FIVjtBQUlFQyxhQUFHLEVBQUU7QUFKUCxTQURNO0FBUEM7QUFIYjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFzQkU7QUFBTSxlQUFTLEVBQUMsU0FBaEI7QUFBQSw4QkFDRTtBQUNFLFVBQUUsRUFBQyxRQURMO0FBRUUsaUJBQVMsRUFBQywyRUFGWjtBQUFBLGdDQUlFLDhEQUFDLDhEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBSkYsZUFNRTtBQUFLLG1CQUFTLEVBQUMsNkVBQWY7QUFBQSxrQ0FDRTtBQUNFLGNBQUUsRUFBQyxNQURMO0FBRUUscUJBQVMsRUFBQyxzRkFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQVFFO0FBQU0scUJBQVMsRUFBQyx1Q0FBaEI7QUFBQSxzQkFDR2pDLGVBQWUsSUFBSUM7QUFEdEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFSRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBTkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFxQkU7QUFBSyxpQkFBUyxFQUFDLGlCQUFmO0FBQUEsZ0NBQ0U7QUFBSyxtQkFBUyxFQUFDLHNFQUFmO0FBQUEsaUNBQ0U7QUFBSyxxQkFBUyxFQUFDLGVBQWY7QUFBQSxvQ0FDRTtBQUFJLHVCQUFTLEVBQUMsaUdBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFJRTtBQUFTLHVCQUFTLEVBQUMsa0NBQW5CO0FBQUEsc0NBQ0U7QUFDRSx5QkFBUyxFQUFDLGVBRFo7QUFBQSx3Q0FFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFGQSxlQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUhBLGVBS0E7QUFBQSwwTEFBK0k7QUFBRywwQkFBTSxFQUFDLFFBQVY7QUFBbUIsd0JBQUksRUFBQyw4QkFBeEI7QUFBdUQsdUJBQUcsRUFBQyxZQUEzRDtBQUF3RSw2QkFBTSxXQUE5RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFBL0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUxBLGVBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBTkEsZUFPQTtBQUFBLHNIQUEyRTtBQUFHLDBCQUFNLEVBQUMsUUFBVjtBQUFtQix3QkFBSSxFQUFDLDhCQUF4QjtBQUF1RCx1QkFBRyxFQUFDLFlBQTNEO0FBQXdFLDZCQUFNLFdBQTlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQUEzRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURGLGVBVUU7QUFBSyx5QkFBUyxFQUFDLDJDQUFmO0FBQUEsMkJBQ0csR0FESCxlQUVFLDhEQUFDLHNEQUFEO0FBQ0UseUJBQU8sRUFBRTtBQUFFaUIsMkJBQU8sRUFBRSxDQUFYO0FBQWNjLDBCQUFNLEVBQUUsT0FBdEI7QUFBK0JELHlCQUFLLEVBQUU7QUFBdEMsbUJBRFg7QUFFRSx5QkFBTyxFQUFFO0FBQUViLDJCQUFPLEVBQUUsQ0FBWDtBQUFjYywwQkFBTSxFQUFFLE9BQXRCO0FBQStCRCx5QkFBSyxFQUFFO0FBQXRDLG1CQUZYO0FBR0UsMkJBQVMsRUFBQztBQUhaO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBRkYsZUFPRSw4REFBQyxtREFBRDtBQUNFLHFCQUFHLEVBQUUsTUFEUDtBQUVFLHFCQUFHLDRCQUZMO0FBR0UsdUJBQUssRUFBRSxHQUhUO0FBSUUsMkJBQVMsRUFBQyxVQUpaO0FBS0UsMkJBQVMsRUFBQyxTQUxaO0FBTUUsd0JBQU0sRUFBRTtBQU5WO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBUEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQVZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFKRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBbUNFO0FBQUssbUJBQVMsRUFBQyxxQkFBZjtBQUFBLGlDQUNFO0FBQUsscUJBQVMsRUFBQztBQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQW5DRixlQXVDRTtBQUNFLGFBQUcsRUFBRWxDLEdBRFA7QUFFRSxtQkFBUyxFQUFDLG1FQUZaO0FBQUEsb0JBSUdxQyw2REFBTyxDQUFDekIsU0FBRCxDQUFQLENBQW1CMEIsR0FBbkIsQ0FBdUIsVUFBQ0MsS0FBRCxFQUFRQyxLQUFSO0FBQUEsZ0NBQ3RCLDhEQUFDLHNEQUFEO0FBQ0UsdUJBQVMsRUFBQyw0QkFEWjtBQUVFLHFCQUFPLEVBQUVELEtBQUssQ0FBQ2xDLFNBRmpCO0FBQUEscUNBS0UsOERBQUMsbURBQUQ7QUFDRSxtQkFBRyxFQUFFLFNBRFA7QUFFRSxtQkFBRyxFQUFFLFVBQVVrQyxLQUFLLENBQUMxQixHQUZ2QjtBQUdFLHFCQUFLLEVBQUUsR0FIVDtBQUlFLHNCQUFNLEVBQUU7QUFKVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEYsZUFHTzJCLEtBSFA7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFEc0I7QUFBQSxXQUF2QjtBQUpIO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBdkNGLGVBMERFO0FBQUssbUJBQU0saUVBQVg7QUFBQSxrQ0FBNkU7QUFBUSxjQUFFLEVBQUMsTUFBWDtBQUFrQixxQkFBTSxzRkFBeEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBQTdFLGVBQThNO0FBQU0scUJBQU07QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQUE5TTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBMURGLGVBMkRFO0FBQUssWUFBRSxFQUFDLFNBQVI7QUFBa0IsbUJBQU07QUFBeEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkEzREYsZUE4REU7QUFBSyxtQkFBUyxFQUFDLHFCQUFmO0FBQUEsaUNBQ0U7QUFBSyxxQkFBUyxFQUFDO0FBQWY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBOURGLGVBaUVFO0FBQ0UsWUFBRSxFQUFDLFNBREw7QUFFRSxtQkFBUyxFQUFDO0FBRlo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFqRUYsZUFxRUU7QUFDRSxZQUFFLEVBQUMsU0FETDtBQUVFLG1CQUFTLEVBQUM7QUFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQXJFRixlQXlFRTtBQUNFLFlBQUUsRUFBQyxTQURMO0FBRUUsbUJBQVMsRUFBQztBQUZaO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBekVGLGVBNkVFO0FBQ0UsWUFBRSxFQUFDLFNBREw7QUFFRSxtQkFBUyxFQUFDO0FBRlo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkE3RUYsZUFpRkU7QUFBSyxtQkFBUyxFQUFDLHNFQUFmO0FBQUEsa0NBQ0U7QUFBSyxxQkFBUyxFQUFDLGVBQWY7QUFBQSxtQ0FDRTtBQUNFLHVCQUFTLEVBQUMsZ0JBRFosQ0FFRTtBQUZGO0FBQUEscUNBSUU7QUFBQSx1Q0FDRTtBQUFBLHVGQUN5RCxHQUR6RCxlQUVFO0FBQ0UsMEJBQU0sRUFBQyxRQURUO0FBRUUsd0JBQUksRUFBQyw2Q0FGUDtBQUdFLHVCQUFHLEVBQUMsWUFITjtBQUlFLDZCQUFTLEVBQUMsV0FKWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFGRixFQVNPLEdBVFAsZ0RBV0U7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFYRixlQVlFO0FBQU0sNkJBQVMsRUFBQyxXQUFoQjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFaRixlQWFFO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBYkYseUlBZVU7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFmVixtTUFrQlE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFsQlIsZ0hBbUI4QztBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQW5COUM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUpGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBaUNFLDhEQUFDLDZEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBakNGLGVBa0NFO0FBQUsscUJBQVMsRUFBQztBQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBbENGLGVBbUNFLDhEQUFDLDhEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBbkNGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkFqRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBckJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxZQXRCRjtBQUFBLGtCQURGO0FBc0tEOztHQW5VdUI1QyxJO1VBQ0VFLG1FLEVBQ2tCSSx3RCxFQUV4Qkksd0QsRUFDREEsd0QsRUFDQ0Esd0QsRUFDREEsd0QsRUFDQ0Esd0QsRUFDQUEsd0Q7OztLQVRJVixJIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2luZGV4LmZjYzU2ODc5N2M1NjJlYzlkYzc4LmhvdC11cGRhdGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZXh0U2VvIH0gZnJvbSBcIm5leHQtc2VvXCI7XG5pbXBvcnQgSGVhZGVyIGZyb20gXCIuLi9jb21wb25lbnRzL0xheW91dC9IZWFkZXJcIjtcbmltcG9ydCBGb290ZXIgZnJvbSBcIi4uL2NvbXBvbmVudHMvTGF5b3V0L0Zvb3RlclwiO1xuaW1wb3J0IHNodWZmbGUgZnJvbSBcIi4uL3V0aWxzL3NodWZmbGUtaW1hZ2VcIjtcbmltcG9ydCBJbWFnZSBmcm9tIFwibmV4dC9pbWFnZVwiO1xuaW1wb3J0IHsgbW90aW9uLCB1c2VBbmltYXRpb24gfSBmcm9tIFwiZnJhbWVyLW1vdGlvblwiO1xuaW1wb3J0IHsgR2V0U3RhdGljUHJvcHMgfSBmcm9tIFwibmV4dFwiO1xuaW1wb3J0IHsgdXNlU3RhdGUsIHVzZUVmZmVjdCB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IHsgdXNlSW5WaWV3IH0gZnJvbSBcInJlYWN0LWludGVyc2VjdGlvbi1vYnNlcnZlclwiO1xuaW1wb3J0IHVzZUNvdW50RG93biBmcm9tIFwiLi4vaG9va3MvdXNlQ291bnREb3duXCI7XG5pbXBvcnQgRm91bmRlciBmcm9tIFwiLi4vY29tcG9uZW50cy9Ib21lL0ZvdW5kZXJcIjtcbmltcG9ydCBkeW5hbWljIGZyb20gXCJuZXh0L2R5bmFtaWNcIjtcblxuLy8gdHlwZSBQcm9wcyA9IHtcbi8vICAgZGF0YToge1xuLy8gICAgIHRpdGxlOiB7IHJlbmRlcmVkOiBzdHJpbmcgfTtcbi8vICAgICBjb250ZW50OiB7IHJlbmRlcmVkOiBzdHJpbmcgfTtcbi8vICAgICBhY2Y6IHtcbi8vICAgICAgIHByb2ZpbGVfaW1hZ2VfdXJsOiBzdHJpbmc7XG4vLyAgICAgICBmaXJzdF9zZWN0aW9uOiBzdHJpbmc7XG4vLyAgICAgICBzZWNvbmRfc2VjdGlvbjogc3RyaW5nO1xuLy8gICAgICAgdGhpcmRfc2VjdGlvbjogc3RyaW5nO1xuLy8gICAgICAgZm91cnRoX3NlY3Rpb246IHN0cmluZztcbi8vICAgICAgIHZhbHVlX2ZpcnN0OiB7XG4vLyAgICAgICAgIGltYWdlXzogeyBzaXplczogeyBsYXJnZTogc3RyaW5nIH07IHVybDogc3RyaW5nIH07XG4vLyAgICAgICAgIGluZm86IHN0cmluZztcbi8vICAgICAgIH07XG4vLyAgICAgICB2YWx1ZV9zZWNvbmQ6IHtcbi8vICAgICAgICAgaW1hZ2VfOiB7IHNpemVzOiB7IGxhcmdlOiBzdHJpbmcgfTsgdXJsOiBzdHJpbmcgfTtcbi8vICAgICAgICAgaW5mbzogc3RyaW5nO1xuLy8gICAgICAgfTtcbi8vICAgICAgIHZhbHVlX3RoaXJkOiB7XG4vLyAgICAgICAgIGltYWdlXzogc3RyaW5nO1xuLy8gICAgICAgICBpbmZvOiBzdHJpbmc7XG4vLyAgICAgICB9O1xuLy8gICAgICAgdmFsdWVfZm91cnRoOiB7XG4vLyAgICAgICAgIGltYWdlXzogeyBzaXplczogeyBsYXJnZTogc3RyaW5nIH07IHVybDogc3RyaW5nIH07XG4vLyAgICAgICAgIGluZm86IHN0cmluZztcbi8vICAgICAgIH07XG4vLyAgICAgICB2YWx1ZV9maWZ0aDoge1xuLy8gICAgICAgICBpbWFnZV86IHsgc2l6ZXM6IHsgbGFyZ2U6IHN0cmluZyB9OyB1cmw6IHN0cmluZyB9O1xuLy8gICAgICAgICBpbmZvOiBzdHJpbmc7XG4vLyAgICAgICB9O1xuLy8gICAgIH07XG4vLyAgIH07XG4vLyB9O1xuY29uc3QgbXNwZWFrZXIgPSBbXG4gIHsgbmFtZTogXCJHYXJ5IFZheW5lcmNodWtcIiwgaXRlbXNUb1Nob3c6IFwiR2FyeS1WYXluZXJjaHVrLW1pbi5qcGdcIiB9LFxuICB7IG5hbWU6IFwiQXJpYW5uYSBIdWZmaW5ndG9uXCIsIGl0ZW1zVG9TaG93OiBcIkFyaWFubmEtSHVmZmluZ3Rvbi1taW4uanBnXCIgfSxcbiAgeyBuYW1lOiBcIlJvYmVydCBLaXlvc2FraVwiLCBpdGVtc1RvU2hvdzogXCJSb2JlcnQtS2l5b3Nha2kuanBnXCIgfSxcbiAgeyBuYW1lOiBcIlBhdHJpY2sgQmV0LURhdmlkXCIsIGl0ZW1zVG9TaG93OiBcIlBhdHJpY2stYmV0LWRhdmlkLW1pbi5qcGdcIiB9LFxuICB7IG5hbWU6IFwiVGltIEZlcnJpc3NcIiwgaXRlbXNUb1Nob3c6IFwiVGltLUZlcnJpc3MtbWluLnBuZ1wiIH0sXG4gIHsgbmFtZTogXCJUYWkgTG9wZXpcIiwgaXRlbXNUb1Nob3c6IFwiVGFpLUxvcGV6LW1pbi5qcGdcIiB9LFxuXTtcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIEhvbWUoeyBkYXRhIH0pIHtcbiAgY29uc3QgeyByZWYsIGluVmlldyB9ID0gdXNlSW5WaWV3KHsgdGhyZXNob2xkOiAwLjIgfSk7XG4gIGNvbnN0IHsgc2hvd1JlbGVhc2VEYXRlLCBjb3VuZG93blRleHQgfSA9IHVzZUNvdW50RG93bihcIjIwMjEtMTItMDQgMTA6MDAgQU1cIik7XG5cbiAgY29uc3QgYW5pbWF0aW9uID0gdXNlQW5pbWF0aW9uKCk7XG4gIGNvbnN0IGZpcnN0SW1nID0gdXNlQW5pbWF0aW9uKCk7XG4gIGNvbnN0IHNlY29uZEltZyA9IHVzZUFuaW1hdGlvbigpO1xuICBjb25zdCB0aGlyZEltZyA9IHVzZUFuaW1hdGlvbigpO1xuICBjb25zdCBmb3VydGhJbWcgPSB1c2VBbmltYXRpb24oKTtcbiAgY29uc3Qgc2xpZGVMZWZ0ID0gdXNlQW5pbWF0aW9uKCk7XG4gIGNvbnN0IGltYWdlUGF0aCA9IFtcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvYm94ZXIgMi5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvYm94ZXIucG5nXCIsIGFuaW1hdGlvbjogZmlyc3RJbWcgfSxcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvZGVubmlzLTMucG5nXCIsIGFuaW1hdGlvbjogZm91cnRoSW1nIH0sXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L2RpdmVyLnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIHsgc3JjOiBcInNsaWRlc2hvdy9tb2JzdGVyLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L3BpbG90LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L3BpcmF0ZXMgY2hlZXRhaC5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIHsgc3JjOiBcInNsaWRlc2hvdy9ydXNzZWxsLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L3N1cmZlci0yLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L3RpZ2VyIHdvb2RzLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L3pvbWJpZSBjaGVldGFoLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93Lzk4OS5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy85OTUucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTAxNC5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMDA3LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzEwMTcucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTA1LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzEwNy5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMDgucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTA5LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzExMC5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMTIucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTI0LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzEzOS5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xNDAucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvOTIucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvOTMucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzEuZ2lmXCIsIGFuaW1hdGlvbjogZmlyc3RJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzIuZ2lmXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy8zLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy80LnBuZ1wiLCBhbmltYXRpb246IGZvdXJ0aEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvNS5wbmdcIiwgYW5pbWF0aW9uOiBmaXJzdEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvNi5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzcucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzgucG5nXCIsIGFuaW1hdGlvbjogZm91cnRoSW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy85LnBuZ1wiLCBhbmltYXRpb246IGZpcnN0SW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy8xMC5qcGVnXCIsIGFuaW1hdGlvbjogZmlyc3RJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzExLnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcIjE5LnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcIjEzOS5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCIxNDcucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgLy8geyBzcmM6IFwiMTQ4LnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcIjE0OS5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCI1NTIxLnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcIjU2OS5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCI1MjAucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgLy8geyBzcmM6IFwiMzM1LnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcIjE2Mi5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgXTtcblxuICBjb25zdCBbc2hvdWxkU2hvd0FjdGlvbnMsIHNldFNob3VsZFNob3dBY3Rpb25zXSA9IHVzZVN0YXRlKHRydWUpO1xuICB1c2VFZmZlY3QoKCkgPT4ge1xuICAgIGlmIChpblZpZXcpIHtcbiAgICAgIGFuaW1hdGlvbi5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDAsXG4gICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgIHpJbmRleDogMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMC42IH0sXG4gICAgICB9KTtcbiAgICAgIGZpcnN0SW1nLnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMCxcbiAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgekluZGV4OiAxLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAwLjggfSxcbiAgICAgIH0pO1xuICAgICAgc2Vjb25kSW1nLnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMCxcbiAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgekluZGV4OiAxLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAwLjkgfSxcbiAgICAgIH0pO1xuICAgICAgdGhpcmRJbWcuc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAwLFxuICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDEuMiB9LFxuICAgICAgfSk7XG4gICAgICBmb3VydGhJbWcuc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAwLFxuICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDEuNCB9LFxuICAgICAgfSk7XG4gICAgICBzbGlkZUxlZnQuc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAwLFxuICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDEgfSxcbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAoIWluVmlldykge1xuICAgICAgYW5pbWF0aW9uLnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMjU4LFxuICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICB6SW5kZXg6IC0xLFxuXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDEgfSxcbiAgICAgIH0pO1xuICAgICAgZmlyc3RJbWcuc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAyNTIsXG4gICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgIHpJbmRleDogLTEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDEgfSxcbiAgICAgIH0pO1xuICAgICAgc2Vjb25kSW1nLnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMjUyLFxuXG4gICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgIHpJbmRleDogMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMSB9LFxuICAgICAgfSk7XG4gICAgICB0aGlyZEltZy5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDI1MixcblxuICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDEgfSxcbiAgICAgIH0pO1xuICAgICAgZm91cnRoSW1nLnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMjUyLFxuXG4gICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgIHpJbmRleDogMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMSB9LFxuICAgICAgfSk7XG5cbiAgICAgIHNsaWRlTGVmdC5zdGFydCh7XG4gICAgICAgIHg6IDE1NixcbiAgICAgICAgeTogMCxcbiAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgekluZGV4OiAxLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAxIH0sXG4gICAgICB9KTtcbiAgICB9XG4gIH0sIFtpblZpZXddKTtcbiAgcmV0dXJuIChcbiAgICA8PlxuICAgICAgPE5leHRTZW9cbiAgICAgICAgdGl0bGU9e1wiSG9tZSAtIENvYWxpdGlvbmNyZXdcIn1cbiAgICAgICAgZGVzY3JpcHRpb249XCJZb3V0dWJlIFR3aXR0ZXIgSW5zdGFncmFtIFdlbGNvbWUgdG8gdGhlIENvYWxpdGlvbiBNaW50IGhlcmUgVGhpcyBpcyB0aGUgTkZUIGZvciBHYW1lIENoYW5nZXJzLiBUaGUgQ29hbGl0aW9uIENyZXcgaXMgYW4gZXhjbHVzaXZlIGNvbGxlY3Rpb24gb2YgNzEwMCB1bmlxdWUgQ2hlZXRhaCBORlRzIGxpdmluZyBvbiB0aGUgRXRoZXJldW0gYmxvY2tjaGFpbi4gSXTigJlzIGVzdGltYXRlZCB0aGF0IGFzIG9mIDIwMjEsIHRoZXJlIGFyZSBvbmx5IDcxMDAgY2hlZXRhaHMgbGVmdCBpbiB0aGUgd2lsZC4gQ2hlZXRhaHMgYXJlIGN1cnJlbnRseSBsaXN0ZWQgYXMgdnVsbmVyYWJsZSBhbmQgJmhlbGxpcDsgSG9tZSBSZWFkIE1vcmUgJnJhcXVvO1wiXG4gICAgICAgIG9wZW5HcmFwaD17e1xuICAgICAgICAgIGRlc2NyaXB0aW9uOlxuICAgICAgICAgICAgXCJZb3V0dWJlIFR3aXR0ZXIgSW5zdGFncmFtIFdlbGNvbWUgdG8gdGhlIENvYWxpdGlvbiBNaW50IGhlcmUgVGhpcyBpcyB0aGUgTkZUIGZvciBHYW1lIENoYW5nZXJzLiBUaGUgQ29hbGl0aW9uIENyZXcgaXMgYW4gZXhjbHVzaXZlIGNvbGxlY3Rpb24gb2YgNzEwMCB1bmlxdWUgQ2hlZXRhaCBORlRzIGxpdmluZyBvbiB0aGUgRXRoZXJldW0gYmxvY2tjaGFpbi4gSXTigJlzIGVzdGltYXRlZCB0aGF0IGFzIG9mIDIwMjEsIHRoZXJlIGFyZSBvbmx5IDcxMDAgY2hlZXRhaHMgbGVmdCBpbiB0aGUgd2lsZC4gQ2hlZXRhaHMgYXJlIGN1cnJlbnRseSBsaXN0ZWQgYXMgdnVsbmVyYWJsZSBhbmQgJmhlbGxpcDsgSG9tZSBSZWFkIE1vcmUgJnJhcXVvO1wiLFxuICAgICAgICAgIHRpdGxlOiBcIkhvbWUgLSBDb2FsaXRpb25jcmV3XCIsXG4gICAgICAgICAgdHlwZTogXCJ3ZWJzaXRlXCIsXG4gICAgICAgICAgbG9jYWxlOiBcImVuX1VTXCIsXG4gICAgICAgICAgdXJsOiBwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19ET01BSU4sXG4gICAgICAgICAgaW1hZ2VzOiBbXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIHVybDogXCJodHRwOi8vY29hbGl0aW9uY3Jldy5jb20vd3AtY29udGVudC91cGxvYWRzLzIwMjEvMTAvVENDXzEtVHJhbnNwYXJlbnQtMTAyNHg5ODIucG5nXCIsXG4gICAgICAgICAgICAgIHdpZHRoOiAxMjAwLFxuICAgICAgICAgICAgICBoZWlnaHQ6IDYzMCxcbiAgICAgICAgICAgICAgYWx0OiBcImNvYWxpdGlvbiBjcmV3IGxvZ29cIixcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgXSxcbiAgICAgICAgfX1cbiAgICAgIC8+XG5cbiAgICAgIDxtYWluIGNsYXNzTmFtZT1cIm14LWF1dG9cIj5cbiAgICAgICAgPGRpdlxuICAgICAgICAgIGlkPVwiYmFubmVyXCJcbiAgICAgICAgICBjbGFzc05hbWU9XCIgcmVsYXRpdmUgYmctY3JldyB3LWZ1bGwgIGgtc2NyZWVuIGJnLWNvdmVyIGZsZXggZmxleC1jb2wganVzdGlmeS1iZXR3ZWVuXCJcbiAgICAgICAgPlxuICAgICAgICAgIDxIZWFkZXIgLz5cblxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBmbGV4LWNvbCAgbWQ6YWJzb2x1dGUgcmlnaHQtNjQgYm90dG9tLTExICAganVzdGlmeS1jZW50ZXIgaXRlbXMtY2VudGVyXCI+XG4gICAgICAgICAgICA8YnV0dG9uXG4gICAgICAgICAgICAgIGlkPVwiY3RhMlwiXG4gICAgICAgICAgICAgIGNsYXNzTmFtZT1cIiAgdGV4dC0zeGwgYmcteWVsbG93LTMwMCB1cHBlcmNhc2UgaXRhbGljIGZvbnQtYm9sZCAgbWItMiBweC0xNiBweS00ICB0ZXh0LWljb25Db2xvclwiXG4gICAgICAgICAgICA+XG4gICAgICAgICAgICAgIE1pbnQgSGVyZVxuICAgICAgICAgICAgPC9idXR0b24+XG5cbiAgICAgICAgICAgIDxzcGFuIGNsYXNzTmFtZT1cInRleHQtMnhsIGZvbnQtYm9sZCB0ZXh0LWdyYXktODAwIG1iLTRcIj5cbiAgICAgICAgICAgICAge3Nob3dSZWxlYXNlRGF0ZSAmJiBjb3VuZG93blRleHR9XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiIHRleHQtZ3JheS04MDAgXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwb3N0LWNvbnRlbnQgY29udGFpbmVyIG14LWF1dG8gdGV4dC1sZyBwLTQgdGV4dC1jZW50ZXIgbWQ6dGV4dC1sZWZ0IFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmaXJzdC1zZWN0aW9uXCI+XG4gICAgICAgICAgICAgIDxoMSBjbGFzc05hbWU9XCJ0ZXh0LWdyYXktOTAwIHRleHQtbGVmdCBvdXItbWlzc2lvbiBmb250LWJvbGQgdGV4dC0zeGwgYm9yZGVyLWItMiBwYi0zIGJvcmRlci1ncmF5LTgwMCBtZDp3LTEvNFwiPlxuICAgICAgICAgICAgICAgIE91ciBNaXNzaW9uXG4gICAgICAgICAgICAgIDwvaDE+XG4gICAgICAgICAgICAgIDxzZWN0aW9uIGNsYXNzTmFtZT1cImdyaWQgbWQ6Z3JpZC1jb2xzLTIgaXRlbXMtY2VudGVyXCI+XG4gICAgICAgICAgICAgICAgPGRpdlxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiYWJvdXQtbWlzc2lvblwiPiAgICAgICAgICBcbiAgICAgICAgICAgICAgICA8cD5IaSEgV2VsY29tZSB0byB0aGUgQ29hbGl0aW9uIENyZXchIFRoaXMgaXMgYSBsaW1pdGVkIGNvbGxlY3Rpb24gb2YgNTAwMCB1bmlxdWUgQ2hlZXRhaCBORlRzIGxpdmluZyBvbiB0aGUgRXRoZXJldW0gYmxvY2tjaGFpbi4gVGhpcyBwcm9qZWN0IGlzIHRlY2huaWNhbGx5IGJyb2tlbiB1cCBpbnRvIHRocmVlIGNvbGxlY3Rpb25zLjwvcD5cbiAgICAgICAgICAgICAgICA8cD4xc3QgQ29sbGVjdGlvbiAtIENvYWxpdGlvbiBDcmV3IChPRyBDT0xMRUNUSU9OKSBPbmx5IDEwMTAgYXZhaWwuXG4qVXBkYXRlIC0gU29sZCBvdXQgaW4gbGVzcyB0aGFuIGEgZGF5ISo8L3A+XG4gICAgICAgICAgICAgICAgPHA+Mm5kIENvbGxlY3Rpb24gLSBBdWN0aW9uIENoZWV0YWhzIC0gMjAgYXZhaWwuIFRoZXNlIHF1YWxpZnkgZm9yIFZJUCBhY2Nlc3MgdG8gYWxsIGxpdmUgZXZlbnRzLiBUaGUgYXVjdGlvbiBiZWdpbnMgSmFuIDh0aC4gUGxlYXNlIHZpc2l0IG91ciA8YSB0YXJnZXQ9XCJfYmxhbmtcIiBocmVmPVwiaHR0cDovL2Rpc2NvcmQuZ2cvM25LUkJjRFMzM1wiIHJlbD1cIm5vcmVmZXJyZXJcIiBjbGFzcz1cInVuZGVybGluZVwiPiBEaXNjb3JkPC9hPiBmb3IgbW9yZSBpbmZvIG9uIHRoaXMhIDwvcD5cbiAgICAgICAgICAgICAgICA8cD4zcmQgQ29sbGVjdGlvbiAtIENvYWxpdGlvbiBDcmV3IDIuMCAtIE9ubHkgMzk3MCBhdmFpbC4gVGhpcyB3aWxsIHdyYXAgdXAgdGhlIGNvbGxlY3Rpb24gZm9yIHRoZSBDb2FsaXRpb24gQ3JldyBwcm9qZWN0LiBNaW50IHByaWNlIGlzIC4wOSBFVEggYW5kIHlvdSBjYW4gbWludCB1cCB0byA1LiBUaGlzIGNvbGxlY3Rpb24gd2lsbCBiZWdpbiBtaW50aW5nIEphbiAyNnRoIGF0IDEwYW0gUFNULzFwbSBFU1QuPC9wPlxuICAgICAgICAgICAgICAgIDxwPk91dCBvZiB0aGUgNTAwMCB0b3RhbCwgb25seSA1MCBxdWFsaWZ5IGZvciBWSVAgYWNjZXNzLiBQbGVhc2UgdmlzaXQgb3VyIDxhIHRhcmdldD1cIl9ibGFua1wiIGhyZWY9XCJodHRwOi8vZGlzY29yZC5nZy8zbktSQmNEUzMzXCIgcmVsPVwibm9yZWZlcnJlclwiIGNsYXNzPVwidW5kZXJsaW5lXCI+IERpc2NvcmQ8L2E+IERpc2NvcmQgZm9yIG1vcmUgaW5mbyBvbiB3aGljaCBvbmVzIHF1YWxpZnkgZm9yIFZJUC48L3A+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4IGl0ZW1zLWNlbnRlciBqdXN0aWZ5LWNlbnRlciByZWxhdGl2ZVwiPlxuICAgICAgICAgICAgICAgICAge1wiIFwifVxuICAgICAgICAgICAgICAgICAgPG1vdGlvbi5kaXZcbiAgICAgICAgICAgICAgICAgICAgaW5pdGlhbD17eyBvcGFjaXR5OiAwLCBoZWlnaHQ6IFwiMjByZW1cIiwgd2lkdGg6IFwiMjByZW1cIiB9fVxuICAgICAgICAgICAgICAgICAgICBhbmltYXRlPXt7IG9wYWNpdHk6IDEsIGhlaWdodDogXCIyMnJlbVwiLCB3aWR0aDogXCIyMnJlbVwiIH19XG4gICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImgtNzIgIGR1cmF0aW9uLTMwMCB3LTcyIG1kOnctOTYgbWQ6aC05NiBiZy1ncmF5LTgwMCByb3VuZGVkLWZ1bGwgYWJzb2x1dGUgei0wIG1kOmxlZnQtMzhcIlxuICAgICAgICAgICAgICAgICAgPjwvbW90aW9uLmRpdj5cbiAgICAgICAgICAgICAgICAgIDxJbWFnZVxuICAgICAgICAgICAgICAgICAgICBhbHQ9e1wibG9nb1wifVxuICAgICAgICAgICAgICAgICAgICBzcmM9e2AvaW1nL3J1bm5pbmctY2hlZXRhaC5naWZgfVxuICAgICAgICAgICAgICAgICAgICB3aWR0aD17NDAwfVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJyZWxhdGl2ZVwiXG4gICAgICAgICAgICAgICAgICAgIG9iamVjdEZpdD1cImNvbnRhaW5cIlxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ9ezQ1MH1cbiAgICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvc2VjdGlvbj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBqdXN0aWZ5LWNlbnRlclwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJ3LTM2IGgtMiBmbGV4ICBiZy1ncmF5LTcwMCBteS05XCI+PC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgXG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgcmVmPXtyZWZ9XG4gICAgICAgICAgICBjbGFzc05hbWU9XCJnYWxsZXJ5IHJlbGF0aXZlIGp1c3RpZnktaXRlbXMtY2VudGVyIGZsZXggZ2FwLTggIG1kOmdyaWQtY29scy00IFwiXG4gICAgICAgICAgPlxuICAgICAgICAgICAge3NodWZmbGUoaW1hZ2VQYXRoKS5tYXAoKGltYWdlLCBpbmRleCkgPT4gKFxuICAgICAgICAgICAgICA8bW90aW9uLmRpdlxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cIml0ZW0gbWluLXctZnVsbCByb3VuZGVkLWxnXCJcbiAgICAgICAgICAgICAgICBhbmltYXRlPXtpbWFnZS5hbmltYXRpb259XG4gICAgICAgICAgICAgICAga2V5PXtpbmRleH1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxJbWFnZVxuICAgICAgICAgICAgICAgICAgYWx0PXtcIkNoZWV0YWhcIn1cbiAgICAgICAgICAgICAgICAgIHNyYz17XCIvaW1nL1wiICsgaW1hZ2Uuc3JjfVxuICAgICAgICAgICAgICAgICAgd2lkdGg9ezYyMH1cbiAgICAgICAgICAgICAgICAgIGhlaWdodD17NjUwfVxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvbW90aW9uLmRpdj5cbiAgICAgICAgICAgICkpfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJmbGV4IGZsZXgtY29sICByaWdodC02NCBib3R0b20tMTEgICBqdXN0aWZ5LWNlbnRlciBpdGVtcy1jZW50ZXJcIj48YnV0dG9uIGlkPVwiY3RhMlwiIGNsYXNzPVwiICB0ZXh0LTN4bCBiZy15ZWxsb3ctMzAwIHVwcGVyY2FzZSBpdGFsaWMgZm9udC1ib2xkICBtYi0yIHB4LTE2IHB5LTQgIHRleHQtaWNvbkNvbG9yXCI+TWludCBIZXJlPC9idXR0b24+PHNwYW4gY2xhc3M9XCJ0ZXh0LTJ4bCBmb250LWJvbGQgdGV4dC1ncmF5LTgwMCBtYi00XCI+PC9zcGFuPjwvZGl2PlxuICAgICAgICAgIDxkaXYgaWQ9XCJiYW5uZXIyXCIgY2xhc3M9XCIgcmVsYXRpdmUgYmctdmFsdWUgdy1mdWxsICBoLXNjcmVlbiBiZy1jb3ZlciBmbGV4IGZsZXgtY29sIGp1c3RpZnktYmV0d2VlbiBcIj5cbiAgICAgICAgICBcbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgganVzdGlmeS1jZW50ZXJcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibWF4LXctNnhsIG92ZXJmbG93LWhpZGRlbiB3LTM2IGgtMiBmbGV4IG10LTggIGJnLWdyYXktODAwIFwiPjwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGlkPVwiYmFubmVyMlwiXG4gICAgICAgICAgICBjbGFzc05hbWU9XCIgcmVsYXRpdmUgYmctdmFsdWUgdy1mdWxsICBoLXNjcmVlbiBiZy1jb3ZlciBmbGV4IGZsZXgtY29sIGp1c3RpZnktYmV0d2VlbiBcIlxuICAgICAgICAgID48L2Rpdj5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBpZD1cImJhbm5lcjNcIlxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiIHJlbGF0aXZlIGJnLW1lbnRvciB3LWZ1bGwgIGgtc2NyZWVuIGJnLWNvdmVyIGZsZXggZmxleC1jb2wganVzdGlmeS1iZXR3ZWVuIG15LTEwXCJcbiAgICAgICAgICA+PC9kaXY+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgaWQ9XCJiYW5uZXI0XCJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cIiByZWxhdGl2ZSBiZy1uZXR3b3JrMSB3LWZ1bGwgIGgtc2NyZWVuIGJnLWNvdmVyIGZsZXggZmxleC1jb2wganVzdGlmeS1iZXR3ZWVuIG1iLTEwXCJcbiAgICAgICAgICA+PC9kaXY+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgaWQ9XCJiYW5uZXI1XCJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cIiByZWxhdGl2ZSBiZy1uZXR3b3JrMiB3LWZ1bGwgIGgtc2NyZWVuIGJnLWNvdmVyIGZsZXggZmxleC1jb2wganVzdGlmeS1iZXR3ZWVuIG1iLTEwXCJcbiAgICAgICAgICA+PC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJwb3N0LWNvbnRlbnQgY29udGFpbmVyIG14LWF1dG8gdGV4dC1sZyBwLTQgdGV4dC1jZW50ZXIgbWQ6dGV4dC1sZWZ0IFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmaXJzdC1zZWN0aW9uXCI+XG4gICAgICAgICAgICAgIDxzZWN0aW9uXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwiZm91cnRoLXNlY3Rpb25cIlxuICAgICAgICAgICAgICAgIC8vIGRhbmdlcm91c2x5U2V0SW5uZXJIVE1MPXt7IF9faHRtbDogZGF0YS5hY2YuZm91cnRoX3NlY3Rpb24gfX1cbiAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICA8cD5cbiAgICAgICAgICAgICAgICAgICAgT25jZSBtaW50ZWQgb3IgYm91Z2h0IG9uIE9wZW5TZWEsIHBsZWFzZSBmaWxsIG91dCB0aGlze1wiIFwifVxuICAgICAgICAgICAgICAgICAgICA8YVxuICAgICAgICAgICAgICAgICAgICAgIHRhcmdldD1cIl9ibGFua1wiXG4gICAgICAgICAgICAgICAgICAgICAgaHJlZj1cImh0dHBzOi8vd3d3LmdhbWVjaGFuZ2Vyc21vdmVtZW1lbnQuY29tL2NyZXdcIlxuICAgICAgICAgICAgICAgICAgICAgIHJlbD1cIm5vcmVmZXJyZXJcIlxuICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cInVuZGVybGluZVwiXG4gICAgICAgICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICAgICAgICBmb3JtXG4gICAgICAgICAgICAgICAgICAgIDwvYT57XCIgXCJ9XG4gICAgICAgICAgICAgICAgICAgIHRvIGdldCBhY2Nlc3MgdG8gdGhlIEFjYWRlbXkuXG4gICAgICAgICAgICAgICAgICAgIDxiciAvPlxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJmb250LWJvbGRcIj5Ob3RlITwvc3Bhbj5cbiAgICAgICAgICAgICAgICAgICAgPGJyIC8+IElmIHlvdSBzZWxsIHlvdXIgb25seSBjaGVldGFoLCB5b3VyIGFjY2VzcyB0byB0aGVcbiAgICAgICAgICAgICAgICAgICAgQWNhZGVteSB3aWxsIGJlIHN1c3BlbmRlZC4gWW91IG11c3Qgb3duIGF0IGxlYXN0IDEgdG8ga2VlcFxuICAgICAgICAgICAgICAgICAgICBhY2Nlc3MuIDxiciAvPiBBZnRlciAzIG1vbnRocyBhbGwgaG9sZGVycyB3aWxsIHJlY2VpdmUgdGhlaXJcbiAgICAgICAgICAgICAgICAgICAgY2hlZXRhaCBhZG9wdGlvbiDigJxwYXBlcndvcmvigJ0uIFRoaXMgaW5jbHVkZXMgYSBwaG90bywgYW5cbiAgICAgICAgICAgICAgICAgICAgYWRvcHRpb24gY2VydGlmaWNhdGUsIGEgc3R1ZmZlZCBhbmltYWwsIGEgc3BlY2llcyBjYXJkICtcbiAgICAgICAgICAgICAgICAgICAgbW9yZSEgPGJyIC8+IFdlIGFyZSBsYXVuY2hpbmcgYSB1bmlxdWUgbWVyY2hhbmRpc2Ugc3RvcmUgZm9yXG4gICAgICAgICAgICAgICAgICAgIENvYWxpdGlvbiBDcmV3IE1lbWJlcnMgdG8gbWFrZSBjdXN0b20gZ2Vhci4gPGJyIC8+IEFsbFxuICAgICAgICAgICAgICAgICAgICBob2xkZXJzIGdldCBhdXRvbWF0aWMgV0wgZm9yIGZ1dHVyZSBwcm9qZWN0cywgYXMgd2VsbCBhc1xuICAgICAgICAgICAgICAgICAgICBleGNsdXNpdmUgYWlyZHJvcHMuXG4gICAgICAgICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvc2VjdGlvbj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPEZvdW5kZXIgLz5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaC0wLjUgdy1mdWxsIGJnLWdyYXktNzAwIG15LThcIj48L2Rpdj5cbiAgICAgICAgICAgIDxGb290ZXIgLz5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9kaXY+XG4gICAgICA8L21haW4+XG4gICAgPC8+XG4gICk7XG59XG5cbmV4cG9ydCBjb25zdCBnZXRTdGF0aWNQcm9wcyA9IGFzeW5jICgpID0+IHtcbiAgY29uc3QgZGF0YSA9IGF3YWl0IGZldGNoKFxuICAgIGAke3Byb2Nlc3MuZW52Lk5FWFRfUFVCTElDX0JMT0dfVVJMfS93cC92Mi9wYWdlcy80ODc3YFxuICApLnRoZW4oKTtcbiAgY29uc3QgaG9tZSA9IGF3YWl0IGRhdGEuanNvbigpO1xuXG4gIHJldHVybiB7XG4gICAgcHJvcHM6IHtcbiAgICAgIGRhdGE6IGhvbWUsXG4gICAgfSxcbiAgfTtcbn07XG4iXSwic291cmNlUm9vdCI6IiJ9