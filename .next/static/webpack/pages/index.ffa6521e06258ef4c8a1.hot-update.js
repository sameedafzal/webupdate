self["webpackHotUpdate_N_E"]("pages/index",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "__N_SSG": function() { return /* binding */ __N_SSG; },
/* harmony export */   "default": function() { return /* binding */ Home; }
/* harmony export */ });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next-seo */ "./node_modules/next-seo/lib/next-seo.module.js");
/* harmony import */ var _components_Layout_Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Layout/Header */ "./components/Layout/Header.js");
/* harmony import */ var _components_Layout_Footer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Layout/Footer */ "./components/Layout/Footer.js");
/* harmony import */ var _utils_shuffle_image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/shuffle-image */ "./utils/shuffle-image.js");
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/image */ "./node_modules/next/image.js");
/* harmony import */ var next_image__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_image__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var framer_motion__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! framer-motion */ "./node_modules/framer-motion/dist/es/index.mjs");
/* harmony import */ var next__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! next */ "next");
/* harmony import */ var next__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_intersection_observer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! react-intersection-observer */ "./node_modules/react-intersection-observer/react-intersection-observer.m.js");
/* harmony import */ var _hooks_useCountDown__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../hooks/useCountDown */ "./hooks/useCountDown.js");
/* harmony import */ var _components_Home_Founder__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../components/Home/Founder */ "./components/Home/Founder.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! next/dynamic */ "./node_modules/next/dynamic.js");
/* harmony import */ var next_dynamic__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(next_dynamic__WEBPACK_IMPORTED_MODULE_10__);
/* module decorator */ module = __webpack_require__.hmd(module);
/* provided dependency */ var process = __webpack_require__(/*! process */ "./node_modules/process/browser.js");



var _jsxFileName = "C:\\Users\\samee\\Desktop\\crew-main\\pages\\index.js",
    _s = $RefreshSig$();












 // type Props = {
//   data: {
//     title: { rendered: string };
//     content: { rendered: string };
//     acf: {
//       profile_image_url: string;
//       first_section: string;
//       second_section: string;
//       third_section: string;
//       fourth_section: string;
//       value_first: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//       value_second: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//       value_third: {
//         image_: string;
//         info: string;
//       };
//       value_fourth: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//       value_fifth: {
//         image_: { sizes: { large: string }; url: string };
//         info: string;
//       };
//     };
//   };
// };

var mspeaker = [{
  name: "Gary Vaynerchuk",
  itemsToShow: "Gary-Vaynerchuk-min.jpg"
}, {
  name: "Arianna Huffington",
  itemsToShow: "Arianna-Huffington-min.jpg"
}, {
  name: "Robert Kiyosaki",
  itemsToShow: "Robert-Kiyosaki.jpg"
}, {
  name: "Patrick Bet-David",
  itemsToShow: "Patrick-bet-david-min.jpg"
}, {
  name: "Tim Ferriss",
  itemsToShow: "Tim-Ferriss-min.png"
}, {
  name: "Tai Lopez",
  itemsToShow: "Tai-Lopez-min.jpg"
}];
var __N_SSG = true;
function Home(_ref) {
  _s();

  var _this = this;

  var data = _ref.data;

  var _useInView = (0,react_intersection_observer__WEBPACK_IMPORTED_MODULE_11__.useInView)({
    threshold: 0.2
  }),
      ref = _useInView.ref,
      inView = _useInView.inView;

  var _useCountDown = (0,_hooks_useCountDown__WEBPACK_IMPORTED_MODULE_8__.default)("2021-12-04 10:00 AM"),
      showReleaseDate = _useCountDown.showReleaseDate,
      coundownText = _useCountDown.coundownText;

  var animation = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var firstImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var secondImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var thirdImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var fourthImg = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var slideLeft = (0,framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation)();
  var imagePath = [{
    src: "slideshow/boxer 2.png",
    animation: secondImg
  }, {
    src: "slideshow/boxer.png",
    animation: firstImg
  }, {
    src: "slideshow/dennis-3.png",
    animation: fourthImg
  }, {
    src: "slideshow/diver.png",
    animation: secondImg
  }, {
    src: "slideshow/mobster.png",
    animation: thirdImg
  }, {
    src: "slideshow/pilot.png",
    animation: thirdImg
  }, {
    src: "slideshow/pirates cheetah.png",
    animation: thirdImg
  }, {
    src: "slideshow/russell.png",
    animation: thirdImg
  }, {
    src: "slideshow/surfer-2.png",
    animation: thirdImg
  }, {
    src: "slideshow/tiger woods.png",
    animation: thirdImg
  }, {
    src: "slideshow/zombie cheetah.png",
    animation: thirdImg
  }, // { src: "slideshow/989.png", animation: thirdImg },
  // { src: "slideshow/995.png", animation: thirdImg },
  // { src: "slideshow/1014.png", animation: thirdImg },
  // { src: "slideshow/1007.png", animation: thirdImg },
  // { src: "slideshow/1017.png", animation: thirdImg },
  // { src: "slideshow/105.png", animation: thirdImg },
  // { src: "slideshow/107.png", animation: thirdImg },
  // { src: "slideshow/108.png", animation: thirdImg },
  // { src: "slideshow/109.png", animation: thirdImg },
  // { src: "slideshow/110.png", animation: thirdImg },
  // { src: "slideshow/112.png", animation: thirdImg },
  // { src: "slideshow/124.png", animation: thirdImg },
  // { src: "slideshow/139.png", animation: thirdImg },
  // { src: "slideshow/140.png", animation: thirdImg },
  // { src: "slideshow/92.png", animation: thirdImg },
  // { src: "slideshow/93.png", animation: thirdImg },
  {
    src: "slideshow/1.gif",
    animation: firstImg
  }, {
    src: "slideshow/2.gif",
    animation: secondImg
  }, {
    src: "slideshow/3.png",
    animation: thirdImg
  }, {
    src: "slideshow/4.png",
    animation: fourthImg
  }, {
    src: "slideshow/5.png",
    animation: firstImg
  }, {
    src: "slideshow/6.png",
    animation: secondImg
  }, {
    src: "slideshow/7.png",
    animation: thirdImg
  }, {
    src: "slideshow/8.png",
    animation: fourthImg
  }, {
    src: "slideshow/9.png",
    animation: firstImg
  }, {
    src: "slideshow/10.jpeg",
    animation: firstImg
  }, {
    src: "slideshow/11.png",
    animation: secondImg
  } // { src: "19.png", animation: secondImg },
  // { src: "139.png", animation: secondImg },
  // { src: "147.png", animation: secondImg },
  // { src: "148.png", animation: secondImg },
  // { src: "149.png", animation: secondImg },
  // { src: "5521.png", animation: secondImg },
  // { src: "569.png", animation: secondImg },
  // { src: "520.png", animation: secondImg },
  // { src: "335.png", animation: secondImg },
  // { src: "162.png", animation: secondImg },
  ];

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_7__.useState)(true),
      shouldShowActions = _useState[0],
      setShouldShowActions = _useState[1];

  (0,react__WEBPACK_IMPORTED_MODULE_7__.useEffect)(function () {
    if (inView) {
      animation.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 0.6
        }
      });
      firstImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 0.8
        }
      });
      secondImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 0.9
        }
      });
      thirdImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 1.2
        }
      });
      fourthImg.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 1.4
        }
      });
      slideLeft.start({
        x: 0,
        y: 0,
        opacity: 1,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
    }

    if (!inView) {
      animation.start({
        x: 0,
        y: 258,
        opacity: 0,
        zIndex: -1,
        transition: {
          duration: 1
        }
      });
      firstImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: -1,
        transition: {
          duration: 1
        }
      });
      secondImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
      thirdImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
      fourthImg.start({
        x: 0,
        y: 252,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
      slideLeft.start({
        x: 156,
        y: 0,
        opacity: 0,
        zIndex: 1,
        transition: {
          duration: 1
        }
      });
    }
  }, [inView]);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(next_seo__WEBPACK_IMPORTED_MODULE_1__.NextSeo, {
      title: "Home - Coalitioncrew",
      description: "Youtube Twitter Instagram Welcome to the Coalition Mint here This is the NFT for Game Changers. The Coalition Crew is an exclusive collection of 7100 unique Cheetah NFTs living on the Ethereum blockchain. It\u2019s estimated that as of 2021, there are only 7100 cheetahs left in the wild. Cheetahs are currently listed as vulnerable and \u2026 Home Read More \xBB",
      openGraph: {
        description: "Youtube Twitter Instagram Welcome to the Coalition Mint here This is the NFT for Game Changers. The Coalition Crew is an exclusive collection of 7100 unique Cheetah NFTs living on the Ethereum blockchain. It’s estimated that as of 2021, there are only 7100 cheetahs left in the wild. Cheetahs are currently listed as vulnerable and &hellip; Home Read More &raquo;",
        title: "Home - Coalitioncrew",
        type: "website",
        locale: "en_US",
        url: process.env.NEXT_PUBLIC_DOMAIN,
        images: [{
          url: "http://coalitioncrew.com/wp-content/uploads/2021/10/TCC_1-Transparent-1024x982.png",
          width: 1200,
          height: 630,
          alt: "coalition crew logo"
        }]
      }
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 214,
      columnNumber: 7
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("main", {
      className: "mx-auto",
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        id: "banner",
        className: " relative bg-crew w-full  h-screen bg-cover flex flex-col justify-between",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_Layout_Header__WEBPACK_IMPORTED_MODULE_2__.default, {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 240,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "flex flex-col  md:absolute right-64 bottom-11   justify-center items-center",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            id: "cta2",
            className: "  text-3xl bg-yellow-300 uppercase italic font-bold  mb-2 px-16 py-4  text-iconColor",
            children: "Mint Here"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 243,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            className: "text-2xl font-bold text-gray-800 mb-4",
            children: showReleaseDate && coundownText
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 250,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 242,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 236,
        columnNumber: 9
      }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
        className: " text-gray-800 ",
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "post-content container mx-auto text-lg p-4 text-center md:text-left ",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "first-section",
            children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("h1", {
              className: "text-gray-900 text-left our-mission font-bold text-3xl border-b-2 pb-3 border-gray-800 md:w-1/4",
              children: "Our Mission"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 259,
              columnNumber: 15
            }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("section", {
              className: "grid md:grid-cols-2 items-center",
              children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "about-mission",
                children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: "Hi! Welcome to the Coalition Crew! This is a limited collection of 5000 unique Cheetah NFTs living on the Ethereum blockchain. This project is technically broken up into three collections."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 265,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: "1st Collection - Coalition Crew (OG COLLECTION) Only 1010 avail. *Update - Sold out in less than a day!*"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 266,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: ["2nd Collection - Auction Cheetahs - 20 avail. These qualify for VIP access to all live events. The auction begins Jan 8th. Please visit our ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
                    target: "_blank",
                    href: "http://discord.gg/3nKRBcDS33",
                    rel: "noreferrer",
                    "class": "underline",
                    children: " Discord"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 268,
                    columnNumber: 160
                  }, this), " for more info on this! "]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 268,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: "3rd Collection - Coalition Crew 2.0 - Only 3970 avail. This will wrap up the collection for the Coalition Crew project. Mint price is .09 ETH and you can mint up to 5. This collection will begin minting Jan 26th at 10am PST/1pm EST."
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 269,
                  columnNumber: 17
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: ["Out of the 5000 total, only 50 qualify for VIP access. Please visit our ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
                    target: "_blank",
                    href: "http://discord.gg/3nKRBcDS33",
                    rel: "noreferrer",
                    "class": "underline",
                    children: " Discord"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 270,
                    columnNumber: 92
                  }, this), " Discord for more info on which ones qualify for VIP."]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 270,
                  columnNumber: 17
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 263,
                columnNumber: 17
              }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                className: "flex items-center justify-center relative",
                children: [" ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(framer_motion__WEBPACK_IMPORTED_MODULE_12__.motion.div, {
                  initial: {
                    opacity: 0,
                    height: "20rem",
                    width: "20rem"
                  },
                  animate: {
                    opacity: 1,
                    height: "22rem",
                    width: "22rem"
                  },
                  className: "h-72  duration-300 w-72 md:w-96 md:h-96 bg-gray-800 rounded-full absolute z-0 md:left-38"
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 274,
                  columnNumber: 19
                }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_image__WEBPACK_IMPORTED_MODULE_5___default()), {
                  alt: "logo",
                  src: "/img/running-cheetah.gif",
                  width: 400,
                  className: "relative",
                  objectFit: "contain",
                  height: 450
                }, void 0, false, {
                  fileName: _jsxFileName,
                  lineNumber: 279,
                  columnNumber: 19
                }, this)]
              }, void 0, true, {
                fileName: _jsxFileName,
                lineNumber: 272,
                columnNumber: 17
              }, this)]
            }, void 0, true, {
              fileName: _jsxFileName,
              lineNumber: 262,
              columnNumber: 15
            }, this)]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 258,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 257,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "flex justify-center",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "w-36 h-2 flex  bg-gray-700 my-9"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 292,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 291,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          ref: ref,
          className: "gallery relative justify-items-center flex gap-8  md:grid-cols-4 ",
          children: (0,_utils_shuffle_image__WEBPACK_IMPORTED_MODULE_4__.default)(imagePath).map(function (image, index) {
            return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(framer_motion__WEBPACK_IMPORTED_MODULE_12__.motion.div, {
              className: "item min-w-full rounded-lg",
              animate: image.animation,
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)((next_image__WEBPACK_IMPORTED_MODULE_5___default()), {
                alt: "Cheetah",
                src: "/img/" + image.src,
                width: 620,
                height: 650
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 305,
                columnNumber: 17
              }, _this)
            }, index, false, {
              fileName: _jsxFileName,
              lineNumber: 300,
              columnNumber: 15
            }, _this);
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 295,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          "class": "flex flex-col  right-64 bottom-11   justify-center items-center",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("button", {
            id: "cta2",
            "class": " roadmap  text-3xl bg-yellow-300 uppercase italic font-bold  mb-2 px-16 py-4  text-iconColor",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
              target: "_blank",
              href: "https://drive.google.com/file/d/1JPAXXLlR-vTpeGe3rHmxeq4XC3US3hv5/view?usp=sharing",
              children: "Roadmap"
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 315,
              columnNumber: 132
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 315,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
            "class": "text-2xl font-bold text-gray-800 mb-4"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 316,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 314,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner2",
          "class": " relative bg-value w-full  h-screen bg-cover flex flex-col justify-between "
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 318,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "flex justify-center",
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "max-w-6xl overflow-hidden w-36 h-2 flex mt-8  bg-gray-800 "
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 322,
            columnNumber: 13
          }, this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 321,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner2",
          className: " relative bg-value w-full  h-screen bg-cover flex flex-col justify-between "
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 324,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner3",
          className: " relative bg-mentor w-full  h-screen bg-cover flex flex-col justify-between my-10"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 328,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner4",
          className: " relative bg-network1 w-full  h-screen bg-cover flex flex-col justify-between mb-10"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 332,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          id: "banner5",
          className: " relative bg-network2 w-full  h-screen bg-cover flex flex-col justify-between mb-10"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 336,
          columnNumber: 11
        }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
          className: "post-content container mx-auto text-lg p-4 text-center md:text-left ",
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "first-section",
            children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("section", {
              className: "fourth-section" // dangerouslySetInnerHTML={{ __html: data.acf.fourth_section }}
              ,
              children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
                children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("p", {
                  children: ["Once minted or bought on OpenSea, please fill out this", " ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("a", {
                    target: "_blank",
                    href: "https://www.gamechangersmovemement.com/crew",
                    rel: "noreferrer",
                    className: "underline",
                    children: "form"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 349,
                    columnNumber: 21
                  }, this), " ", "to get access to the Academy.", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 358,
                    columnNumber: 21
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("span", {
                    className: "font-bold",
                    children: "Note!"
                  }, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 359,
                    columnNumber: 21
                  }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 360,
                    columnNumber: 21
                  }, this), " If you sell your only cheetah, your access to the Academy will be suspended. You must own at least 1 to keep access. ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 362,
                    columnNumber: 29
                  }, this), " After 3 months all holders will receive their cheetah adoption \u201Cpaperwork\u201D. This includes a photo, an adoption certificate, a stuffed animal, a species card + more! ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 365,
                    columnNumber: 27
                  }, this), " We are launching a unique merchandise store for Coalition Crew Members to make custom gear. ", /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("br", {}, void 0, false, {
                    fileName: _jsxFileName,
                    lineNumber: 366,
                    columnNumber: 65
                  }, this), " All holders get automatic WL for future projects, as well as exclusive airdrops."]
                }, void 0, true, {
                  fileName: _jsxFileName,
                  lineNumber: 347,
                  columnNumber: 19
                }, this)
              }, void 0, false, {
                fileName: _jsxFileName,
                lineNumber: 346,
                columnNumber: 17
              }, this)
            }, void 0, false, {
              fileName: _jsxFileName,
              lineNumber: 342,
              columnNumber: 15
            }, this)
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 341,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_Home_Founder__WEBPACK_IMPORTED_MODULE_9__.default, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 373,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)("div", {
            className: "h-0.5 w-full bg-gray-700 my-8"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 374,
            columnNumber: 13
          }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(_components_Layout_Footer__WEBPACK_IMPORTED_MODULE_3__.default, {}, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 375,
            columnNumber: 13
          }, this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 340,
          columnNumber: 11
        }, this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 256,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 235,
      columnNumber: 7
    }, this)]
  }, void 0, true);
}

_s(Home, "Lqqk7nocCv117nQUjWydCl8unGQ=", false, function () {
  return [react_intersection_observer__WEBPACK_IMPORTED_MODULE_11__.useInView, _hooks_useCountDown__WEBPACK_IMPORTED_MODULE_8__.default, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation, framer_motion__WEBPACK_IMPORTED_MODULE_12__.useAnimation];
});

_c = Home;

var _c;

$RefreshReg$(_c, "Home");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvaW5kZXguanMiXSwibmFtZXMiOlsibXNwZWFrZXIiLCJuYW1lIiwiaXRlbXNUb1Nob3ciLCJIb21lIiwiZGF0YSIsInVzZUluVmlldyIsInRocmVzaG9sZCIsInJlZiIsImluVmlldyIsInVzZUNvdW50RG93biIsInNob3dSZWxlYXNlRGF0ZSIsImNvdW5kb3duVGV4dCIsImFuaW1hdGlvbiIsInVzZUFuaW1hdGlvbiIsImZpcnN0SW1nIiwic2Vjb25kSW1nIiwidGhpcmRJbWciLCJmb3VydGhJbWciLCJzbGlkZUxlZnQiLCJpbWFnZVBhdGgiLCJzcmMiLCJ1c2VTdGF0ZSIsInNob3VsZFNob3dBY3Rpb25zIiwic2V0U2hvdWxkU2hvd0FjdGlvbnMiLCJ1c2VFZmZlY3QiLCJzdGFydCIsIngiLCJ5Iiwib3BhY2l0eSIsInpJbmRleCIsInRyYW5zaXRpb24iLCJkdXJhdGlvbiIsImRlc2NyaXB0aW9uIiwidGl0bGUiLCJ0eXBlIiwibG9jYWxlIiwidXJsIiwicHJvY2VzcyIsImVudiIsIk5FWFRfUFVCTElDX0RPTUFJTiIsImltYWdlcyIsIndpZHRoIiwiaGVpZ2h0IiwiYWx0Iiwic2h1ZmZsZSIsIm1hcCIsImltYWdlIiwiaW5kZXgiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0NBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBLElBQU1BLFFBQVEsR0FBRyxDQUNmO0FBQUVDLE1BQUksRUFBRSxpQkFBUjtBQUEyQkMsYUFBVyxFQUFFO0FBQXhDLENBRGUsRUFFZjtBQUFFRCxNQUFJLEVBQUUsb0JBQVI7QUFBOEJDLGFBQVcsRUFBRTtBQUEzQyxDQUZlLEVBR2Y7QUFBRUQsTUFBSSxFQUFFLGlCQUFSO0FBQTJCQyxhQUFXLEVBQUU7QUFBeEMsQ0FIZSxFQUlmO0FBQUVELE1BQUksRUFBRSxtQkFBUjtBQUE2QkMsYUFBVyxFQUFFO0FBQTFDLENBSmUsRUFLZjtBQUFFRCxNQUFJLEVBQUUsYUFBUjtBQUF1QkMsYUFBVyxFQUFFO0FBQXBDLENBTGUsRUFNZjtBQUFFRCxNQUFJLEVBQUUsV0FBUjtBQUFxQkMsYUFBVyxFQUFFO0FBQWxDLENBTmUsQ0FBakI7O0FBUWUsU0FBU0MsSUFBVCxPQUF3QjtBQUFBOztBQUFBOztBQUFBLE1BQVJDLElBQVEsUUFBUkEsSUFBUTs7QUFBQSxtQkFDYkMsdUVBQVMsQ0FBQztBQUFFQyxhQUFTLEVBQUU7QUFBYixHQUFELENBREk7QUFBQSxNQUM3QkMsR0FENkIsY0FDN0JBLEdBRDZCO0FBQUEsTUFDeEJDLE1BRHdCLGNBQ3hCQSxNQUR3Qjs7QUFBQSxzQkFFS0MsNERBQVksQ0FBQyxxQkFBRCxDQUZqQjtBQUFBLE1BRTdCQyxlQUY2QixpQkFFN0JBLGVBRjZCO0FBQUEsTUFFWkMsWUFGWSxpQkFFWkEsWUFGWTs7QUFJckMsTUFBTUMsU0FBUyxHQUFHQyw0REFBWSxFQUE5QjtBQUNBLE1BQU1DLFFBQVEsR0FBR0QsNERBQVksRUFBN0I7QUFDQSxNQUFNRSxTQUFTLEdBQUdGLDREQUFZLEVBQTlCO0FBQ0EsTUFBTUcsUUFBUSxHQUFHSCw0REFBWSxFQUE3QjtBQUNBLE1BQU1JLFNBQVMsR0FBR0osNERBQVksRUFBOUI7QUFDQSxNQUFNSyxTQUFTLEdBQUdMLDREQUFZLEVBQTlCO0FBQ0EsTUFBTU0sU0FBUyxHQUFHLENBQ2hCO0FBQUVDLE9BQUcsRUFBRSx1QkFBUDtBQUFnQ1IsYUFBUyxFQUFFRztBQUEzQyxHQURnQixFQUVoQjtBQUFFSyxPQUFHLEVBQUUscUJBQVA7QUFBOEJSLGFBQVMsRUFBRUU7QUFBekMsR0FGZ0IsRUFHaEI7QUFBRU0sT0FBRyxFQUFFLHdCQUFQO0FBQWlDUixhQUFTLEVBQUVLO0FBQTVDLEdBSGdCLEVBSWhCO0FBQUVHLE9BQUcsRUFBRSxxQkFBUDtBQUE4QlIsYUFBUyxFQUFFRztBQUF6QyxHQUpnQixFQUtoQjtBQUFFSyxPQUFHLEVBQUUsdUJBQVA7QUFBZ0NSLGFBQVMsRUFBRUk7QUFBM0MsR0FMZ0IsRUFNaEI7QUFBRUksT0FBRyxFQUFFLHFCQUFQO0FBQThCUixhQUFTLEVBQUVJO0FBQXpDLEdBTmdCLEVBT2hCO0FBQUVJLE9BQUcsRUFBRSwrQkFBUDtBQUF3Q1IsYUFBUyxFQUFFSTtBQUFuRCxHQVBnQixFQVFoQjtBQUFFSSxPQUFHLEVBQUUsdUJBQVA7QUFBZ0NSLGFBQVMsRUFBRUk7QUFBM0MsR0FSZ0IsRUFTaEI7QUFBRUksT0FBRyxFQUFFLHdCQUFQO0FBQWlDUixhQUFTLEVBQUVJO0FBQTVDLEdBVGdCLEVBVWhCO0FBQUVJLE9BQUcsRUFBRSwyQkFBUDtBQUFvQ1IsYUFBUyxFQUFFSTtBQUEvQyxHQVZnQixFQVdoQjtBQUFFSSxPQUFHLEVBQUUsOEJBQVA7QUFBdUNSLGFBQVMsRUFBRUk7QUFBbEQsR0FYZ0IsRUFZaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQztBQUFFSSxPQUFHLEVBQUUsaUJBQVA7QUFBMEJSLGFBQVMsRUFBRUU7QUFBckMsR0E1QmUsRUE2QmY7QUFBRU0sT0FBRyxFQUFFLGlCQUFQO0FBQTBCUixhQUFTLEVBQUVHO0FBQXJDLEdBN0JlLEVBOEJmO0FBQUVLLE9BQUcsRUFBRSxpQkFBUDtBQUEwQlIsYUFBUyxFQUFFSTtBQUFyQyxHQTlCZSxFQStCZjtBQUFFSSxPQUFHLEVBQUUsaUJBQVA7QUFBMEJSLGFBQVMsRUFBRUs7QUFBckMsR0EvQmUsRUFnQ2Y7QUFBRUcsT0FBRyxFQUFFLGlCQUFQO0FBQTBCUixhQUFTLEVBQUVFO0FBQXJDLEdBaENlLEVBaUNmO0FBQUVNLE9BQUcsRUFBRSxpQkFBUDtBQUEwQlIsYUFBUyxFQUFFRztBQUFyQyxHQWpDZSxFQWtDZjtBQUFFSyxPQUFHLEVBQUUsaUJBQVA7QUFBMEJSLGFBQVMsRUFBRUk7QUFBckMsR0FsQ2UsRUFtQ2Y7QUFBRUksT0FBRyxFQUFFLGlCQUFQO0FBQTBCUixhQUFTLEVBQUVLO0FBQXJDLEdBbkNlLEVBb0NmO0FBQUVHLE9BQUcsRUFBRSxpQkFBUDtBQUEwQlIsYUFBUyxFQUFFRTtBQUFyQyxHQXBDZSxFQXFDZjtBQUFFTSxPQUFHLEVBQUUsbUJBQVA7QUFBNEJSLGFBQVMsRUFBRUU7QUFBdkMsR0FyQ2UsRUFzQ2Y7QUFBRU0sT0FBRyxFQUFFLGtCQUFQO0FBQTJCUixhQUFTLEVBQUVHO0FBQXRDLEdBdENlLENBdUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQWhEZ0IsR0FBbEI7O0FBVnFDLGtCQTZEYU0sK0NBQVEsQ0FBQyxJQUFELENBN0RyQjtBQUFBLE1BNkQ5QkMsaUJBN0Q4QjtBQUFBLE1BNkRYQyxvQkE3RFc7O0FBOERyQ0Msa0RBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSWhCLE1BQUosRUFBWTtBQUNWSSxlQUFTLENBQUNhLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLENBRFc7QUFFZEMsU0FBQyxFQUFFLENBRlc7QUFHZEMsZUFBTyxFQUFFLENBSEs7QUFJZEMsY0FBTSxFQUFFLENBSk07QUFLZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMRSxPQUFoQjtBQU9BakIsY0FBUSxDQUFDVyxLQUFULENBQWU7QUFDYkMsU0FBQyxFQUFFLENBRFU7QUFFYkMsU0FBQyxFQUFFLENBRlU7QUFHYkMsZUFBTyxFQUFFLENBSEk7QUFJYkMsY0FBTSxFQUFFLENBSks7QUFLYkMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMQyxPQUFmO0FBT0FoQixlQUFTLENBQUNVLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLENBRFc7QUFFZEMsU0FBQyxFQUFFLENBRlc7QUFHZEMsZUFBTyxFQUFFLENBSEs7QUFJZEMsY0FBTSxFQUFFLENBSk07QUFLZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMRSxPQUFoQjtBQU9BZixjQUFRLENBQUNTLEtBQVQsQ0FBZTtBQUNiQyxTQUFDLEVBQUUsQ0FEVTtBQUViQyxTQUFDLEVBQUUsQ0FGVTtBQUdiQyxlQUFPLEVBQUUsQ0FISTtBQUliQyxjQUFNLEVBQUUsQ0FKSztBQUtiQyxrQkFBVSxFQUFFO0FBQUVDLGtCQUFRLEVBQUU7QUFBWjtBQUxDLE9BQWY7QUFPQWQsZUFBUyxDQUFDUSxLQUFWLENBQWdCO0FBQ2RDLFNBQUMsRUFBRSxDQURXO0FBRWRDLFNBQUMsRUFBRSxDQUZXO0FBR2RDLGVBQU8sRUFBRSxDQUhLO0FBSWRDLGNBQU0sRUFBRSxDQUpNO0FBS2RDLGtCQUFVLEVBQUU7QUFBRUMsa0JBQVEsRUFBRTtBQUFaO0FBTEUsT0FBaEI7QUFPQWIsZUFBUyxDQUFDTyxLQUFWLENBQWdCO0FBQ2RDLFNBQUMsRUFBRSxDQURXO0FBRWRDLFNBQUMsRUFBRSxDQUZXO0FBR2RDLGVBQU8sRUFBRSxDQUhLO0FBSWRDLGNBQU0sRUFBRSxDQUpNO0FBS2RDLGtCQUFVLEVBQUU7QUFBRUMsa0JBQVEsRUFBRTtBQUFaO0FBTEUsT0FBaEI7QUFPRDs7QUFDRCxRQUFJLENBQUN2QixNQUFMLEVBQWE7QUFDWEksZUFBUyxDQUFDYSxLQUFWLENBQWdCO0FBQ2RDLFNBQUMsRUFBRSxDQURXO0FBRWRDLFNBQUMsRUFBRSxHQUZXO0FBR2RDLGVBQU8sRUFBRSxDQUhLO0FBSWRDLGNBQU0sRUFBRSxDQUFDLENBSks7QUFNZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFORSxPQUFoQjtBQVFBakIsY0FBUSxDQUFDVyxLQUFULENBQWU7QUFDYkMsU0FBQyxFQUFFLENBRFU7QUFFYkMsU0FBQyxFQUFFLEdBRlU7QUFHYkMsZUFBTyxFQUFFLENBSEk7QUFJYkMsY0FBTSxFQUFFLENBQUMsQ0FKSTtBQUtiQyxrQkFBVSxFQUFFO0FBQUVDLGtCQUFRLEVBQUU7QUFBWjtBQUxDLE9BQWY7QUFPQWhCLGVBQVMsQ0FBQ1UsS0FBVixDQUFnQjtBQUNkQyxTQUFDLEVBQUUsQ0FEVztBQUVkQyxTQUFDLEVBQUUsR0FGVztBQUlkQyxlQUFPLEVBQUUsQ0FKSztBQUtkQyxjQUFNLEVBQUUsQ0FMTTtBQU1kQyxrQkFBVSxFQUFFO0FBQUVDLGtCQUFRLEVBQUU7QUFBWjtBQU5FLE9BQWhCO0FBUUFmLGNBQVEsQ0FBQ1MsS0FBVCxDQUFlO0FBQ2JDLFNBQUMsRUFBRSxDQURVO0FBRWJDLFNBQUMsRUFBRSxHQUZVO0FBSWJDLGVBQU8sRUFBRSxDQUpJO0FBS2JDLGNBQU0sRUFBRSxDQUxLO0FBTWJDLGtCQUFVLEVBQUU7QUFBRUMsa0JBQVEsRUFBRTtBQUFaO0FBTkMsT0FBZjtBQVFBZCxlQUFTLENBQUNRLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLENBRFc7QUFFZEMsU0FBQyxFQUFFLEdBRlc7QUFJZEMsZUFBTyxFQUFFLENBSks7QUFLZEMsY0FBTSxFQUFFLENBTE07QUFNZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFORSxPQUFoQjtBQVNBYixlQUFTLENBQUNPLEtBQVYsQ0FBZ0I7QUFDZEMsU0FBQyxFQUFFLEdBRFc7QUFFZEMsU0FBQyxFQUFFLENBRlc7QUFHZEMsZUFBTyxFQUFFLENBSEs7QUFJZEMsY0FBTSxFQUFFLENBSk07QUFLZEMsa0JBQVUsRUFBRTtBQUFFQyxrQkFBUSxFQUFFO0FBQVo7QUFMRSxPQUFoQjtBQU9EO0FBQ0YsR0E5RlEsRUE4Rk4sQ0FBQ3ZCLE1BQUQsQ0E5Rk0sQ0FBVDtBQStGQSxzQkFDRTtBQUFBLDRCQUNFLDhEQUFDLDZDQUFEO0FBQ0UsV0FBSyxFQUFFLHNCQURUO0FBRUUsaUJBQVcsRUFBQyw2V0FGZDtBQUdFLGVBQVMsRUFBRTtBQUNUd0IsbUJBQVcsRUFDVCw2V0FGTztBQUdUQyxhQUFLLEVBQUUsc0JBSEU7QUFJVEMsWUFBSSxFQUFFLFNBSkc7QUFLVEMsY0FBTSxFQUFFLE9BTEM7QUFNVEMsV0FBRyxFQUFFQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsa0JBTlI7QUFPVEMsY0FBTSxFQUFFLENBQ047QUFDRUosYUFBRyxFQUFFLG9GQURQO0FBRUVLLGVBQUssRUFBRSxJQUZUO0FBR0VDLGdCQUFNLEVBQUUsR0FIVjtBQUlFQyxhQUFHLEVBQUU7QUFKUCxTQURNO0FBUEM7QUFIYjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFzQkU7QUFBTSxlQUFTLEVBQUMsU0FBaEI7QUFBQSw4QkFDRTtBQUNFLFVBQUUsRUFBQyxRQURMO0FBRUUsaUJBQVMsRUFBQywyRUFGWjtBQUFBLGdDQUlFLDhEQUFDLDhEQUFEO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBSkYsZUFNRTtBQUFLLG1CQUFTLEVBQUMsNkVBQWY7QUFBQSxrQ0FDRTtBQUNFLGNBQUUsRUFBQyxNQURMO0FBRUUscUJBQVMsRUFBQyxzRkFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQVFFO0FBQU0scUJBQVMsRUFBQyx1Q0FBaEI7QUFBQSxzQkFDR2pDLGVBQWUsSUFBSUM7QUFEdEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFSRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBTkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGNBREYsZUFxQkU7QUFBSyxpQkFBUyxFQUFDLGlCQUFmO0FBQUEsZ0NBQ0U7QUFBSyxtQkFBUyxFQUFDLHNFQUFmO0FBQUEsaUNBQ0U7QUFBSyxxQkFBUyxFQUFDLGVBQWY7QUFBQSxvQ0FDRTtBQUFJLHVCQUFTLEVBQUMsaUdBQWQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsb0JBREYsZUFJRTtBQUFTLHVCQUFTLEVBQUMsa0NBQW5CO0FBQUEsc0NBQ0U7QUFDRSx5QkFBUyxFQUFDLGVBRFo7QUFBQSx3Q0FFQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSx3QkFGQSxlQUdBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUhBLGVBS0E7QUFBQSwwTEFBK0k7QUFBRywwQkFBTSxFQUFDLFFBQVY7QUFBbUIsd0JBQUksRUFBQyw4QkFBeEI7QUFBdUQsdUJBQUcsRUFBQyxZQUEzRDtBQUF3RSw2QkFBTSxXQUE5RTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFBL0k7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHdCQUxBLGVBTUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBTkEsZUFPQTtBQUFBLHNIQUEyRTtBQUFHLDBCQUFNLEVBQUMsUUFBVjtBQUFtQix3QkFBSSxFQUFDLDhCQUF4QjtBQUF1RCx1QkFBRyxFQUFDLFlBQTNEO0FBQXdFLDZCQUFNLFdBQTlFO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQUEzRTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBUEE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQURGLGVBVUU7QUFBSyx5QkFBUyxFQUFDLDJDQUFmO0FBQUEsMkJBQ0csR0FESCxlQUVFLDhEQUFDLHNEQUFEO0FBQ0UseUJBQU8sRUFBRTtBQUFFaUIsMkJBQU8sRUFBRSxDQUFYO0FBQWNjLDBCQUFNLEVBQUUsT0FBdEI7QUFBK0JELHlCQUFLLEVBQUU7QUFBdEMsbUJBRFg7QUFFRSx5QkFBTyxFQUFFO0FBQUViLDJCQUFPLEVBQUUsQ0FBWDtBQUFjYywwQkFBTSxFQUFFLE9BQXRCO0FBQStCRCx5QkFBSyxFQUFFO0FBQXRDLG1CQUZYO0FBR0UsMkJBQVMsRUFBQztBQUhaO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBRkYsZUFPRSw4REFBQyxtREFBRDtBQUNFLHFCQUFHLEVBQUUsTUFEUDtBQUVFLHFCQUFHLDRCQUZMO0FBR0UsdUJBQUssRUFBRSxHQUhUO0FBSUUsMkJBQVMsRUFBQyxVQUpaO0FBS0UsMkJBQVMsRUFBQyxTQUxaO0FBTUUsd0JBQU0sRUFBRTtBQU5WO0FBQUE7QUFBQTtBQUFBO0FBQUEsd0JBUEY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHNCQVZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxvQkFKRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQURGLGVBbUNFO0FBQUssbUJBQVMsRUFBQyxxQkFBZjtBQUFBLGlDQUNFO0FBQUsscUJBQVMsRUFBQztBQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQW5DRixlQXVDRTtBQUNFLGFBQUcsRUFBRWxDLEdBRFA7QUFFRSxtQkFBUyxFQUFDLG1FQUZaO0FBQUEsb0JBSUdxQyw2REFBTyxDQUFDekIsU0FBRCxDQUFQLENBQW1CMEIsR0FBbkIsQ0FBdUIsVUFBQ0MsS0FBRCxFQUFRQyxLQUFSO0FBQUEsZ0NBQ3RCLDhEQUFDLHNEQUFEO0FBQ0UsdUJBQVMsRUFBQyw0QkFEWjtBQUVFLHFCQUFPLEVBQUVELEtBQUssQ0FBQ2xDLFNBRmpCO0FBQUEscUNBS0UsOERBQUMsbURBQUQ7QUFDRSxtQkFBRyxFQUFFLFNBRFA7QUFFRSxtQkFBRyxFQUFFLFVBQVVrQyxLQUFLLENBQUMxQixHQUZ2QjtBQUdFLHFCQUFLLEVBQUUsR0FIVDtBQUlFLHNCQUFNLEVBQUU7QUFKVjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBTEYsZUFHTzJCLEtBSFA7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFEc0I7QUFBQSxXQUF2QjtBQUpIO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBdkNGLGVBMERFO0FBQUssbUJBQU0saUVBQVg7QUFBQSxrQ0FDRTtBQUFRLGNBQUUsRUFBQyxNQUFYO0FBQWtCLHFCQUFNLDhGQUF4QjtBQUFBLG1DQUF1SDtBQUFHLG9CQUFNLEVBQUMsUUFBVjtBQUFtQixrQkFBSSxFQUFDLG9GQUF4QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUF2SDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQURGLGVBRUU7QUFBTSxxQkFBTTtBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUEsa0JBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQTFERixlQThERTtBQUFLLFlBQUUsRUFBQyxTQUFSO0FBQWtCLG1CQUFNO0FBQXhCO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBOURGLGVBaUVFO0FBQUssbUJBQVMsRUFBQyxxQkFBZjtBQUFBLGlDQUNFO0FBQUsscUJBQVMsRUFBQztBQUFmO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQWpFRixlQW9FRTtBQUNFLFlBQUUsRUFBQyxTQURMO0FBRUUsbUJBQVMsRUFBQztBQUZaO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBcEVGLGVBd0VFO0FBQ0UsWUFBRSxFQUFDLFNBREw7QUFFRSxtQkFBUyxFQUFDO0FBRlo7QUFBQTtBQUFBO0FBQUE7QUFBQSxnQkF4RUYsZUE0RUU7QUFDRSxZQUFFLEVBQUMsU0FETDtBQUVFLG1CQUFTLEVBQUM7QUFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBLGdCQTVFRixlQWdGRTtBQUNFLFlBQUUsRUFBQyxTQURMO0FBRUUsbUJBQVMsRUFBQztBQUZaO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBaEZGLGVBb0ZFO0FBQUssbUJBQVMsRUFBQyxzRUFBZjtBQUFBLGtDQUNFO0FBQUsscUJBQVMsRUFBQyxlQUFmO0FBQUEsbUNBQ0U7QUFDRSx1QkFBUyxFQUFDLGdCQURaLENBRUU7QUFGRjtBQUFBLHFDQUlFO0FBQUEsdUNBQ0U7QUFBQSx1RkFDeUQsR0FEekQsZUFFRTtBQUNFLDBCQUFNLEVBQUMsUUFEVDtBQUVFLHdCQUFJLEVBQUMsNkNBRlA7QUFHRSx1QkFBRyxFQUFDLFlBSE47QUFJRSw2QkFBUyxFQUFDLFdBSlo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBRkYsRUFTTyxHQVRQLGdEQVdFO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBWEYsZUFZRTtBQUFNLDZCQUFTLEVBQUMsV0FBaEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBWkYsZUFhRTtBQUFBO0FBQUE7QUFBQTtBQUFBLDBCQWJGLHlJQWVVO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBZlYsbU1Ba0JRO0FBQUE7QUFBQTtBQUFBO0FBQUEsMEJBbEJSLGdIQW1COEM7QUFBQTtBQUFBO0FBQUE7QUFBQSwwQkFuQjlDO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFKRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxrQkFERixlQWlDRSw4REFBQyw2REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQWpDRixlQWtDRTtBQUFLLHFCQUFTLEVBQUM7QUFBZjtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQWxDRixlQW1DRSw4REFBQyw4REFBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGtCQW5DRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZ0JBcEZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQXJCRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsWUF0QkY7QUFBQSxrQkFERjtBQXlLRDs7R0F0VXVCNUMsSTtVQUNFRSxtRSxFQUNrQkksd0QsRUFFeEJJLHdELEVBQ0RBLHdELEVBQ0NBLHdELEVBQ0RBLHdELEVBQ0NBLHdELEVBQ0FBLHdEOzs7S0FUSVYsSSIsImZpbGUiOiJzdGF0aWMvd2VicGFjay9wYWdlcy9pbmRleC5mZmE2NTIxZTA2MjU4ZWY0YzhhMS5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmV4dFNlbyB9IGZyb20gXCJuZXh0LXNlb1wiO1xuaW1wb3J0IEhlYWRlciBmcm9tIFwiLi4vY29tcG9uZW50cy9MYXlvdXQvSGVhZGVyXCI7XG5pbXBvcnQgRm9vdGVyIGZyb20gXCIuLi9jb21wb25lbnRzL0xheW91dC9Gb290ZXJcIjtcbmltcG9ydCBzaHVmZmxlIGZyb20gXCIuLi91dGlscy9zaHVmZmxlLWltYWdlXCI7XG5pbXBvcnQgSW1hZ2UgZnJvbSBcIm5leHQvaW1hZ2VcIjtcbmltcG9ydCB7IG1vdGlvbiwgdXNlQW5pbWF0aW9uIH0gZnJvbSBcImZyYW1lci1tb3Rpb25cIjtcbmltcG9ydCB7IEdldFN0YXRpY1Byb3BzIH0gZnJvbSBcIm5leHRcIjtcbmltcG9ydCB7IHVzZVN0YXRlLCB1c2VFZmZlY3QgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCB7IHVzZUluVmlldyB9IGZyb20gXCJyZWFjdC1pbnRlcnNlY3Rpb24tb2JzZXJ2ZXJcIjtcbmltcG9ydCB1c2VDb3VudERvd24gZnJvbSBcIi4uL2hvb2tzL3VzZUNvdW50RG93blwiO1xuaW1wb3J0IEZvdW5kZXIgZnJvbSBcIi4uL2NvbXBvbmVudHMvSG9tZS9Gb3VuZGVyXCI7XG5pbXBvcnQgZHluYW1pYyBmcm9tIFwibmV4dC9keW5hbWljXCI7XG5cbi8vIHR5cGUgUHJvcHMgPSB7XG4vLyAgIGRhdGE6IHtcbi8vICAgICB0aXRsZTogeyByZW5kZXJlZDogc3RyaW5nIH07XG4vLyAgICAgY29udGVudDogeyByZW5kZXJlZDogc3RyaW5nIH07XG4vLyAgICAgYWNmOiB7XG4vLyAgICAgICBwcm9maWxlX2ltYWdlX3VybDogc3RyaW5nO1xuLy8gICAgICAgZmlyc3Rfc2VjdGlvbjogc3RyaW5nO1xuLy8gICAgICAgc2Vjb25kX3NlY3Rpb246IHN0cmluZztcbi8vICAgICAgIHRoaXJkX3NlY3Rpb246IHN0cmluZztcbi8vICAgICAgIGZvdXJ0aF9zZWN0aW9uOiBzdHJpbmc7XG4vLyAgICAgICB2YWx1ZV9maXJzdDoge1xuLy8gICAgICAgICBpbWFnZV86IHsgc2l6ZXM6IHsgbGFyZ2U6IHN0cmluZyB9OyB1cmw6IHN0cmluZyB9O1xuLy8gICAgICAgICBpbmZvOiBzdHJpbmc7XG4vLyAgICAgICB9O1xuLy8gICAgICAgdmFsdWVfc2Vjb25kOiB7XG4vLyAgICAgICAgIGltYWdlXzogeyBzaXplczogeyBsYXJnZTogc3RyaW5nIH07IHVybDogc3RyaW5nIH07XG4vLyAgICAgICAgIGluZm86IHN0cmluZztcbi8vICAgICAgIH07XG4vLyAgICAgICB2YWx1ZV90aGlyZDoge1xuLy8gICAgICAgICBpbWFnZV86IHN0cmluZztcbi8vICAgICAgICAgaW5mbzogc3RyaW5nO1xuLy8gICAgICAgfTtcbi8vICAgICAgIHZhbHVlX2ZvdXJ0aDoge1xuLy8gICAgICAgICBpbWFnZV86IHsgc2l6ZXM6IHsgbGFyZ2U6IHN0cmluZyB9OyB1cmw6IHN0cmluZyB9O1xuLy8gICAgICAgICBpbmZvOiBzdHJpbmc7XG4vLyAgICAgICB9O1xuLy8gICAgICAgdmFsdWVfZmlmdGg6IHtcbi8vICAgICAgICAgaW1hZ2VfOiB7IHNpemVzOiB7IGxhcmdlOiBzdHJpbmcgfTsgdXJsOiBzdHJpbmcgfTtcbi8vICAgICAgICAgaW5mbzogc3RyaW5nO1xuLy8gICAgICAgfTtcbi8vICAgICB9O1xuLy8gICB9O1xuLy8gfTtcbmNvbnN0IG1zcGVha2VyID0gW1xuICB7IG5hbWU6IFwiR2FyeSBWYXluZXJjaHVrXCIsIGl0ZW1zVG9TaG93OiBcIkdhcnktVmF5bmVyY2h1ay1taW4uanBnXCIgfSxcbiAgeyBuYW1lOiBcIkFyaWFubmEgSHVmZmluZ3RvblwiLCBpdGVtc1RvU2hvdzogXCJBcmlhbm5hLUh1ZmZpbmd0b24tbWluLmpwZ1wiIH0sXG4gIHsgbmFtZTogXCJSb2JlcnQgS2l5b3Nha2lcIiwgaXRlbXNUb1Nob3c6IFwiUm9iZXJ0LUtpeW9zYWtpLmpwZ1wiIH0sXG4gIHsgbmFtZTogXCJQYXRyaWNrIEJldC1EYXZpZFwiLCBpdGVtc1RvU2hvdzogXCJQYXRyaWNrLWJldC1kYXZpZC1taW4uanBnXCIgfSxcbiAgeyBuYW1lOiBcIlRpbSBGZXJyaXNzXCIsIGl0ZW1zVG9TaG93OiBcIlRpbS1GZXJyaXNzLW1pbi5wbmdcIiB9LFxuICB7IG5hbWU6IFwiVGFpIExvcGV6XCIsIGl0ZW1zVG9TaG93OiBcIlRhaS1Mb3Blei1taW4uanBnXCIgfSxcbl07XG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBIb21lKHsgZGF0YSB9KSB7XG4gIGNvbnN0IHsgcmVmLCBpblZpZXcgfSA9IHVzZUluVmlldyh7IHRocmVzaG9sZDogMC4yIH0pO1xuICBjb25zdCB7IHNob3dSZWxlYXNlRGF0ZSwgY291bmRvd25UZXh0IH0gPSB1c2VDb3VudERvd24oXCIyMDIxLTEyLTA0IDEwOjAwIEFNXCIpO1xuXG4gIGNvbnN0IGFuaW1hdGlvbiA9IHVzZUFuaW1hdGlvbigpO1xuICBjb25zdCBmaXJzdEltZyA9IHVzZUFuaW1hdGlvbigpO1xuICBjb25zdCBzZWNvbmRJbWcgPSB1c2VBbmltYXRpb24oKTtcbiAgY29uc3QgdGhpcmRJbWcgPSB1c2VBbmltYXRpb24oKTtcbiAgY29uc3QgZm91cnRoSW1nID0gdXNlQW5pbWF0aW9uKCk7XG4gIGNvbnN0IHNsaWRlTGVmdCA9IHVzZUFuaW1hdGlvbigpO1xuICBjb25zdCBpbWFnZVBhdGggPSBbXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L2JveGVyIDIucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L2JveGVyLnBuZ1wiLCBhbmltYXRpb246IGZpcnN0SW1nIH0sXG4gICAgeyBzcmM6IFwic2xpZGVzaG93L2Rlbm5pcy0zLnBuZ1wiLCBhbmltYXRpb246IGZvdXJ0aEltZyB9LFxuICAgIHsgc3JjOiBcInNsaWRlc2hvdy9kaXZlci5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvbW9ic3Rlci5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIHsgc3JjOiBcInNsaWRlc2hvdy9waWxvdC5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIHsgc3JjOiBcInNsaWRlc2hvdy9waXJhdGVzIGNoZWV0YWgucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICB7IHNyYzogXCJzbGlkZXNob3cvcnVzc2VsbC5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIHsgc3JjOiBcInNsaWRlc2hvdy9zdXJmZXItMi5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIHsgc3JjOiBcInNsaWRlc2hvdy90aWdlciB3b29kcy5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIHsgc3JjOiBcInNsaWRlc2hvdy96b21iaWUgY2hlZXRhaC5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy85ODkucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvOTk1LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzEwMTQucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTAwNy5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMDE3LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzEwNS5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMDcucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTA4LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzEwOS5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMTAucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTEyLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzEyNC5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcInNsaWRlc2hvdy8xMzkucG5nXCIsIGFuaW1hdGlvbjogdGhpcmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCJzbGlkZXNob3cvMTQwLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzkyLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgLy8geyBzcmM6IFwic2xpZGVzaG93LzkzLnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy8xLmdpZlwiLCBhbmltYXRpb246IGZpcnN0SW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy8yLmdpZlwiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvMy5wbmdcIiwgYW5pbWF0aW9uOiB0aGlyZEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvNC5wbmdcIiwgYW5pbWF0aW9uOiBmb3VydGhJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzUucG5nXCIsIGFuaW1hdGlvbjogZmlyc3RJbWcgfSxcbiAgICAgeyBzcmM6IFwic2xpZGVzaG93LzYucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy83LnBuZ1wiLCBhbmltYXRpb246IHRoaXJkSW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy84LnBuZ1wiLCBhbmltYXRpb246IGZvdXJ0aEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvOS5wbmdcIiwgYW5pbWF0aW9uOiBmaXJzdEltZyB9LFxuICAgICB7IHNyYzogXCJzbGlkZXNob3cvMTAuanBlZ1wiLCBhbmltYXRpb246IGZpcnN0SW1nIH0sXG4gICAgIHsgc3JjOiBcInNsaWRlc2hvdy8xMS5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCIxOS5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCIxMzkucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgLy8geyBzcmM6IFwiMTQ3LnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcIjE0OC5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCIxNDkucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgLy8geyBzcmM6IFwiNTUyMS5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCI1NjkucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gICAgLy8geyBzcmM6IFwiNTIwLnBuZ1wiLCBhbmltYXRpb246IHNlY29uZEltZyB9LFxuICAgIC8vIHsgc3JjOiBcIjMzNS5wbmdcIiwgYW5pbWF0aW9uOiBzZWNvbmRJbWcgfSxcbiAgICAvLyB7IHNyYzogXCIxNjIucG5nXCIsIGFuaW1hdGlvbjogc2Vjb25kSW1nIH0sXG4gIF07XG5cbiAgY29uc3QgW3Nob3VsZFNob3dBY3Rpb25zLCBzZXRTaG91bGRTaG93QWN0aW9uc10gPSB1c2VTdGF0ZSh0cnVlKTtcbiAgdXNlRWZmZWN0KCgpID0+IHtcbiAgICBpZiAoaW5WaWV3KSB7XG4gICAgICBhbmltYXRpb24uc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAwLFxuICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDAuNiB9LFxuICAgICAgfSk7XG4gICAgICBmaXJzdEltZy5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDAsXG4gICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgIHpJbmRleDogMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMC44IH0sXG4gICAgICB9KTtcbiAgICAgIHNlY29uZEltZy5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDAsXG4gICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgIHpJbmRleDogMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMC45IH0sXG4gICAgICB9KTtcbiAgICAgIHRoaXJkSW1nLnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMCxcbiAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgekluZGV4OiAxLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAxLjIgfSxcbiAgICAgIH0pO1xuICAgICAgZm91cnRoSW1nLnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMCxcbiAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgekluZGV4OiAxLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAxLjQgfSxcbiAgICAgIH0pO1xuICAgICAgc2xpZGVMZWZ0LnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMCxcbiAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgekluZGV4OiAxLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAxIH0sXG4gICAgICB9KTtcbiAgICB9XG4gICAgaWYgKCFpblZpZXcpIHtcbiAgICAgIGFuaW1hdGlvbi5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDI1OCxcbiAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgekluZGV4OiAtMSxcblxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAxIH0sXG4gICAgICB9KTtcbiAgICAgIGZpcnN0SW1nLnN0YXJ0KHtcbiAgICAgICAgeDogMCxcbiAgICAgICAgeTogMjUyLFxuICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICB6SW5kZXg6IC0xLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAxIH0sXG4gICAgICB9KTtcbiAgICAgIHNlY29uZEltZy5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDI1MixcblxuICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDEgfSxcbiAgICAgIH0pO1xuICAgICAgdGhpcmRJbWcuc3RhcnQoe1xuICAgICAgICB4OiAwLFxuICAgICAgICB5OiAyNTIsXG5cbiAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgekluZGV4OiAxLFxuICAgICAgICB0cmFuc2l0aW9uOiB7IGR1cmF0aW9uOiAxIH0sXG4gICAgICB9KTtcbiAgICAgIGZvdXJ0aEltZy5zdGFydCh7XG4gICAgICAgIHg6IDAsXG4gICAgICAgIHk6IDI1MixcblxuICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICB6SW5kZXg6IDEsXG4gICAgICAgIHRyYW5zaXRpb246IHsgZHVyYXRpb246IDEgfSxcbiAgICAgIH0pO1xuXG4gICAgICBzbGlkZUxlZnQuc3RhcnQoe1xuICAgICAgICB4OiAxNTYsXG4gICAgICAgIHk6IDAsXG4gICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgIHpJbmRleDogMSxcbiAgICAgICAgdHJhbnNpdGlvbjogeyBkdXJhdGlvbjogMSB9LFxuICAgICAgfSk7XG4gICAgfVxuICB9LCBbaW5WaWV3XSk7XG4gIHJldHVybiAoXG4gICAgPD5cbiAgICAgIDxOZXh0U2VvXG4gICAgICAgIHRpdGxlPXtcIkhvbWUgLSBDb2FsaXRpb25jcmV3XCJ9XG4gICAgICAgIGRlc2NyaXB0aW9uPVwiWW91dHViZSBUd2l0dGVyIEluc3RhZ3JhbSBXZWxjb21lIHRvIHRoZSBDb2FsaXRpb24gTWludCBoZXJlIFRoaXMgaXMgdGhlIE5GVCBmb3IgR2FtZSBDaGFuZ2Vycy4gVGhlIENvYWxpdGlvbiBDcmV3IGlzIGFuIGV4Y2x1c2l2ZSBjb2xsZWN0aW9uIG9mIDcxMDAgdW5pcXVlIENoZWV0YWggTkZUcyBsaXZpbmcgb24gdGhlIEV0aGVyZXVtIGJsb2NrY2hhaW4uIEl04oCZcyBlc3RpbWF0ZWQgdGhhdCBhcyBvZiAyMDIxLCB0aGVyZSBhcmUgb25seSA3MTAwIGNoZWV0YWhzIGxlZnQgaW4gdGhlIHdpbGQuIENoZWV0YWhzIGFyZSBjdXJyZW50bHkgbGlzdGVkIGFzIHZ1bG5lcmFibGUgYW5kICZoZWxsaXA7IEhvbWUgUmVhZCBNb3JlICZyYXF1bztcIlxuICAgICAgICBvcGVuR3JhcGg9e3tcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcbiAgICAgICAgICAgIFwiWW91dHViZSBUd2l0dGVyIEluc3RhZ3JhbSBXZWxjb21lIHRvIHRoZSBDb2FsaXRpb24gTWludCBoZXJlIFRoaXMgaXMgdGhlIE5GVCBmb3IgR2FtZSBDaGFuZ2Vycy4gVGhlIENvYWxpdGlvbiBDcmV3IGlzIGFuIGV4Y2x1c2l2ZSBjb2xsZWN0aW9uIG9mIDcxMDAgdW5pcXVlIENoZWV0YWggTkZUcyBsaXZpbmcgb24gdGhlIEV0aGVyZXVtIGJsb2NrY2hhaW4uIEl04oCZcyBlc3RpbWF0ZWQgdGhhdCBhcyBvZiAyMDIxLCB0aGVyZSBhcmUgb25seSA3MTAwIGNoZWV0YWhzIGxlZnQgaW4gdGhlIHdpbGQuIENoZWV0YWhzIGFyZSBjdXJyZW50bHkgbGlzdGVkIGFzIHZ1bG5lcmFibGUgYW5kICZoZWxsaXA7IEhvbWUgUmVhZCBNb3JlICZyYXF1bztcIixcbiAgICAgICAgICB0aXRsZTogXCJIb21lIC0gQ29hbGl0aW9uY3Jld1wiLFxuICAgICAgICAgIHR5cGU6IFwid2Vic2l0ZVwiLFxuICAgICAgICAgIGxvY2FsZTogXCJlbl9VU1wiLFxuICAgICAgICAgIHVybDogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfRE9NQUlOLFxuICAgICAgICAgIGltYWdlczogW1xuICAgICAgICAgICAge1xuICAgICAgICAgICAgICB1cmw6IFwiaHR0cDovL2NvYWxpdGlvbmNyZXcuY29tL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDIxLzEwL1RDQ18xLVRyYW5zcGFyZW50LTEwMjR4OTgyLnBuZ1wiLFxuICAgICAgICAgICAgICB3aWR0aDogMTIwMCxcbiAgICAgICAgICAgICAgaGVpZ2h0OiA2MzAsXG4gICAgICAgICAgICAgIGFsdDogXCJjb2FsaXRpb24gY3JldyBsb2dvXCIsXG4gICAgICAgICAgICB9LFxuICAgICAgICAgIF0sXG4gICAgICAgIH19XG4gICAgICAvPlxuXG4gICAgICA8bWFpbiBjbGFzc05hbWU9XCJteC1hdXRvXCI+XG4gICAgICAgIDxkaXZcbiAgICAgICAgICBpZD1cImJhbm5lclwiXG4gICAgICAgICAgY2xhc3NOYW1lPVwiIHJlbGF0aXZlIGJnLWNyZXcgdy1mdWxsICBoLXNjcmVlbiBiZy1jb3ZlciBmbGV4IGZsZXgtY29sIGp1c3RpZnktYmV0d2VlblwiXG4gICAgICAgID5cbiAgICAgICAgICA8SGVhZGVyIC8+XG5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXggZmxleC1jb2wgIG1kOmFic29sdXRlIHJpZ2h0LTY0IGJvdHRvbS0xMSAgIGp1c3RpZnktY2VudGVyIGl0ZW1zLWNlbnRlclwiPlxuICAgICAgICAgICAgPGJ1dHRvblxuICAgICAgICAgICAgICBpZD1cImN0YTJcIlxuICAgICAgICAgICAgICBjbGFzc05hbWU9XCIgIHRleHQtM3hsIGJnLXllbGxvdy0zMDAgdXBwZXJjYXNlIGl0YWxpYyBmb250LWJvbGQgIG1iLTIgcHgtMTYgcHktNCAgdGV4dC1pY29uQ29sb3JcIlxuICAgICAgICAgICAgPlxuICAgICAgICAgICAgICBNaW50IEhlcmVcbiAgICAgICAgICAgIDwvYnV0dG9uPlxuXG4gICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJ0ZXh0LTJ4bCBmb250LWJvbGQgdGV4dC1ncmF5LTgwMCBtYi00XCI+XG4gICAgICAgICAgICAgIHtzaG93UmVsZWFzZURhdGUgJiYgY291bmRvd25UZXh0fVxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cblxuICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIiB0ZXh0LWdyYXktODAwIFwiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicG9zdC1jb250ZW50IGNvbnRhaW5lciBteC1hdXRvIHRleHQtbGcgcC00IHRleHQtY2VudGVyIG1kOnRleHQtbGVmdCBcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmlyc3Qtc2VjdGlvblwiPlxuICAgICAgICAgICAgICA8aDEgY2xhc3NOYW1lPVwidGV4dC1ncmF5LTkwMCB0ZXh0LWxlZnQgb3VyLW1pc3Npb24gZm9udC1ib2xkIHRleHQtM3hsIGJvcmRlci1iLTIgcGItMyBib3JkZXItZ3JheS04MDAgbWQ6dy0xLzRcIj5cbiAgICAgICAgICAgICAgICBPdXIgTWlzc2lvblxuICAgICAgICAgICAgICA8L2gxPlxuICAgICAgICAgICAgICA8c2VjdGlvbiBjbGFzc05hbWU9XCJncmlkIG1kOmdyaWQtY29scy0yIGl0ZW1zLWNlbnRlclwiPlxuICAgICAgICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImFib3V0LW1pc3Npb25cIj4gICAgICAgICAgXG4gICAgICAgICAgICAgICAgPHA+SGkhIFdlbGNvbWUgdG8gdGhlIENvYWxpdGlvbiBDcmV3ISBUaGlzIGlzIGEgbGltaXRlZCBjb2xsZWN0aW9uIG9mIDUwMDAgdW5pcXVlIENoZWV0YWggTkZUcyBsaXZpbmcgb24gdGhlIEV0aGVyZXVtIGJsb2NrY2hhaW4uIFRoaXMgcHJvamVjdCBpcyB0ZWNobmljYWxseSBicm9rZW4gdXAgaW50byB0aHJlZSBjb2xsZWN0aW9ucy48L3A+XG4gICAgICAgICAgICAgICAgPHA+MXN0IENvbGxlY3Rpb24gLSBDb2FsaXRpb24gQ3JldyAoT0cgQ09MTEVDVElPTikgT25seSAxMDEwIGF2YWlsLlxuKlVwZGF0ZSAtIFNvbGQgb3V0IGluIGxlc3MgdGhhbiBhIGRheSEqPC9wPlxuICAgICAgICAgICAgICAgIDxwPjJuZCBDb2xsZWN0aW9uIC0gQXVjdGlvbiBDaGVldGFocyAtIDIwIGF2YWlsLiBUaGVzZSBxdWFsaWZ5IGZvciBWSVAgYWNjZXNzIHRvIGFsbCBsaXZlIGV2ZW50cy4gVGhlIGF1Y3Rpb24gYmVnaW5zIEphbiA4dGguIFBsZWFzZSB2aXNpdCBvdXIgPGEgdGFyZ2V0PVwiX2JsYW5rXCIgaHJlZj1cImh0dHA6Ly9kaXNjb3JkLmdnLzNuS1JCY0RTMzNcIiByZWw9XCJub3JlZmVycmVyXCIgY2xhc3M9XCJ1bmRlcmxpbmVcIj4gRGlzY29yZDwvYT4gZm9yIG1vcmUgaW5mbyBvbiB0aGlzISA8L3A+XG4gICAgICAgICAgICAgICAgPHA+M3JkIENvbGxlY3Rpb24gLSBDb2FsaXRpb24gQ3JldyAyLjAgLSBPbmx5IDM5NzAgYXZhaWwuIFRoaXMgd2lsbCB3cmFwIHVwIHRoZSBjb2xsZWN0aW9uIGZvciB0aGUgQ29hbGl0aW9uIENyZXcgcHJvamVjdC4gTWludCBwcmljZSBpcyAuMDkgRVRIIGFuZCB5b3UgY2FuIG1pbnQgdXAgdG8gNS4gVGhpcyBjb2xsZWN0aW9uIHdpbGwgYmVnaW4gbWludGluZyBKYW4gMjZ0aCBhdCAxMGFtIFBTVC8xcG0gRVNULjwvcD5cbiAgICAgICAgICAgICAgICA8cD5PdXQgb2YgdGhlIDUwMDAgdG90YWwsIG9ubHkgNTAgcXVhbGlmeSBmb3IgVklQIGFjY2Vzcy4gUGxlYXNlIHZpc2l0IG91ciA8YSB0YXJnZXQ9XCJfYmxhbmtcIiBocmVmPVwiaHR0cDovL2Rpc2NvcmQuZ2cvM25LUkJjRFMzM1wiIHJlbD1cIm5vcmVmZXJyZXJcIiBjbGFzcz1cInVuZGVybGluZVwiPiBEaXNjb3JkPC9hPiBEaXNjb3JkIGZvciBtb3JlIGluZm8gb24gd2hpY2ggb25lcyBxdWFsaWZ5IGZvciBWSVAuPC9wPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmxleCBpdGVtcy1jZW50ZXIganVzdGlmeS1jZW50ZXIgcmVsYXRpdmVcIj5cbiAgICAgICAgICAgICAgICAgIHtcIiBcIn1cbiAgICAgICAgICAgICAgICAgIDxtb3Rpb24uZGl2XG4gICAgICAgICAgICAgICAgICAgIGluaXRpYWw9e3sgb3BhY2l0eTogMCwgaGVpZ2h0OiBcIjIwcmVtXCIsIHdpZHRoOiBcIjIwcmVtXCIgfX1cbiAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZT17eyBvcGFjaXR5OiAxLCBoZWlnaHQ6IFwiMjJyZW1cIiwgd2lkdGg6IFwiMjJyZW1cIiB9fVxuICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJoLTcyICBkdXJhdGlvbi0zMDAgdy03MiBtZDp3LTk2IG1kOmgtOTYgYmctZ3JheS04MDAgcm91bmRlZC1mdWxsIGFic29sdXRlIHotMCBtZDpsZWZ0LTM4XCJcbiAgICAgICAgICAgICAgICAgID48L21vdGlvbi5kaXY+XG4gICAgICAgICAgICAgICAgICA8SW1hZ2VcbiAgICAgICAgICAgICAgICAgICAgYWx0PXtcImxvZ29cIn1cbiAgICAgICAgICAgICAgICAgICAgc3JjPXtgL2ltZy9ydW5uaW5nLWNoZWV0YWguZ2lmYH1cbiAgICAgICAgICAgICAgICAgICAgd2lkdGg9ezQwMH1cbiAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPVwicmVsYXRpdmVcIlxuICAgICAgICAgICAgICAgICAgICBvYmplY3RGaXQ9XCJjb250YWluXCJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0PXs0NTB9XG4gICAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L3NlY3Rpb24+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZsZXgganVzdGlmeS1jZW50ZXJcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwidy0zNiBoLTIgZmxleCAgYmctZ3JheS03MDAgbXktOVwiPjwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIFxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIHJlZj17cmVmfVxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiZ2FsbGVyeSByZWxhdGl2ZSBqdXN0aWZ5LWl0ZW1zLWNlbnRlciBmbGV4IGdhcC04ICBtZDpncmlkLWNvbHMtNCBcIlxuICAgICAgICAgID5cbiAgICAgICAgICAgIHtzaHVmZmxlKGltYWdlUGF0aCkubWFwKChpbWFnZSwgaW5kZXgpID0+IChcbiAgICAgICAgICAgICAgPG1vdGlvbi5kaXZcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJpdGVtIG1pbi13LWZ1bGwgcm91bmRlZC1sZ1wiXG4gICAgICAgICAgICAgICAgYW5pbWF0ZT17aW1hZ2UuYW5pbWF0aW9ufVxuICAgICAgICAgICAgICAgIGtleT17aW5kZXh9XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8SW1hZ2VcbiAgICAgICAgICAgICAgICAgIGFsdD17XCJDaGVldGFoXCJ9XG4gICAgICAgICAgICAgICAgICBzcmM9e1wiL2ltZy9cIiArIGltYWdlLnNyY31cbiAgICAgICAgICAgICAgICAgIHdpZHRoPXs2MjB9XG4gICAgICAgICAgICAgICAgICBoZWlnaHQ9ezY1MH1cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L21vdGlvbi5kaXY+XG4gICAgICAgICAgICApKX1cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwiZmxleCBmbGV4LWNvbCAgcmlnaHQtNjQgYm90dG9tLTExICAganVzdGlmeS1jZW50ZXIgaXRlbXMtY2VudGVyXCI+XG4gICAgICAgICAgICA8YnV0dG9uIGlkPVwiY3RhMlwiIGNsYXNzPVwiIHJvYWRtYXAgIHRleHQtM3hsIGJnLXllbGxvdy0zMDAgdXBwZXJjYXNlIGl0YWxpYyBmb250LWJvbGQgIG1iLTIgcHgtMTYgcHktNCAgdGV4dC1pY29uQ29sb3JcIj48YSB0YXJnZXQ9XCJfYmxhbmtcIiBocmVmPVwiaHR0cHM6Ly9kcml2ZS5nb29nbGUuY29tL2ZpbGUvZC8xSlBBWFhMbFItdlRwZUdlM3JIbXhlcTRYQzNVUzNodjUvdmlldz91c3A9c2hhcmluZ1wiPlJvYWRtYXA8L2E+PC9idXR0b24+XG4gICAgICAgICAgICA8c3BhbiBjbGFzcz1cInRleHQtMnhsIGZvbnQtYm9sZCB0ZXh0LWdyYXktODAwIG1iLTRcIj48L3NwYW4+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2IGlkPVwiYmFubmVyMlwiIGNsYXNzPVwiIHJlbGF0aXZlIGJnLXZhbHVlIHctZnVsbCAgaC1zY3JlZW4gYmctY292ZXIgZmxleCBmbGV4LWNvbCBqdXN0aWZ5LWJldHdlZW4gXCI+XG4gICAgICAgICAgXG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmbGV4IGp1c3RpZnktY2VudGVyXCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm1heC13LTZ4bCBvdmVyZmxvdy1oaWRkZW4gdy0zNiBoLTIgZmxleCBtdC04ICBiZy1ncmF5LTgwMCBcIj48L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8ZGl2XG4gICAgICAgICAgICBpZD1cImJhbm5lcjJcIlxuICAgICAgICAgICAgY2xhc3NOYW1lPVwiIHJlbGF0aXZlIGJnLXZhbHVlIHctZnVsbCAgaC1zY3JlZW4gYmctY292ZXIgZmxleCBmbGV4LWNvbCBqdXN0aWZ5LWJldHdlZW4gXCJcbiAgICAgICAgICA+PC9kaXY+XG4gICAgICAgICAgPGRpdlxuICAgICAgICAgICAgaWQ9XCJiYW5uZXIzXCJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cIiByZWxhdGl2ZSBiZy1tZW50b3Igdy1mdWxsICBoLXNjcmVlbiBiZy1jb3ZlciBmbGV4IGZsZXgtY29sIGp1c3RpZnktYmV0d2VlbiBteS0xMFwiXG4gICAgICAgICAgPjwvZGl2PlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGlkPVwiYmFubmVyNFwiXG4gICAgICAgICAgICBjbGFzc05hbWU9XCIgcmVsYXRpdmUgYmctbmV0d29yazEgdy1mdWxsICBoLXNjcmVlbiBiZy1jb3ZlciBmbGV4IGZsZXgtY29sIGp1c3RpZnktYmV0d2VlbiBtYi0xMFwiXG4gICAgICAgICAgPjwvZGl2PlxuICAgICAgICAgIDxkaXZcbiAgICAgICAgICAgIGlkPVwiYmFubmVyNVwiXG4gICAgICAgICAgICBjbGFzc05hbWU9XCIgcmVsYXRpdmUgYmctbmV0d29yazIgdy1mdWxsICBoLXNjcmVlbiBiZy1jb3ZlciBmbGV4IGZsZXgtY29sIGp1c3RpZnktYmV0d2VlbiBtYi0xMFwiXG4gICAgICAgICAgPjwvZGl2PlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwicG9zdC1jb250ZW50IGNvbnRhaW5lciBteC1hdXRvIHRleHQtbGcgcC00IHRleHQtY2VudGVyIG1kOnRleHQtbGVmdCBcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmlyc3Qtc2VjdGlvblwiPlxuICAgICAgICAgICAgICA8c2VjdGlvblxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZT1cImZvdXJ0aC1zZWN0aW9uXCJcbiAgICAgICAgICAgICAgICAvLyBkYW5nZXJvdXNseVNldElubmVySFRNTD17eyBfX2h0bWw6IGRhdGEuYWNmLmZvdXJ0aF9zZWN0aW9uIH19XG4gICAgICAgICAgICAgID5cbiAgICAgICAgICAgICAgICA8ZGl2PlxuICAgICAgICAgICAgICAgICAgPHA+XG4gICAgICAgICAgICAgICAgICAgIE9uY2UgbWludGVkIG9yIGJvdWdodCBvbiBPcGVuU2VhLCBwbGVhc2UgZmlsbCBvdXQgdGhpc3tcIiBcIn1cbiAgICAgICAgICAgICAgICAgICAgPGFcbiAgICAgICAgICAgICAgICAgICAgICB0YXJnZXQ9XCJfYmxhbmtcIlxuICAgICAgICAgICAgICAgICAgICAgIGhyZWY9XCJodHRwczovL3d3dy5nYW1lY2hhbmdlcnNtb3ZlbWVtZW50LmNvbS9jcmV3XCJcbiAgICAgICAgICAgICAgICAgICAgICByZWw9XCJub3JlZmVycmVyXCJcbiAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9XCJ1bmRlcmxpbmVcIlxuICAgICAgICAgICAgICAgICAgICA+XG4gICAgICAgICAgICAgICAgICAgICAgZm9ybVxuICAgICAgICAgICAgICAgICAgICA8L2E+e1wiIFwifVxuICAgICAgICAgICAgICAgICAgICB0byBnZXQgYWNjZXNzIHRvIHRoZSBBY2FkZW15LlxuICAgICAgICAgICAgICAgICAgICA8YnIgLz5cbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiZm9udC1ib2xkXCI+Tm90ZSE8L3NwYW4+XG4gICAgICAgICAgICAgICAgICAgIDxiciAvPiBJZiB5b3Ugc2VsbCB5b3VyIG9ubHkgY2hlZXRhaCwgeW91ciBhY2Nlc3MgdG8gdGhlXG4gICAgICAgICAgICAgICAgICAgIEFjYWRlbXkgd2lsbCBiZSBzdXNwZW5kZWQuIFlvdSBtdXN0IG93biBhdCBsZWFzdCAxIHRvIGtlZXBcbiAgICAgICAgICAgICAgICAgICAgYWNjZXNzLiA8YnIgLz4gQWZ0ZXIgMyBtb250aHMgYWxsIGhvbGRlcnMgd2lsbCByZWNlaXZlIHRoZWlyXG4gICAgICAgICAgICAgICAgICAgIGNoZWV0YWggYWRvcHRpb24g4oCccGFwZXJ3b3Jr4oCdLiBUaGlzIGluY2x1ZGVzIGEgcGhvdG8sIGFuXG4gICAgICAgICAgICAgICAgICAgIGFkb3B0aW9uIGNlcnRpZmljYXRlLCBhIHN0dWZmZWQgYW5pbWFsLCBhIHNwZWNpZXMgY2FyZCArXG4gICAgICAgICAgICAgICAgICAgIG1vcmUhIDxiciAvPiBXZSBhcmUgbGF1bmNoaW5nIGEgdW5pcXVlIG1lcmNoYW5kaXNlIHN0b3JlIGZvclxuICAgICAgICAgICAgICAgICAgICBDb2FsaXRpb24gQ3JldyBNZW1iZXJzIHRvIG1ha2UgY3VzdG9tIGdlYXIuIDxiciAvPiBBbGxcbiAgICAgICAgICAgICAgICAgICAgaG9sZGVycyBnZXQgYXV0b21hdGljIFdMIGZvciBmdXR1cmUgcHJvamVjdHMsIGFzIHdlbGwgYXNcbiAgICAgICAgICAgICAgICAgICAgZXhjbHVzaXZlIGFpcmRyb3BzLlxuICAgICAgICAgICAgICAgICAgPC9wPlxuICAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICA8L3NlY3Rpb24+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxGb3VuZGVyIC8+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImgtMC41IHctZnVsbCBiZy1ncmF5LTcwMCBteS04XCI+PC9kaXY+XG4gICAgICAgICAgICA8Rm9vdGVyIC8+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvZGl2PlxuICAgICAgPC9tYWluPlxuICAgIDwvPlxuICApO1xufVxuXG5leHBvcnQgY29uc3QgZ2V0U3RhdGljUHJvcHMgPSBhc3luYyAoKSA9PiB7XG4gIGNvbnN0IGRhdGEgPSBhd2FpdCBmZXRjaChcbiAgICBgJHtwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19CTE9HX1VSTH0vd3AvdjIvcGFnZXMvNDg3N2BcbiAgKS50aGVuKCk7XG4gIGNvbnN0IGhvbWUgPSBhd2FpdCBkYXRhLmpzb24oKTtcblxuICByZXR1cm4ge1xuICAgIHByb3BzOiB7XG4gICAgICBkYXRhOiBob21lLFxuICAgIH0sXG4gIH07XG59O1xuIl0sInNvdXJjZVJvb3QiOiIifQ==