(function() {
var exports = {};
exports.id = "pages/_app";
exports.ids = ["pages/_app"];
exports.modules = {

/***/ "./pages/_app.js":
/*!***********************!*\
  !*** ./pages/_app.js ***!
  \***********************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var framer_motion__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! framer-motion */ "framer-motion");
/* harmony import */ var framer_motion__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(framer_motion__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_wp_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../styles/wp.css */ "./styles/wp.css");
/* harmony import */ var _styles_wp_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_wp_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/globals.css */ "./styles/globals.css");
/* harmony import */ var _styles_globals_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_globals_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var tailwindcss_tailwind_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tailwindcss/tailwind.css */ "./node_modules/tailwindcss/tailwind.css");
/* harmony import */ var tailwindcss_tailwind_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(tailwindcss_tailwind_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next-seo */ "next-seo");
/* harmony import */ var next_seo__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_seo__WEBPACK_IMPORTED_MODULE_5__);

var _jsxFileName = "C:\\Users\\samee\\Desktop\\crew-main\\pages\\_app.js";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




 // import type { AppProps } from "next/app";



function MyApp({
  Component,
  pageProps
}) {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(framer_motion__WEBPACK_IMPORTED_MODULE_1__.AnimatePresence, {
    exitBeforeEnter: true,
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(next_seo__WEBPACK_IMPORTED_MODULE_5__.DefaultSeo, {
      title: "Coalitioncrew",
      titleTemplate: "%s | Coalitioncrew",
      description: "Youtube Twitter Instagram Welcome to the Coalition Mint here This is the NFT for Game Changers. The Coalition Crew is an exclusive collection of 7100 unique Cheetah NFTs living on the Ethereum blockchain. It\u2019s estimated that as of 2021, there are only 7100 cheetahs left in the wild. Cheetahs are currently listed as vulnerable and \u2026 Home Read More \xBB",
      canonical: process.env.NEXT_PUBLIC_DOMAIN,
      openGraph: {
        type: "website",
        locale: "en_US",
        url: process.env.NEXT_PUBLIC_DOMAIN,
        title: "Coalitioncrew",
        description: "Youtube Twitter Instagram Welcome to the Coalition Mint here This is the NFT for Game Changers. The Coalition Crew is an exclusive collection of 7100 unique Cheetah NFTs living on the Ethereum blockchain. It’s estimated that as of 2021, there are only 7100 cheetahs left in the wild. Cheetahs are currently listed as vulnerable and &hellip; Home Read More &raquo;"
      }
    }, "seo", false, {
      fileName: _jsxFileName,
      lineNumber: 12,
      columnNumber: 7
    }, this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Component, _objectSpread({}, pageProps), void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 28,
      columnNumber: 7
    }, this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 11,
    columnNumber: 5
  }, this);
}

/* harmony default export */ __webpack_exports__["default"] = (MyApp);

/***/ }),

/***/ "./node_modules/tailwindcss/tailwind.css":
/*!***********************************************!*\
  !*** ./node_modules/tailwindcss/tailwind.css ***!
  \***********************************************/
/***/ (function() {



/***/ }),

/***/ "./styles/globals.css":
/*!****************************!*\
  !*** ./styles/globals.css ***!
  \****************************/
/***/ (function() {



/***/ }),

/***/ "./styles/wp.css":
/*!***********************!*\
  !*** ./styles/wp.css ***!
  \***********************/
/***/ (function() {



/***/ }),

/***/ "framer-motion":
/*!********************************!*\
  !*** external "framer-motion" ***!
  \********************************/
/***/ (function(module) {

"use strict";
module.exports = require("framer-motion");;

/***/ }),

/***/ "next-seo":
/*!***************************!*\
  !*** external "next-seo" ***!
  \***************************/
/***/ (function(module) {

"use strict";
module.exports = require("next-seo");;

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-dev-runtime");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/_app.js"));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9jcmV3Ly4vcGFnZXMvX2FwcC5qcyIsIndlYnBhY2s6Ly9jcmV3L2V4dGVybmFsIFwiZnJhbWVyLW1vdGlvblwiIiwid2VicGFjazovL2NyZXcvZXh0ZXJuYWwgXCJuZXh0LXNlb1wiIiwid2VicGFjazovL2NyZXcvZXh0ZXJuYWwgXCJyZWFjdC9qc3gtZGV2LXJ1bnRpbWVcIiJdLCJuYW1lcyI6WyJNeUFwcCIsIkNvbXBvbmVudCIsInBhZ2VQcm9wcyIsInByb2Nlc3MiLCJlbnYiLCJORVhUX1BVQkxJQ19ET01BSU4iLCJ0eXBlIiwibG9jYWxlIiwidXJsIiwidGl0bGUiLCJkZXNjcmlwdGlvbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUVBO0FBQ0E7Q0FHQTs7QUFDQTs7QUFDQSxTQUFTQSxLQUFULENBQWU7QUFBRUMsV0FBRjtBQUFhQztBQUFiLENBQWYsRUFBeUM7QUFDdkMsc0JBQ0UsOERBQUMsMERBQUQ7QUFBaUIsbUJBQWUsTUFBaEM7QUFBQSw0QkFDRSw4REFBQyxnREFBRDtBQUVFLFdBQUssRUFBQyxlQUZSO0FBR0UsbUJBQWEsRUFBQyxvQkFIaEI7QUFJRSxpQkFBVyxFQUFDLDZXQUpkO0FBS0UsZUFBUyxFQUFFQyxPQUFPLENBQUNDLEdBQVIsQ0FBWUMsa0JBTHpCO0FBTUUsZUFBUyxFQUFFO0FBQ1RDLFlBQUksRUFBRSxTQURHO0FBRVRDLGNBQU0sRUFBRSxPQUZDO0FBR1RDLFdBQUcsRUFBRUwsT0FBTyxDQUFDQyxHQUFSLENBQVlDLGtCQUhSO0FBSVRJLGFBQUssRUFBRSxlQUpFO0FBS1RDLG1CQUFXLEVBQ1Q7QUFOTztBQU5iLE9BQ00sS0FETjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBREYsZUFpQkUsOERBQUMsU0FBRCxvQkFBZVIsU0FBZjtBQUFBO0FBQUE7QUFBQTtBQUFBLFlBakJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBcUJEOztBQUNELCtEQUFlRixLQUFmLEU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0JBLDJDOzs7Ozs7Ozs7OztBQ0FBLHNDOzs7Ozs7Ozs7OztBQ0FBLG1EIiwiZmlsZSI6InBhZ2VzL19hcHAuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBbmltYXRlUHJlc2VuY2UgfSBmcm9tIFwiZnJhbWVyLW1vdGlvblwiO1xuXG5pbXBvcnQgXCIuLi9zdHlsZXMvd3AuY3NzXCI7XG5pbXBvcnQgXCIuLi9zdHlsZXMvZ2xvYmFscy5jc3NcIjtcbmltcG9ydCBcInRhaWx3aW5kY3NzL3RhaWx3aW5kLmNzc1wiO1xuXG4vLyBpbXBvcnQgdHlwZSB7IEFwcFByb3BzIH0gZnJvbSBcIm5leHQvYXBwXCI7XG5pbXBvcnQgeyBEZWZhdWx0U2VvIH0gZnJvbSBcIm5leHQtc2VvXCI7XG5mdW5jdGlvbiBNeUFwcCh7IENvbXBvbmVudCwgcGFnZVByb3BzIH0pIHtcbiAgcmV0dXJuIChcbiAgICA8QW5pbWF0ZVByZXNlbmNlIGV4aXRCZWZvcmVFbnRlcj5cbiAgICAgIDxEZWZhdWx0U2VvXG4gICAgICAgIGtleT1cInNlb1wiXG4gICAgICAgIHRpdGxlPVwiQ29hbGl0aW9uY3Jld1wiXG4gICAgICAgIHRpdGxlVGVtcGxhdGU9XCIlcyB8IENvYWxpdGlvbmNyZXdcIlxuICAgICAgICBkZXNjcmlwdGlvbj1cIllvdXR1YmUgVHdpdHRlciBJbnN0YWdyYW0gV2VsY29tZSB0byB0aGUgQ29hbGl0aW9uIE1pbnQgaGVyZSBUaGlzIGlzIHRoZSBORlQgZm9yIEdhbWUgQ2hhbmdlcnMuIFRoZSBDb2FsaXRpb24gQ3JldyBpcyBhbiBleGNsdXNpdmUgY29sbGVjdGlvbiBvZiA3MTAwIHVuaXF1ZSBDaGVldGFoIE5GVHMgbGl2aW5nIG9uIHRoZSBFdGhlcmV1bSBibG9ja2NoYWluLiBJdOKAmXMgZXN0aW1hdGVkIHRoYXQgYXMgb2YgMjAyMSwgdGhlcmUgYXJlIG9ubHkgNzEwMCBjaGVldGFocyBsZWZ0IGluIHRoZSB3aWxkLiBDaGVldGFocyBhcmUgY3VycmVudGx5IGxpc3RlZCBhcyB2dWxuZXJhYmxlIGFuZCAmaGVsbGlwOyBIb21lIFJlYWQgTW9yZSAmcmFxdW87XCJcbiAgICAgICAgY2Fub25pY2FsPXtwcm9jZXNzLmVudi5ORVhUX1BVQkxJQ19ET01BSU59XG4gICAgICAgIG9wZW5HcmFwaD17e1xuICAgICAgICAgIHR5cGU6IFwid2Vic2l0ZVwiLFxuICAgICAgICAgIGxvY2FsZTogXCJlbl9VU1wiLFxuICAgICAgICAgIHVybDogcHJvY2Vzcy5lbnYuTkVYVF9QVUJMSUNfRE9NQUlOLFxuICAgICAgICAgIHRpdGxlOiBcIkNvYWxpdGlvbmNyZXdcIixcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcbiAgICAgICAgICAgIFwiWW91dHViZSBUd2l0dGVyIEluc3RhZ3JhbSBXZWxjb21lIHRvIHRoZSBDb2FsaXRpb24gTWludCBoZXJlIFRoaXMgaXMgdGhlIE5GVCBmb3IgR2FtZSBDaGFuZ2Vycy4gVGhlIENvYWxpdGlvbiBDcmV3IGlzIGFuIGV4Y2x1c2l2ZSBjb2xsZWN0aW9uIG9mIDcxMDAgdW5pcXVlIENoZWV0YWggTkZUcyBsaXZpbmcgb24gdGhlIEV0aGVyZXVtIGJsb2NrY2hhaW4uIEl04oCZcyBlc3RpbWF0ZWQgdGhhdCBhcyBvZiAyMDIxLCB0aGVyZSBhcmUgb25seSA3MTAwIGNoZWV0YWhzIGxlZnQgaW4gdGhlIHdpbGQuIENoZWV0YWhzIGFyZSBjdXJyZW50bHkgbGlzdGVkIGFzIHZ1bG5lcmFibGUgYW5kICZoZWxsaXA7IEhvbWUgUmVhZCBNb3JlICZyYXF1bztcIixcbiAgICAgICAgfX1cbiAgICAgIC8+XG5cbiAgICAgIDxDb21wb25lbnQgey4uLnBhZ2VQcm9wc30gLz5cbiAgICA8L0FuaW1hdGVQcmVzZW5jZT5cbiAgKTtcbn1cbmV4cG9ydCBkZWZhdWx0IE15QXBwO1xuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZnJhbWVyLW1vdGlvblwiKTs7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwibmV4dC1zZW9cIik7OyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTs7Il0sInNvdXJjZVJvb3QiOiIifQ==